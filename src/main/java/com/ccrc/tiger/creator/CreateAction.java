/**
 * 
 */
package com.ccrc.tiger.creator;

import java.util.List;

import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;

/**
 * @author Jalen
 * 基于 struts2 创建 Action 层
 */
public class CreateAction extends Creator {

	public CreateAction(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}

}
