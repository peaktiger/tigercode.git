/**
 * 
 */
package com.ccrc.tiger.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

/**
 * @author Jalen
 *
 */
@SuppressWarnings("serial")
public class EntitySetting extends BasePanel {

	private int width = 900;
	private int height = 600;

	public EntitySetting(int width, int height) {
		this.width = width;
		this.height = height;
		initComponent();
	}

	public void initComponent() {
		this.setSize(new Dimension(width, height - 30));
		this.setLayout(null);
		this.setBackground(Color.WHITE);

		JButton close = new JButton("X");
		close.setBounds(width - 45, 2, 30, 20);
		this.add(close);
		close.addActionListener(new EntityActionListener(this));

		JLabel tbPrixLabel = new JLabel("是否去掉表前缀：");
		tbPrixLabel.setBounds(20, 6, 150, 25);
		
		JCheckBox prefixCheckBox = new JCheckBox();
		prefixCheckBox.setBounds(130, 6, 40, 25);
		
		this.add(tbPrixLabel);
		this.add(prefixCheckBox);
		
	}

	private class EntityActionListener implements ActionListener {
		private EntitySetting entset = null;

		public EntityActionListener(EntitySetting entset) {
			this.entset = entset;
		}

		public void actionPerformed(ActionEvent e) {
			entset.setVisible(false);
		}
	}

}
