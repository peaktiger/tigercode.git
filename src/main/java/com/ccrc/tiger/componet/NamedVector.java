/**
 * 
 */
package com.ccrc.tiger.componet;

import java.util.Vector;

/**
 * @author Jalen
 *
 */
@SuppressWarnings("serial")
public class NamedVector extends Vector<Object> {
	private String name;

	public NamedVector(String name) {
		this.name = name;
	}

	public NamedVector(String name, Object elements[]) {
		this.name = name;
		for (int i = 0, n = elements.length; i < n; i++) {
			add(elements[i]);
		}
	}
	
	public String toString() {
		return name;
	}
}
