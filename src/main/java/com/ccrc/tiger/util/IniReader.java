package com.ccrc.tiger.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.Maps;

public class IniReader {
    private final static Logger LOG = Logger.getLogger(IniReader.class);

    private static IniReader reader = null;
    public static HashMap<String, HashMap<String, HashMap<String, String>>> sections = Maps.newHashMap();
    private String currentSecion, currTag;
    private HashMap<String, HashMap<String, String>> current;
    private HashMap<String, String> cfgMap;
    private static String fileName = "/cfg.ini";
    public static String file = "/cfg.ini";
    public static String baseDir = "/";
    
    private String currdbType;

    private IniReader() {
    }

    private IniReader(String filename) {
        file = filename;
        init();
    }

    public static IniReader getIniReader() {
        if (Objects.isNull(reader)) {
            baseDir = Utililies.getBaseDir();
            reader = new IniReader(baseDir + fileName);
        }
        return reader;
    }

    private void init() {
        try {
            FileUtils.createDir(baseDir);
            FileUtils.createFile(file);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            read(reader);
            reader.close();

            if (!sections.containsKey("use_db_type")) {
                HashMap<String, HashMap<String, String>> tagMap = Maps.newHashMap();
                
                HashMap<String, String> cfgMap = Maps.newHashMap();
                cfgMap.put("myType", "mysql_conn_info");
                cfgMap.put("orcType", "oracle_conn_info");
                tagMap.put("db", cfgMap);
                
                sections.put("use_db_type", tagMap);
                this.save();
            }
        } catch (Exception e) {
            LOG.error("初始化配置失败", e);
        }
    }

    private void read(BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            parseLine(line);
        }
    }

    private void parseLine(String line) {
        line = line.trim();
        LOG.info("line===parseLine=##########========="+line);
        if (line.matches("\\[.*\\]")) {
            currentSecion = line.replaceFirst("\\[(.*)\\]", "$1");
            current = Maps.newHashMap();
            sections.put(currentSecion, current);
            cfgMap = Maps.newHashMap();
        } else if (line.matches("<.*>")) {
            currTag = line.replaceFirst("<(.*)>", "$1");
            cfgMap = Maps.newHashMap();
            current.put(currTag, cfgMap);
        } else if (line.matches(".*=.*")) {
            if (Objects.nonNull(cfgMap)) {
                int i = line.indexOf('=');
                String name = line.substring(0, i);
                String value = line.substring(i + 1);
                cfgMap.put(name, value);
            }
        }
    }

    public HashMap<String, HashMap<String, String>> getConfig(String section) {
        HashMap<String, HashMap<String, String>> p = sections.get(section);
        if (MapUtils.isEmpty(p)) {
            return null;
        }
        return p;
    }

    public HashMap<String, String> getConfig(String section, String tag) {
        HashMap<String, HashMap<String, String>> p = sections.get(section);
        if (MapUtils.isEmpty(p)) {
            return null;
        }
        HashMap<String, String> valMap = p.get(tag);
        if (MapUtils.isEmpty(valMap)) {
            return null;
        }
        return valMap;
    }

    public String getValue(String section, String tag, String name) {
        HashMap<String, String> valMap = getConfig(section, tag);
        if (MapUtils.isEmpty(valMap)) {
            return null;
        }
        return valMap.get(name);
    }

    public IniReader putValue(String section, String tag, String name, String value) {
        HashMap<String, HashMap<String, String>> map = sections.get(section);
        HashMap<String, String> cfg = null;
        if (MapUtils.isEmpty(map)) {
            map = Maps.newHashMap();
            sections.put(section, map);
            cfg = Maps.newHashMap();
            map.put(tag, cfg);
        } else {
            cfg = map.get(tag);
            if (MapUtils.isEmpty(cfg)) {
                cfg = Maps.newHashMap();
                map.put(tag, cfg);
            }
        }
        cfg.put(name, value);
        return this;
    }

    /**
     * 获取正激活使用的配置
     * 
     * @param section
     * @return
     */
    public HashMap<String, String> getActiveCfg(String section) {
        HashMap<String, HashMap<String, String>> map = sections.get(section);
        HashMap<String, String> result = null, temp = null;
        if (MapUtils.isNotEmpty(map)) {
            String isSelected = StringUtils.EMPTY;
            for (Map.Entry<String, HashMap<String, String>> ent : map.entrySet()) {
                temp = ent.getValue();
                isSelected = temp.get("isSelected");
                if (StringUtils.isNotEmpty(isSelected) && isSelected.equals("true")) {
                    result = temp;
                    break;
                }
            }
        }
        return result;
    }
    
    /**
     * 替换激活配置
     * 
     * @param section
     * @return
     */
    public HashMap<String, String> replaceActiveCfg(String section, String tag) {
        HashMap<String, HashMap<String, String>> map = sections.get(section);
        HashMap<String, String> result = null;
        if (MapUtils.isNotEmpty(map)) {
        	result = map.get(tag);
        	String key = StringUtils.EMPTY;
            for (Map.Entry<String, HashMap<String, String>> ent : map.entrySet()) {
            	key = ent.getKey();
            	if(key.equals(tag)){
            		this.putValue(section, key, "isSelected", "true");
            	} else {
            		this.putValue(section, key, "isSelected", "false");
            	}
            }
        }
        return result;
    }

    public IniReader save() {
        try {
            StringBuffer sb = new StringBuffer();
            HashMap<String, HashMap<String, String>> currMap = null;
            HashMap<String, String> cfgMap = null;
            for (Map.Entry<String, HashMap<String, HashMap<String, String>>> entry : sections.entrySet()) {
                sb.append("[").append(entry.getKey()).append("]\r\n");
                currMap = entry.getValue();

                if (MapUtils.isNotEmpty(currMap)) {
                    for (Map.Entry<String, HashMap<String, String>> ent : currMap.entrySet()) {
                        sb.append("<").append(ent.getKey()).append(">\r\n");
                        cfgMap = ent.getValue();

                        for (Map.Entry<String, String> e : cfgMap.entrySet()) {
                            sb.append(e.getKey() + "=").append(e.getValue() + "\r\n");
                        }
                    }
                }
                sb.append("\r\n");
            }
            OutputStream fos = new FileOutputStream(file);// 加载读取文件流
            fos.write(sb.toString().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("保存配置信息错误！", e);
        }
        return this;
    }
    
    public String getCurrdbType() {
		return currdbType;
	}

	public void setCurrdbType(String currdbType) {
		this.currdbType = currdbType;
	}

	public static void main(String[] args) {
        IniReader.getIniReader();
        LOG.info(IniReader.sections);
    }

}
