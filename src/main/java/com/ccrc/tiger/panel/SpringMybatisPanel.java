/**
 * 
 */
package com.ccrc.tiger.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 * @author Jalen
 *
 */
@SuppressWarnings("serial")
public class SpringMybatisPanel extends BasePanel {
	
	private int width = 900;
	private int height = 600;

	public SpringMybatisPanel(int width, int height) {
		this.width = width;
		this.height = height;
		initComponent();
	}
	
	public void initComponent() {
		this.setSize(new Dimension(width, height - 30));
		this.setLayout(null);
		this.setBackground(Color.WHITE);

		JButton close = new JButton("X");
		close.setBounds(width - 45, 2, 30, 20);
		this.add(close);
		close.addActionListener(new SpringMybatisActionListener(this));

		JLabel headerLabel = new JLabel("SpringMVC + Spring + Mybatis集成开发");
		headerLabel.setBounds(10, 2, 250, 25);
		
		StringBuffer html = new StringBuffer()
				.append("<html>SpringMVC + Spring + Mybatis框架以Spring版本为4.3.5.RELEASE和Mybatis版本为3.4.1集成介绍。<br><br>")
				.append("<strong>1.</strong>创建项目工程<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>2.</strong>下载依赖工程<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>3.</strong>替换项目的pom文件或引入项目依赖jar包<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>4.</strong>引入项目配置文件和配置web.xml<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>5.</strong>使用TigerCoder工具生成代码<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>6.</strong>工程发布Tomcat服务器<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>7.</strong>访问项目地址<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍############</html>");
		
		JLabel mainLabel = new JLabel(html.toString());
		
		JScrollPane infoJsp = new JScrollPane(mainLabel);
        infoJsp.setBounds(5, 28, width - 20, height - 68);
        infoJsp.getViewport().setBackground(Color.WHITE);
        this.add(infoJsp);
        infoJsp.setVisible(true);
        
		this.add(headerLabel);
	}

	private class SpringMybatisActionListener implements ActionListener {
		private SpringMybatisPanel smPanel = null;

		public SpringMybatisActionListener(SpringMybatisPanel smPanel) {
			this.smPanel = smPanel;
		}

		public void actionPerformed(ActionEvent e) {
			smPanel.setVisible(false);
		}
	}

}

