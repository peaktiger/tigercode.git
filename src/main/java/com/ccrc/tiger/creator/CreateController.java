/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.ccrc.lang.DateStyle;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.FileUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 基于spring mvc 创建 controller 层
 */
public class CreateController extends Creator {

	public CreateController(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}
	
	public String createBaseController() {
        String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getControllerFilePath()) + "BaseController.java");
        String result = "BaseController类生成完成：" + javaPath;
        try {
        	File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "BaseController类已存在，无须重新生成！";
			} else {
	            String content = Utililies.readResourceFile(dir + "baseController.tlp");
	            // package
	            //content = Utililies.parseTemplate(content, "ControllerSuperPackage", Utililies.getSuperPackage(ca.getControllerPackage()));
	            //content = Utililies.parseTemplate(content, "EntitySuperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
	            //content = Utililies.parseTemplate(content, "ServiceSuperPackage", Utililies.getSuperPackage(ca.getServicePackage()));
	            content = Utililies.parseTemplate(content, "ControllerPackage", ca.getControllerRealPackage());
	            
	            //Utililies.writeContentToFile(javaPath, content);
	            Files.write(content.getBytes(), javaFile);
			}
        } catch (Exception e) {
            result = "BaseController类生成失败：" + e.getMessage();
        }
        return result;
    }
	
	public String createController() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
	    String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getControllerFilePath());
        String result = ca.getControllerName() + "生成完成：" + javaPath;
        try {
        	File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() + "Controller类已存在，无须重新生成！";
			} else {
	            String content = Utililies.readResourceFile(dir + "controller.tlp");
	            // package
	            /*content = Utililies.parseTemplate(content, "ControllerSuperPackage", Utililies.getSuperPackage(ca.getControllerPackage()));
	            content = Utililies.parseTemplate(content, "ControllerNameLower", CommonUtils.firstCharToLowerCase(ca.getControllerName()));
	            content = Utililies.parseTemplate(content, "ControllerName", ca.getControllerName());
	            content = Utililies.parseTemplate(content, "ServiceName", ca.getServiceName());
	            content = Utililies.parseTemplate(content, "ServiceNameLower", CommonUtils.firstCharToLowerCase(ca.getServiceName()));
	            content = Utililies.parseTemplate(content, "EntityName", getEntityName());
	            content = Utililies.parseTemplate(content, "EntitySuperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
	            content = Utililies.parseTemplate(content, "IServicePackage", ca.getIservicePackage());*/
	            content = Utililies.parseTemplate(content, "ControllerPackage", ca.getControllerRealPackage());
	            content = Utililies.parseTemplate(content, "ServicePackage", ca.getIservicePackage());
	            content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ? 
	            															getTableComment() : StringUtils.EMPTY);
	            content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
	            content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM));
	            content = Utililies.parseTemplate(content, "entityName", StringUtils.uncapitalize(getEntityName()));
	            content = Utililies.parseTemplate(content, "EntityName", getEntityName());
	            content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
	            content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
	            
	           /* if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
	            	content = Utililies.parseTemplate(content, "EntityPKPackage", ca.getDaoFrame().equalsIgnoreCase("Hibernate") ? "import " + ca.getEntityPackage() + "PK;" : StringUtils.EMPTY);
	            } else {
	            	content = Utililies.parseTemplate(content, "EntityPKPackage", StringUtils.EMPTY);
	            }*/
	            
	            Utililies.writeContentToFile(javaPath, content);
			}
			
			// 生成controller类里面的CURD操作
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.listPage(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getAddOp()) {
				String content = createSingleMethod(javaFile, Consts.add(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()) {
				String content = createSingleMethod(javaFile, Consts.modify(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				String content = StringUtils.EMPTY;
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKParams().split(Constants.CHAR_AND);
					content = createSingleMethod(javaFile, Consts.removeUnion(getEntityName(), split[0], split[1]));
				} else {
					String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.remove(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
				}
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.get(getEntityName(), methondBys, methondParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.getUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.get(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.find(getEntityName(), methondBys, methondParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.findUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.find(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.count(getEntityName(), methondBys, methondParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					//String[] curColumn = getCurColumnKey();
					String content = createSingleMethod(javaFile, Consts.count(getEntityName()));
					Files.write(content.getBytes(), javaFile);
				}
			}
        } catch (Exception e) {
            result = ca.getControllerName() + "生成失败：" + e.getMessage();
        }
        return result;
	}

}
