/**
 * 
 */
package com.ccrc.tiger.entity;

import com.ccrc.tiger.util.Objects;

/**
 * Oracle 数据库表字段信息
 * @author Jalen
 *
 */
public class OracleDbColumn extends DbTableColumn {
	private String tableSchema; // 所属库
    private String tableName; // 所属表
    private String tableComment; // 表注释
    private String columnName; // 列名
    private String columnType; // bigint(11)，varchar(30)
    private String dataType; // bigint->Long，int->Integer，varchar/char->String，timestamp->Timestamp，date/time->Date，float->Float，double/decimal->Double，blob->byte[]
    private Object columnDefault; // 列缺省值
    private Integer characterOctetLength; // 列最大字节长度，utf-8：
                                          // characterMaximumLength*
                                          // 3，gbk：characterMaximumLength* 2
    private Integer characterMaximumLength; // 列最大长度
    private Integer ordinalPosition; // 字节顺序码，从1开始
    private String isNullable; // 是否可为空，YES：可空，NO：非空
    private String columnKey; // 列键值,PRI主键
    private String extra; // auto_increment 自增长
    private String columnComment; // 列描述
    private String privileges; // 列权限集合，以逗号隔开
    
    public String getTableSchema() {
        return tableSchema;
    }

    public OracleDbColumn setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public OracleDbColumn setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }
    
    public String getTableComment() {
		return tableComment;
	}

	public OracleDbColumn setTableComment(String tableComment) {
		this.tableComment = tableComment;
		return this;
	}

	public String getColumnName() {
        return columnName;
    }

    public OracleDbColumn setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getColumnType() {
        return columnType;
    }

    public OracleDbColumn setColumnType(String columnType) {
        this.columnType = columnType;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public OracleDbColumn setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public Object getColumnDefault() {
        return columnDefault;
    }

    public OracleDbColumn setColumnDefault(Object columnDefault) {
        this.columnDefault = columnDefault;
        return this;
    }

    public Integer getCharacterOctetLength() {
        return characterOctetLength;
    }

    public OracleDbColumn setCharacterOctetLength(Integer characterOctetLength) {
        this.characterOctetLength = characterOctetLength;
        return this;
    }

    public Integer getCharacterMaximumLength() {
        return characterMaximumLength;
    }

    public OracleDbColumn setCharacterMaximumLength(Integer characterMaximumLength) {
        this.characterMaximumLength = characterMaximumLength;
        return this;
    }

    public Integer getOrdinalPosition() {
        return ordinalPosition;
    }

    public OracleDbColumn setOrdinalPosition(Integer ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
        return this;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public OracleDbColumn setIsNullable(String isNullable) {
        this.isNullable = isNullable;
        return this;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public OracleDbColumn setColumnKey(String columnKey) {
        this.columnKey = columnKey;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public OracleDbColumn setExtra(String extra) {
        this.extra = extra;
        return this;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public OracleDbColumn setColumnComment(String columnComment) {
        this.columnComment = columnComment;
        return this;
    }

    public String getPrivileges() {
        return privileges;
    }

    public OracleDbColumn setPrivileges(String privileges) {
        this.privileges = privileges;
        return this;
    }

    public boolean isPrimaryKey(){
        return Objects.nonNull(columnKey) && columnKey.equalsIgnoreCase("P");
    }
}
