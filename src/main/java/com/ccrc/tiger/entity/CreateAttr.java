package com.ccrc.tiger.entity;

import java.util.List;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.ccrc.tiger.componet.CheckBoxNode;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.collect.Multimap;

public class CreateAttr implements Cloneable {

	private String daoFrame;
	private String conFrame;
	private String viewFrame;
	private String jsFame;
	private String entityName;
	private String entityPackage;
	private String entityFilePath;
	private String entityPKFilePath;
	private String entitySerdeFilePath;
	private String daoName;
	private String daoPackage;
	private String daoFilePath;
	private String serviceName;
	private String iserviceName;
	private String servicePackage;
	private String serviceFilePath;
	private String iservicePackage;
	private String iserviceFilePath;
	private String mapName;
	private String mapPackage;
	private String mapFilePath;
	private String hbmName;
	private String hbmPackage;
	private String hbmFilePath;
	private String controllerName;
	private String controllerRealPackage;
	private String controllerPackage;
	private String controllerFilePath;
	private String actionName;
	private String actionPackage;
	private String actionFilePath;
	private String jspFileName;
	private String jspFilePath;
	private String jsFileName;
	private String jsFilePath;
	private String saveDir;
	private String saveDir2;
	
	// 生成文件
	private Boolean daoOp;
	private Boolean serviceOp;
	private Boolean controllerOp;
	private Boolean jspOp;
	private Boolean jsOp;
	
	// CRUD操作属性
	private Boolean getOp;
	private Boolean addOp;
	private Boolean modifyOp;
	private Boolean deleteOp;
	private Boolean pageOp;
	private Boolean findOp;
	private Boolean countOp;
	
	// 存放该表checkbox被选择的属性
	private Multimap<String, CheckBoxNode> selectedNodes;

	public String getDaoFrame() {
		return daoFrame;
	}

	public void setDaoFrame(String daoFrame) {
		this.daoFrame = daoFrame;
	}

	public String getConFrame() {
		return conFrame;
	}

	public void setConFrame(String conFrame) {
		this.conFrame = conFrame;
	}

	public String getViewFrame() {
		return viewFrame;
	}

	public void setViewFrame(String viewFrame) {
		this.viewFrame = viewFrame;
	}

	public String getJsFame() {
		return jsFame;
	}

	public void setJsFame(String jsFame) {
		this.jsFame = jsFame;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityPackage() {
		return entityPackage;
	}

	public void setEntityPackage(String entityPackage) {
		this.entityPackage = entityPackage;
	}

	public String getDaoName() {
		return daoName;
	}

	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}

	public String getDaoPackage() {
		return daoPackage;
	}

	public void setDaoPackage(String daoPackage) {
		this.daoPackage = daoPackage;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServicePackage() {
		return servicePackage;
	}

	public void setServicePackage(String servicePackage) {
		this.servicePackage = servicePackage;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getMapPackage() {
		return mapPackage;
	}

	public void setMapPackage(String mapPackage) {
		this.mapPackage = mapPackage;
	}

	public String getHbmName() {
		return hbmName;
	}

	public void setHbmName(String hbmName) {
		this.hbmName = hbmName;
	}

	public String getHbmPackage() {
		return hbmPackage;
	}

	public void setHbmPackage(String hbmPackage) {
		this.hbmPackage = hbmPackage;
	}

	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public String getControllerPackage() {
		return controllerPackage;
	}

	public void setControllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
	}

	public String getControllerRealPackage() {
		return controllerRealPackage;
	}

	public void setControllerRealPackage(String controllerRealPackage) {
		this.controllerRealPackage = controllerRealPackage;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getActionPackage() {
		return actionPackage;
	}

	public void setActionPackage(String actionPackage) {
		this.actionPackage = actionPackage;
	}

	public String getEntityFilePath() {
		return entityFilePath;
	}

	public void setEntityFilePath(String entityFilePath) {
		this.entityFilePath = entityFilePath;
	}
	
	public String getEntityPKFilePath() {
		return entityPKFilePath;
	}

	public void setEntityPKFilePath(String entityPKFilePath) {
		this.entityPKFilePath = entityPKFilePath;
	}

	public String getEntitySerdeFilePath() {
		return entitySerdeFilePath;
	}

	public void setEntitySerdeFilePath(String entitySerdeFilePath) {
		this.entitySerdeFilePath = entitySerdeFilePath;
	}

	public String getDaoFilePath() {
		return daoFilePath;
	}

	public void setDaoFilePath(String daoFilePath) {
		this.daoFilePath = daoFilePath;
	}

	public String getServiceFilePath() {
		return serviceFilePath;
	}

	public void setServiceFilePath(String serviceFilePath) {
		this.serviceFilePath = serviceFilePath;
	}

	public String getIservicePackage() {
		return iservicePackage;
	}

	public void setIservicePackage(String iservicePackage) {
		this.iservicePackage = iservicePackage;
	}

	public String getIserviceFilePath() {
		return iserviceFilePath;
	}

	public void setIserviceFilePath(String iserviceFilePath) {
		this.iserviceFilePath = iserviceFilePath;
	}

	public String getIserviceName() {
        return iserviceName;
    }

    public void setIserviceName(String iserviceName) {
        this.iserviceName = iserviceName;
    }

    public String getMapFilePath() {
		return mapFilePath;
	}

	public void setMapFilePath(String mapFilePath) {
		this.mapFilePath = mapFilePath;
	}

	public String getHbmFilePath() {
		return hbmFilePath;
	}

	public void setHbmFilePath(String hbmFilePath) {
		this.hbmFilePath = hbmFilePath;
	}

	public String getControllerFilePath() {
		return controllerFilePath;
	}

	public void setControllerFilePath(String controllerFilePath) {
		this.controllerFilePath = controllerFilePath;
	}

	public String getActionFilePath() {
		return actionFilePath;
	}

	public void setActionFilePath(String actionFilePath) {
		this.actionFilePath = actionFilePath;
	}

	public String getJspFileName() {
		return jspFileName;
	}

	public void setJspFileName(String jspFileName) {
		this.jspFileName = jspFileName;
	}

	public String getJspFilePath() {
		return jspFilePath;
	}

	public void setJspFilePath(String jspFilePath) {
		this.jspFilePath = jspFilePath;
	}

	public String getJsFileName() {
		return jsFileName;
	}

	public void setJsFileName(String jsFileName) {
		this.jsFileName = jsFileName;
	}

	public String getJsFilePath() {
		return jsFilePath;
	}

	public void setJsFilePath(String jsFilePath) {
		this.jsFilePath = jsFilePath;
	}

	public String getSaveDir() {
		return saveDir;
	}

	public void setSaveDir(String saveDir) {
		this.saveDir = saveDir;
	}

	public String getSaveDir2() {
        return saveDir2;
    }

    public void setSaveDir2(String saveDir2) {
        this.saveDir2 = saveDir2;
    }
    
    public Boolean getDaoOp() {
		return daoOp;
	}

	public void setDaoOp(Boolean daoOp) {
		this.daoOp = daoOp;
	}

	public Boolean getServiceOp() {
		return serviceOp;
	}

	public void setServiceOp(Boolean serviceOp) {
		this.serviceOp = serviceOp;
	}

	public Boolean getControllerOp() {
		return controllerOp;
	}

	public void setControllerOp(Boolean controllerOp) {
		this.controllerOp = controllerOp;
	}

	public Boolean getJspOp() {
		return jspOp;
	}

	public void setJspOp(Boolean jspOp) {
		this.jspOp = jspOp;
	}

	public Boolean getJsOp() {
		return jsOp;
	}

	public void setJsOp(Boolean jsOp) {
		this.jsOp = jsOp;
	}

	public Boolean getGetOp() {
		return getOp;
	}

	public void setGetOp(Boolean getOp) {
		this.getOp = getOp;
	}

	public Boolean getAddOp() {
		return addOp;
	}

	public void setAddOp(Boolean addOp) {
		this.addOp = addOp;
	}

	public Boolean getModifyOp() {
		return modifyOp;
	}

	public void setModifyOp(Boolean modifyOp) {
		this.modifyOp = modifyOp;
	}

	public Boolean getDeleteOp() {
		return deleteOp;
	}

	public void setDeleteOp(Boolean deleteOp) {
		this.deleteOp = deleteOp;
	}

	public Boolean getPageOp() {
		return pageOp;
	}

	public void setPageOp(Boolean pageOp) {
		this.pageOp = pageOp;
	}

	public Boolean getFindOp() {
		return findOp;
	}

	public void setFindOp(Boolean findOp) {
		this.findOp = findOp;
	}
	
	public Boolean getCountOp() {
		return countOp;
	}

	public void setCountOp(Boolean countOp) {
		this.countOp = countOp;
	}

	public Multimap<String, CheckBoxNode> getSelectedNodes() {
		return selectedNodes;
	}
	
	public List<CheckBoxNode> getCheckBoxNode(String tableName) {
		return (List<CheckBoxNode>) selectedNodes.get(tableName);
	}

	public void setSelectedNodes(Multimap<String, CheckBoxNode> selectedNodes) {
		this.selectedNodes = selectedNodes;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getClass().getName());
		sb.append(" # daoFrame=" + (Objects.isNull(daoFrame) ? "null" : daoFrame.toString()));
		sb.append("; conFrame=" + (Objects.isNull(conFrame) ? "null" : conFrame.toString()));
		sb.append("; viewFrame=" + (Objects.isNull(viewFrame) ? "null" : viewFrame.toString()));
		sb.append("; jsFame=" + (Objects.isNull(jsFame) ? "null" : jsFame.toString()));
		sb.append("; entityName=" + (Objects.isNull(entityName) ? "null" : entityName.toString()));
		sb.append("; entityPackage=" + (Objects.isNull(entityPackage) ? "null" : entityPackage.toString()));
		sb.append("; entityPKFilePath=" + (Objects.isNull(entityPKFilePath) ? "null" : entityPKFilePath.toString()));
		sb.append("; entityFilePath=" + (Objects.isNull(entityFilePath) ? "null" : entityFilePath.toString()));
		sb.append("; entitySerdeFilePath=" + (Objects.isNull(entitySerdeFilePath) ? "null" : entitySerdeFilePath.toString()));
		sb.append("; daoName=" + (Objects.isNull(daoName) ? "null" : daoName.toString()));
		sb.append("; daoPackage=" + (Objects.isNull(daoPackage) ? "null" : daoPackage.toString()));
		sb.append("; daoFilePath=" + (Objects.isNull(daoFilePath) ? "null" : daoFilePath.toString()));
		sb.append("; serviceName=" + (Objects.isNull(serviceName) ? "null" : serviceName.toString()));
		sb.append("; iserviceName=" + (Objects.isNull(iserviceName) ? "null" : iserviceName.toString()));
		sb.append("; servicePackage=" + (Objects.isNull(servicePackage) ? "null" : servicePackage.toString()));
		sb.append("; iservicePackage=" + (Objects.isNull(iservicePackage) ? "null" : iservicePackage.toString()));
		sb.append("; serviceFilePath=" + (Objects.isNull(serviceFilePath) ? "null" : serviceFilePath.toString()));
		sb.append("; iserviceFilePath=" + (Objects.isNull(iserviceFilePath) ? "null" : iserviceFilePath.toString()));
		sb.append("; mapName=" + (Objects.isNull(mapName) ? "null" : mapName.toString()));
		sb.append("; mapPackage=" + (Objects.isNull(mapPackage) ? "null" : mapPackage.toString()));
		sb.append("; mapFilePath=" + (Objects.isNull(mapFilePath) ? "null" : mapFilePath.toString()));
		sb.append("; hbmName=" + (Objects.isNull(hbmName) ? "null" : hbmName.toString()));
		sb.append("; hbmPackage=" + (Objects.isNull(hbmPackage) ? "null" : hbmPackage.toString()));
		sb.append("; hbmFilePath=" + (Objects.isNull(hbmFilePath) ? "null" : hbmFilePath.toString()));
		sb.append("; controllerName=" + (Objects.isNull(controllerName) ? "null" : controllerName.toString()));
		sb.append("; controllerRealPackage=" + (Objects.isNull(controllerRealPackage) ? "null" : controllerRealPackage.toString()));
		sb.append("; controllerPackage=" + (Objects.isNull(controllerPackage) ? "null" : controllerPackage.toString()));
		sb.append("; controllerFilePath=" + (Objects.isNull(controllerFilePath) ? "null" : controllerFilePath.toString()));
		sb.append("; actionName=" + (Objects.isNull(actionName) ? "null" : actionName.toString()));
		sb.append("; actionPackage=" + (Objects.isNull(actionPackage) ? "null" : actionPackage.toString()));
		sb.append("; actionFilePath=" + (Objects.isNull(actionFilePath) ? "null" : actionFilePath.toString()));
		sb.append("; jsFileName=" + (Objects.isNull(jsFileName) ? "null" : jsFileName.toString()));
		sb.append("; jspFilePath=" + (Objects.isNull(jspFilePath) ? "null" : jspFilePath.toString()));
		sb.append("; jsFileName=" + (Objects.isNull(jsFileName) ? "null" : jsFileName.toString()));
		sb.append("; jsFilePath=" + (Objects.isNull(jsFilePath) ? "null" : jsFilePath.toString()));
		sb.append("; saveDir=" + (Objects.isNull(saveDir) ? "null" : saveDir.toString()));
		sb.append("; getOp=" + (Objects.isNull(getOp) ? "null" : getOp.toString()));
		sb.append("; addOp=" + (Objects.isNull(addOp) ? "null" : addOp.toString()));
		sb.append("; modifyOp=" + (Objects.isNull(modifyOp) ? "null" : modifyOp.toString()));
		sb.append("; deleteOp=" + (Objects.isNull(deleteOp) ? "null" : deleteOp.toString()));
		sb.append("; pageOp=" + (Objects.isNull(pageOp) ? "null" : pageOp.toString()));
		sb.append("; findOp=" + (Objects.isNull(findOp) ? "null" : findOp.toString()));
		return sb.toString();
	}

	public void replaceAll(String table) {
		//this.entityName = Utililies.tableToEntity(table);
		this.entityName = CommonUtils.convertToCamelCase(table);
		this.entityPackage = Utililies.parseTemplate(this.entityPackage, "EntityName", this.entityName);
		this.entityPKFilePath = Utililies.parseTemplate(this.entityPKFilePath, "EntityName", this.entityName);
		this.entityFilePath = Utililies.parseTemplate(this.entityFilePath, "EntityName", this.entityName);

		this.daoName = Utililies.parseTemplate(this.daoName, "EntityName", this.entityName);
		this.daoPackage = Utililies.parseTemplate(this.daoPackage, "EntityName", this.entityName);
		this.daoFilePath = Utililies.parseTemplate(this.daoFilePath, "EntityName", this.entityName);

		this.serviceName = Utililies.parseTemplate(this.serviceName, "EntityName", this.entityName);
		this.servicePackage = Utililies.parseTemplate(this.servicePackage, "EntityName", this.entityName);
		this.serviceFilePath = Utililies.parseTemplate(this.serviceFilePath, "EntityName", this.entityName);
		this.iserviceName = Utililies.parseTemplate(this.iserviceName, "EntityName", this.entityName);
		this.iservicePackage = Utililies.parseTemplate(this.iservicePackage, "EntityName", this.entityName);
		this.iserviceFilePath = Utililies.parseTemplate(this.iserviceFilePath, "EntityName", this.entityName);

		this.mapName = Utililies.parseTemplate(this.mapName, "EntityName", this.entityName);
		this.mapPackage = Utililies.parseTemplate(this.mapPackage, "EntityName", this.entityName);
		this.mapFilePath = Utililies.parseTemplate(this.mapFilePath, "EntityName", this.entityName);

		this.hbmName = Utililies.parseTemplate(this.hbmName, "EntityName", this.entityName);
		this.hbmPackage = Utililies.parseTemplate(this.hbmPackage, "EntityName", this.entityName);
		this.hbmFilePath = Utililies.parseTemplate(this.hbmFilePath, "EntityName", this.entityName);

		this.controllerName = Utililies.parseTemplate(this.controllerName, "EntityName", this.entityName);
		this.controllerRealPackage = Utililies.parseTemplate(this.controllerRealPackage, "EntityName", this.entityName);
		this.controllerPackage = Utililies.parseTemplate(this.controllerPackage, "EntityName", this.entityName);
		this.controllerFilePath = Utililies.parseTemplate(this.controllerFilePath, "EntityName", this.entityName);

		this.actionName = Utililies.parseTemplate(this.actionName, "EntityName", this.entityName);
		this.actionPackage = Utililies.parseTemplate(this.actionPackage, "EntityName", this.entityName);
		this.actionFilePath = Utililies.parseTemplate(this.actionFilePath, "EntityName", this.entityName);
		
		this.jspFileName = Utililies.parseTemplate(this.jspFileName, "EntityName", StringUtils.uncapitalize(this.entityName));
		this.jspFilePath = Utililies.parseTemplate(this.jspFilePath, "EntityName", StringUtils.uncapitalize(this.entityName));
		
		this.jsFileName = Utililies.parseTemplate(this.jsFileName, "EntityName", StringUtils.uncapitalize(this.entityName));
		this.jsFilePath = Utililies.parseTemplate(this.jsFilePath, "EntityName", StringUtils.uncapitalize(this.entityName));
	}

	public CreateAttr clone() {
		CreateAttr createAttr = null;
		try {
			createAttr = (CreateAttr) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return createAttr;
	}


}
