/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.Date;
import java.util.List;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.ccrc.lang.DateStyle;
import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.FileUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 基于mybatis创建 Dao层
 */
public class CreateDaoMybatis extends Creator {

	public CreateDaoMybatis(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}
	
	public String createBaseArgument() {
	    String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getEntityFilePath()) + "base/BaseArgument.java");
        String result = "出入参对象类生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
        try {
            File javaFile = new File(javaPath);
            if(javaFile.exists()){
                result = "出入参对象类已存在，无须重新生成";
            } else {
                String content = Utililies.readResourceFile(dir + "baseArg.tlp");
                // EntitySuperPackage
                content = Utililies.parseTemplate(content, "EntitySuperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
                content = Utililies.parseTemplate(content, "PrimaryKeyJavaType", getPrimaryJavaType());
                Utililies.writeContentToFile(javaPath, content);
            }
        } catch (Exception e) {
            result = "出入参对象类生成失败：" + e.getMessage();
        }
        return result;
    }

	public String createIDao() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getDaoFilePath()) + "base/IBaseDao.java");
		String result = "IBaseDao接口生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "IBaseDao接口已存在，无须重新生成";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("ibase_dao.tlp"));
				// package
				content = Utililies.parseTemplate(content, "DaoSuperPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				Utililies.writeContentToFile(javaPath, content);
			}
		} catch (Exception e) {
			result = "IBaseDao接口生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createDao() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getDaoFilePath()) + "base/BaseDao.java");
		String result = "BaseDao类生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "BaseDao类已存在，无须重新生成";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("base_dao.tlp"));
				// package
				content = Utililies.parseTemplate(content, "DaoSuperPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				
				Utililies.writeContentToFile(javaPath, content);
			}
		} catch (Exception e) {
			result = "BaseDao接口生成失败：" + e.getMessage();
		}
		return result;
	}
	
	public String createDaoMapper() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getDaoFilePath());
		String result = ca.getDaoName() + "类生成完成：" + javaPath;
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() +"Service已存在，无须重新生成！";
			} else {
				//String content = Utililies.readResourceFile(getTlpPath("dao.tlp"));
				String content = Utililies.readResourceFile(getTlpPath("mybatis_dao.tlp"));
				// package
				content = Utililies.parseTemplate(content, "DaoSuperPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "DaoClassName", ca.getDaoName());
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM));
	
				Utililies.writeContentToFile(javaPath, content);
			}
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.getMapperInterfaceTmp(methondBys, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.getMapperInterfaceTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.getMapperInterfaceTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.findMapperInterfaceTmp(methondBys, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.findMapperInterfaceTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.findMapperInterfaceTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.countMapperInterfaceTmp(methondBys, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					//String[] curColumn = getCurColumnKey();
					String content = createSingleMethod(javaFile, Consts.getTotalMapperInterfaceTmp());
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.getPageMapperInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
				
				//content = createSingleMethod(javaFile, Consts.getTotalMapperInterfaceTmp());
				//Files.write(content.getBytes(), javaFile);
			}
			if (ca.getAddOp()) {
				String content = createSingleMethod(javaFile, Consts.getInsertMapperInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()){
				String content = createSingleMethod(javaFile, Consts.getUpdateMapperInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				String content = StringUtils.EMPTY;
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKParams().split(Constants.CHAR_AND);
					content = createSingleMethod(javaFile, Consts.getDeleteMapperInterfaceTmpUnion(split[0], split[1]));
				} else {
					String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.getDeleteMapperInterfaceTmp(Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
				}
				Files.write(content.getBytes(), javaFile);
			}
		} catch (Exception e) {
			result = ca.getDaoName() + "类生成失败：" + e.getMessage();
		}
		return result;
	}
	
	public String createIBaseService() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getServiceFilePath()) + "base/IBaseService.java");
		String result = "IBaseService接口生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "IBaseService接口已存在，无须重新生成";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("ibase_service.tlp"));
				// package
				content = Utililies.parseTemplate(content, "ServiceSuperPackage", Utililies.getSuperPackage(ca.getServicePackage()));
				content = Utililies.parseTemplate(content, "DaoPackage", ca.getDaoPackage());
				content = Utililies.parseTemplate(content, "EntitySuperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
	
				Utililies.writeContentToFile(javaPath, content);
			}
		} catch (Exception e) {
			result = "IBaseService接口生成失败：" + e.getMessage();
		}
		return result;
	}
	
	public String createBaseService() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getServiceFilePath()) + "base/BaseService.java");
		String result = "BaseService类生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "BaseService类已存在，无须重新生成";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("base_service.tlp"));
				// package
				content = Utililies.parseTemplate(content, "ServiceSuperPackage", Utililies.getSuperPackage(ca.getServicePackage()));
				content = Utililies.parseTemplate(content, "DaoSupperPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "EntitySupperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
				content = Utililies.parseTemplate(content, "DaoPackage", ca.getDaoPackage());
				content = Utililies.parseTemplate(content, "DaoClassName", ca.getDaoName());
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM));
	
				Utililies.writeContentToFile(javaPath, content);
			}
		} catch (Exception e) {
			result = "BaseService类生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createIService() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), Utililies.parseTemplate(ca.getIserviceFilePath(), "EntityName", getEntityName()));
		String result = "I" + ca.getEntityName() + "接口生成完成：" + javaPath;
		try {
			String content = Utililies.readResourceFile(getTlpPath("iservice.tlp"));
			// package
			content = Utililies.parseTemplate(content, "IServiceSuperPackage", Utililies.getSuperPackage(ca.getIservicePackage()));
			content = Utililies.parseTemplate(content, "EntitySupperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
			content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
			content = Utililies.parseTemplate(content, "EntityName", getEntityName());
			
			Utililies.writeContentToFile(javaPath, content);
		} catch (Exception e) {
			result = "I" + ca.getEntityName() + "接口生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createService() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getServiceFilePath());
		String result = ca.getServiceName() + "类生成完成：" + javaPath;
		try {
			String content = Utililies.readResourceFile(getTlpPath("service.tlp"));
			// package
			content = Utililies.parseTemplate(content, "ServiceSuperPackage", Utililies.getSuperPackage(ca.getServicePackage()));
			content = Utililies.parseTemplate(content, "EntitySupperPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
			content = Utililies.parseTemplate(content, "DaoSuperPackage", ca.getEntityPackage());
			content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
			content = Utililies.parseTemplate(content, "EntityName", getEntityName());
			content = Utililies.parseTemplate(content, "IServicePackage", ca.getIservicePackage());
			content = Utililies.parseTemplate(content, "ServiceName", ca.getServiceName());
			content = Utililies.parseTemplate(content, "IServiceName", ca.getIserviceName());
			
			Utililies.writeContentToFile(javaPath, content);
		} catch (Exception e) {
			result = ca.getServiceName() + "类生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createMapper() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getMapFilePath());
        if(javaPath != null && javaPath.indexOf("main/java/main/") != -1){
            javaPath = Utililies.parseFilePath(ca.getSaveDir2(), ca.getMapFilePath());
        }
		String result = ca.getMapName() + ".xml生成完成：" + javaPath;
		try {
			String content = Utililies.readResourceFile(getTlpPath("mapper.tlp"));
			// package
			content = Utililies.parseTemplate(content, "MapperPackage", ca.getDaoPackage());
			content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
			content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
			content = Utililies.parseTemplate(content, "PrimaryJdbcType", getPrimaryJdbcType());
			content = Utililies.parseTemplate(content, "PrimaryJavaType", getPrimaryJavaType());
			content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
			content = Utililies.parseTemplate(content, "FeildMapList", getFeildMapList());
			content = Utililies.parseTemplate(content, "FeildJoinId", getFeildJoinId());
			content = Utililies.parseTemplate(content, "TableName", tableName);
			content = Utililies.parseTemplate(content, "FeildIfList", getFeildIfList());
			content = Utililies.parseTemplate(content, "FeildJoin", getFeildJoin());
			content = Utililies.parseTemplate(content, "FeildMapJoin", getFeildMapJoin());
			content = Utililies.parseTemplate(content, "FeildIfJoin", getFeildIfJoin());
			content = Utililies.parseTemplate(content, "FeildIfMapJoin", getFeildIfMapJoin());
			content = Utililies.parseTemplate(content, "FeildIfSetList", getFeildIfSetList());
			content = Utililies.parseTemplate(content, "FeildSetList", getFeildSetList());
			content = Utililies.parseTemplate(content, "ForeachFeildMapJoin", getForeachFeildMapJoin());
			
			Utililies.writeContentToFile(javaPath, content);
		} catch (Exception e) {
			result = ca.getMapName() + ".xml生成失败：" + e.getMessage();
		}
		return result;
	}
	
	public String createNewMapper() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getMapFilePath());
        if(javaPath != null && javaPath.indexOf("main/java/main/") != -1){
            javaPath = Utililies.parseFilePath(ca.getSaveDir2(), ca.getMapFilePath());
        }
		String result = ca.getMapName() + ".xml生成完成：" + javaPath;
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() +"Mapper.xml已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("new_mapper.tlp"));
				// package
				content = Utililies.parseTemplate(content, "MapperPackage", ca.getDaoPackage());
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
				content = Utililies.parseTemplate(content, "PrimaryJdbcType", Utililies.getMybatisJdbcType(getPrimaryJdbcType()));
				content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
				content = Utililies.parseTemplate(content, "FeildMapList", getFeildMapList());
				content = Utililies.parseTemplate(content, "FeildJoinId", getFeildJoinId());
				
				Utililies.writeContentToFile(javaPath, content);
			}
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleElement(javaFile, Consts.selectMappeXml(tableName, methondBys, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKMapperParams().split(Constants.CHAR_AND);
						content = createSingleElement(javaFile, Consts.selectMappeXmlUnion(tableName, split[0], split[1]));
					} else {
						content = createSingleElement(javaFile, Consts.selectMappeXml(tableName));
						content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
						content = Utililies.parseTemplate(content, "PrimaryJdbcType", Utililies.getMybatisJdbcType(getPrimaryJdbcType()));
						content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleElement(javaFile, Consts.findMappeXml(tableName, methondBys, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKMapperParams().split(Constants.CHAR_AND);
						content = createSingleElement(javaFile, Consts.findMappeXmlUnion(tableName, split[0], split[1]));
					} else {
						content = createSingleElement(javaFile, Consts.findMappeXml(tableName));
						content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
						content = Utililies.parseTemplate(content, "PrimaryJdbcType", Utililies.getMybatisJdbcType(getPrimaryJdbcType()));
						content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if(ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleElement(javaFile, Consts.countMappeXml(tableName, methondBys, methondParam));
					content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
					content = Utililies.parseTemplate(content, "PrimaryJavaType", getPrimaryJavaType());
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = createSingleElement(javaFile, Consts.selectTotalMappeXml(tableName));
					content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
					content = Utililies.parseTemplate(content, "PrimaryJavaType", getPrimaryJavaType());
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getPageOp()) {
				String content = createSingleElement(javaFile, Consts.selectPageMappeXml(tableName));
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				Files.write(content.getBytes(), javaFile);
				
				/*content = createSingleElement(javaFile, Consts.selectTotalMappeXml(tableName));
				content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
				content = Utililies.parseTemplate(content, "PrimaryJavaType", getPrimaryJavaType());
				Files.write(content.getBytes(), javaFile);*/
			}
			if (ca.getAddOp()) {
				String content = createSingleElement(javaFile, Consts.insertMappeXml(tableName));
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "FeildJoin", getFeildJoin());
				content = Utililies.parseTemplate(content, "FeildMapJoin", getFeildMapJoin());
				
				// 如果是联合主键 ，不生成序列
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() < 2) {
					StringBuffer sb = new StringBuffer();
					if(DBHelper.sections.equals(OracleSetPanel.sections)){
						String[] curColumn = getCurColumnKey();
						 
						sb.append("<selectKey keyProperty=\""+ CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0]) +"\" resultType=\"java.lang."+ Utililies.getJavaType(curColumn[1]) +"\" order=\"BEFORE\">");
						sb.append(Consts.ENTER);
						sb.append(Consts.TAB3 + "select "+ tableName +"_SEQUENCE.nextval from dual");
						sb.append(Consts.ENTER);
						sb.append(Consts.TAB2 + "</selectKey>");
					}
					content = Utililies.parseTemplate(content, "SelectKey", StringUtils.isNoneEmpty(sb.toString()) ? sb.toString() : StringUtils.EMPTY);
				} else {
					content = Utililies.parseTemplate(content, "SelectKey", StringUtils.EMPTY);
				}
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()){
				String content = createSingleElement(javaFile, Consts.updateMappeXml(tableName));
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "FeildSetList", getFeildSetList());
				content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
				content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
				content = Utililies.parseTemplate(content, "PrimaryJdbcType", Utililies.getMybatisJdbcType(getPrimaryJdbcType()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				String content = StringUtils.EMPTY;
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKMapperParams().split(Constants.CHAR_AND);
					content = createSingleElement(javaFile, Consts.deleteMappeXmlUnion(tableName, split[0], split[1]));
				} else {
					content = createSingleElement(javaFile, Consts.deleteMappeXml(tableName));
					content = Utililies.parseTemplate(content, "PrimaryColumn", getPrimaryColumn());
					content = Utililies.parseTemplate(content, "PrimaryJdbcType", Utililies.getMybatisJdbcType(getPrimaryJdbcType()));
					content = Utililies.parseTemplate(content, "PrimaryFeild", CommonUtils.convertToFirstLetterLowerCaseCamelCase(getPrimaryColumn()));
				}
				Files.write(content.getBytes(), javaFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = ca.getMapName() + ".xml生成失败：" + e.getMessage();
		}
		return result;
	}
	
	private String getForeachFeildMapJoin(){
	    StringBuffer feildSetList = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                /*#{item.rowId,jdbcType=BIGINT}, */
                String map = "#{item.{0},jdbcType={1}}, \r\n";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        if(Objects.isNull(mdc.getExtra()) || !mdc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildSetList.append(Consts.TAB3).append(
                                CommonUtils.format(
                                    map, 
                                    CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                    mdc.getDataType()
                                )
                            );
                        }
                    } else {
                        feildSetList.append(Consts.TAB2).append(
                            CommonUtils.format(
                                map, 
                                CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                mdc.getDataType()
                            )
                        );
                    }
                }
            }
        }
        return feildSetList.toString().replaceFirst(", $", StringUtils.EMPTY);
	}

	private String getFeildSetList(){
	    StringBuffer feildSetList = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                /*APP_SYS_NAME = #{appSysName,jdbcType=VARCHAR},*/
                String map = "{0} = #{{1},jdbcType={2}}, \r\n";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(!mdc.isPrimaryKey()){
                        feildSetList.append(Consts.TAB2).append(
                            CommonUtils.format(map, mdc.getColumnName(), 
                            		CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                            		Utililies.getMybatisJdbcType(mdc.getDataType())
                            )
                        );
                    }
                }
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                /*APP_SYS_NAME = #{appSysName,jdbcType=VARCHAR},*/
                String map = "{0} = #{{1},jdbcType={2}}, \r\n";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn odc = (OracleDbColumn) columnList.get(i);
                    if(!odc.isPrimaryKey()){
                        feildSetList.append(Consts.TAB2).append(
                            CommonUtils.format(map, odc.getColumnName(), 
                            		CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName()),
                            		Utililies.getMybatisJdbcType(odc.getDataType())
                            )
                        );
                    }
                }
            }
        }
        return feildSetList.toString().replaceFirst(", $", StringUtils.EMPTY);
	}

	private String getFeildIfSetList(){
	    StringBuffer feildIfSetList = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                /*<if test="appSysCode != null">
                    APP_SYS_CODE = #{appSysCode,jdbcType=VARCHAR},
                  </if>*/
                StringBuffer feild = new StringBuffer(Consts.TAB2);
                feild.append("<if test=\"{0} != null\">\r\n")
                    .append(Consts.TAB3).append("{1} = #{{0},jdbcType={2}},\r\n")
                    .append(Consts.TAB2).append("</if>\r\n");
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    if(!mdc.isPrimaryKey()){
                        feildIfSetList.append(
                            CommonUtils.format(
                                feild.toString(), 
                                CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                mdc.getColumnName(),
                                mdc.getDataType()
                            )
                        );
                    }
                }
            }
        }
        return feildIfSetList.toString();
	}
	
	private String getFeildIfMapJoin(){
	    StringBuffer feildIfMapJoin = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                /*<if test="appSysCode != null">
                    #{appSysCode,jdbcType=VARCHAR},
                  </if>*/
                StringBuffer feild = new StringBuffer(Consts.TAB2);
                feild.append("<if test=\"{0} != null\">\r\n")
                    .append(Consts.TAB3).append("#{{0},jdbcType={1}},\r\n")
                    .append(Consts.TAB2).append("</if>\r\n");
                String pmap = "#{{0},jdbcType={1}},\r\n";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        if(Objects.isNull(mdc.getExtra()) || !mdc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildIfMapJoin.append(CommonUtils.format(
                                    pmap, 
                                    CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                    mdc.getDataType()
                                ));
                        }
                    } else {
                        feildIfMapJoin.append(
                            CommonUtils.format(
                                feild.toString(), 
                                CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                mdc.getDataType()
                            )
                        );
                    }
                }
            }
        }
        return feildIfMapJoin.toString();
	}
	
	private String getFeildIfJoin(){
	    StringBuffer feildIfList = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                /*<if test="appSysName != null">
                    APP_SYS_NAME,
                  </if>*/
                StringBuffer feild = new StringBuffer(Consts.TAB2);
                feild.append("<if test=\"{0} != null\">\r\n")
                    .append(Consts.TAB3).append("{1},\r\n")
                    .append(Consts.TAB2).append("</if>\r\n");
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        if(Objects.isNull(mdc.getExtra()) || !mdc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildIfList.append(Consts.TAB3)
                            .append(CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()))
                            .append(mdc.getColumnName())
                            .append(",\r\n");
                        }
                    } else {
                        feildIfList.append(
                            CommonUtils.format(
                                feild.toString(), 
                                CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                                mdc.getColumnName()
                            )
                        );
                    }
                }
            }
        }
        return feildIfList.toString();
	}
	
	private String getFeildMapJoin(){
	    StringBuffer feildMapJoin = new StringBuffer(Consts.TAB3);
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                /*#{rowId,jdbcType=BIGINT}*/
                String map = "#{{0},jdbcType={1}}";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        if(Objects.isNull(mdc.getExtra()) || !mdc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildMapJoin.append(
                                CommonUtils.format(map,  CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()), Utililies.getMybatisJdbcType(mdc.getDataType()))
                            ).append(", ");
                        }
                    } else {
                        feildMapJoin.append(
                            CommonUtils.format(map, CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()), Utililies.getMybatisJdbcType(mdc.getDataType()))
                        ).append(", ");
                    }
                }
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                /*#{rowId,jdbcType=BIGINT}*/
                String map = "#{{0},jdbcType={1}}";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn odc = (OracleDbColumn) columnList.get(i);
                    if(odc.isPrimaryKey()){
                        if(Objects.isNull(odc.getExtra()) || !odc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildMapJoin.append(
                                CommonUtils.format(map,  CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName()), Utililies.getMybatisJdbcType(odc.getDataType()))
                            ).append(", ");
                        }
                    } else {
                        feildMapJoin.append(
                            CommonUtils.format(map, CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName()), Utililies.getMybatisJdbcType(odc.getDataType()))
                        ).append(", ");
                    }
                }
            }
        }
        return feildMapJoin.toString().replaceFirst(", $", StringUtils.EMPTY);
	}
	
	private String getFeildJoin(){
	    StringBuffer feildJoin = new StringBuffer(Consts.TAB3);
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        if(Objects.isNull(mdc.getExtra()) || !mdc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildJoin.append(mdc.getColumnName()).append(", ");
                        }
                    } else {
                        feildJoin.append(mdc.getColumnName()).append(", ");
                    }
                }
                feildJoin.append(Consts.ENTER);
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn odc = (OracleDbColumn) columnList.get(i);
                    if(odc.isPrimaryKey()){
                        if(Objects.isNull(odc.getExtra()) || !odc.getExtra().equalsIgnoreCase("auto_increment")){
                            feildJoin.append(odc.getColumnName()).append(", ");
                        }
                    } else {
                        feildJoin.append(odc.getColumnName()).append(", ");
                    }
                }
                feildJoin.append(Consts.ENTER);
            }
        }
        return feildJoin.toString().replaceFirst(", $", StringUtils.EMPTY);
	}
	
	private String getFeildIfList(){
	    StringBuffer feildIfList = new StringBuffer();
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                /*<if test="groupId != null" >
                    and group_id = #{groupId,jdbcType=BIGINT}
                </if>*/
                StringBuffer feild = new StringBuffer(Consts.TAB2);
                feild.append("<if test=\"{0} != null\" >\r\n")
                    .append(Consts.TAB3).append("and {1} = #{{2},jdbcType={3}}\r\n")
                    .append(Consts.TAB2).append("</if>\r\n");
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    feildIfList.append(
                        CommonUtils.format(
                            feild.toString(), 
                            CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                            mdc.getColumnName(), 
                            CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()),
                            mdc.getDataType()
                        )
                    );
                }
            }
        }
        return feildIfList.toString();
	}
	
	private String getFeildJoinId(){
	    StringBuffer feildJoinId = new StringBuffer();
	    if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    feildJoinId.append(mdc.getColumnName());
                    if(i < (k - 1)){
                        feildJoinId.append(", ");
                    }
                }
                feildJoinId.append(Consts.ENTER);
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn odc = (OracleDbColumn) columnList.get(i);
                    feildJoinId.append(odc.getColumnName());
                    if(i < (k - 1)){
                        feildJoinId.append(", ");
                    }
                }
                feildJoinId.append(Consts.ENTER);
            }
        }
	    return feildJoinId.toString();
	}
	
	private String getFeildMapList(){
	    StringBuffer feildMapList = new StringBuffer();
	    if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                //<id column="{PrimaryColumn}" jdbcType="{PrimaryJdbcType}" property="{PrimaryFeild}" />
                String maps = "<result column=\"{0}\" jdbcType=\"{1}\" property=\"{2}\" />";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(!mdc.isPrimaryKey()){
                        feildMapList.append(Consts.TAB2)
                            .append(CommonUtils.format(maps, mdc.getColumnName(), Utililies.getMybatisJdbcType(mdc.getDataType()), CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName())))
                            .append(Consts.ENTER);
                    }
                }
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                //<id column="{PrimaryColumn}" jdbcType="{PrimaryJdbcType}" property="{PrimaryFeild}" />
                String maps = "<result column=\"{0}\" jdbcType=\"{1}\" property=\"{2}\" />";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn odc = (OracleDbColumn) columnList.get(i);
                    if(!odc.isPrimaryKey()){
                        feildMapList.append(Consts.TAB2)
                            .append(CommonUtils.format(maps, odc.getColumnName(), Utililies.getMybatisJdbcType(odc.getDataType()), CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName())))
                            .append(Consts.ENTER);
                    }
                }
            }
        }
	    return feildMapList.toString();
	}
	
}
