/**
 * 
 */
package com.ccrc.tiger.creator;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

/**
 * @author Jalen
 * 创建 javascript 文件
 */
public class CreateJavaScript extends Creator {

	public CreateJavaScript(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}

	public String createJavaSript() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jsPath = Utililies.parseFilePath(tmpPath, ca.getJsFilePath());
        String result = "jsPath文件生成完成：" + jsPath;
        try {
            String content = Utililies.readResourceFile(dir + "js.tlp");
            // js 对象名
            content = Utililies.parseTemplate(content, "jsObj", StringUtils.uncapitalize(getEntityName()));
            content = Utililies.parseTemplate(content, "jsClazz", getEntityName());
            
			// 判断是否是联合主键，如果是 通过联合主键查询
			if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
				List<String> datas = Lists.newArrayList();
				for(DbTableColumn dtc : primaryKeyColumns) {
					if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
						 MysqlDbColumn mdc = (MysqlDbColumn) dtc;
						 String field = CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName());
						 datas.add(field + ": row."+ field);
					}
					if(DBHelper.sections.equals(OracleSetPanel.sections)){
						OracleDbColumn odc = (OracleDbColumn) dtc;
						 String field = CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName());
						datas.add(field + ": row."+ field);
					}
				}
				String jsonDatas = Joiner.on(", ").join(datas);
				content = Utililies.parseTemplate(content, "JsonDatas", jsonDatas);
			} else {
				String[] curColumn = getCurColumnKey();
				String id = CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0]);
				String jsonDatas = id + ": row." + id;
				content = Utililies.parseTemplate(content, "JsonDatas", jsonDatas);
			}
            Utililies.writeContentToFile(jsPath, content);
        } catch (Exception e) {
        	e.printStackTrace();
            result = "jsPath文件生成失败：" + e.getMessage();
        }
        return result;
	}
	
}
