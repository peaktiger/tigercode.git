/**
 * 
 */
package com.ccrc.tiger.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 * @author Jalen
 *
 */
@SuppressWarnings("serial")
public class SpringHibernatePanel extends BasePanel {
	
	private int width = 900;
	private int height = 600;
	
	public SpringHibernatePanel(int width, int height) {
		this.width = width;
		this.height = height;
		initComponent();
	}
	
	public void initComponent() {
		this.setSize(new Dimension(width, height - 30));
		this.setLayout(null);
		this.setBackground(Color.WHITE);

		JButton close = new JButton("X");
		close.setBounds(width - 45, 2, 30, 20);
		this.add(close);
		close.addActionListener(new SpringHibernateActionListener(this));

		JLabel headerLabel = new JLabel("SpringMVC + Spring + Hibernate集成开发");
		headerLabel.setBounds(10, 2, 250, 25);
		
		StringBuffer html = new StringBuffer()
				.append("<html>SpringMVC + Spring + Hibernate框架以Spring版本为4.3.5.RELEASE和Hibernate版本为5.2.5.Final集成介绍。<br><br>")
				.append("<strong>1.</strong>创建项目工程<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>2.</strong>下载依赖工程<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>3.</strong>替换项目的pom文件或引入项目依赖jar包<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>4.</strong>引入项目配置文件和配置web.xml<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>5.</strong>使用TigerCoder工具生成代码<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>6.</strong>工程发布Tomcat服务器<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br><br>")
				.append("<strong>7.</strong>访问项目地址<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍####################################################################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍##########################################<br>")
				.append("介绍############</html>");
		
		JLabel mainLabel = new JLabel(html.toString());
		
		JScrollPane infoJsp = new JScrollPane(mainLabel);
        infoJsp.setBounds(5, 28, width - 20, height - 68);
        infoJsp.getViewport().setBackground(Color.WHITE);
        this.add(infoJsp);
        infoJsp.setVisible(true);
        
		this.add(headerLabel);
	}

	private class SpringHibernateActionListener implements ActionListener {
		private SpringHibernatePanel shPanel = null;

		public SpringHibernateActionListener(SpringHibernatePanel shPanel) {
			this.shPanel = shPanel;
		}

		public void actionPerformed(ActionEvent e) {
			shPanel.setVisible(false);
		}
	}

}

