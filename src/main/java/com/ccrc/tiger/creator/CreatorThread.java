package com.ccrc.tiger.creator;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.ccrc.tiger.db.DbCache;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.main.MainTiger;
import com.ccrc.tiger.util.CommonUtils;

public class CreatorThread extends Thread {
	private final static Logger LOG = Logger.getLogger(CreatorThread.class);
			
	List<String> tables = null;
	CreateAttr ca = null;

	public CreatorThread(List<String> tables, CreateAttr ca) {
		this.tables = tables;
		this.ca = ca;
	}

	public void run() {
	    MainTiger.mainTiger.showProcessPanel();
		if(tables.size() == 0){
			//tables.add(Utililies.entityToTable(ca.getEntityName()));
			tables.add(CommonUtils.convertToCamelCase(ca.getEntityName()));
		}
		List<DbTableColumn> columnList = null;
		String tableName = null;
		CreateAttr creAtt = null;
		for (int i = 0, k = tables.size(); i < k; i++) {
			tableName = tables.get(i);
			columnList = DbCache.mysqlDbColumnMap.get(tableName);
			creAtt = ca.clone();
			new Creator(creAtt, tableName, columnList).run();
		}
		// 执行完成
		MainTiger.mainTiger.hideProcessPanel();
		Toolkit.getDefaultToolkit().beep();
        JOptionPane.showMessageDialog(null, "恭喜，生成完成\n路径：" + ca.getSaveDir(), "提示", JOptionPane.ERROR_MESSAGE);
        try {
            Desktop.getDesktop().open(new File(ca.getSaveDir())); 
        } catch (Exception e) {
        	LOG.error("生成失败！", e);
        }
	}

}
