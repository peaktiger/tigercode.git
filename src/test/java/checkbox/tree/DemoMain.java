package checkbox.tree;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import com.google.common.base.Throwables;

public class DemoMain {
	public static void main(String[] args) {
		try {
			// 设置本属性将改变窗口边框样式定义
			System.setProperty("sun.java2d.noddraw", "true");
			BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
			BeautyEyeLNFHelper.launchBeautyEyeLNF();
			UIManager.put("RootPane.setupButtonVisible", false);
			// 统一设置字体
			// Font font = new Font("宋体", Font.PLAIN, 15);
			// Font font = new Font("alias", Font.PLAIN, 12);
			// InitGlobalFont(font);
		} catch (Exception e) {
			Throwables.throwIfUnchecked(e);
		}
		JFrame frame = new JFrame("CheckBoxTreeDemo");
		frame.setBounds(200, 200, 400, 400);
		JTree tree = new JTree();
		/*CheckBoxTreeNode rootNode = new CheckBoxTreeNode("root");
		CheckBoxTreeNode node1 = new CheckBoxTreeNode("node_1");
		CheckBoxTreeNode node1_1 = new CheckBoxTreeNode("node_1_1");
		CheckBoxTreeNode node1_2 = new CheckBoxTreeNode("node_1_2");
		CheckBoxTreeNode node1_3 = new CheckBoxTreeNode("node_1_3");
		node1.add(node1_1);
		node1.add(node1_2);
		node1.add(node1_3);
		CheckBoxTreeNode node2 = new CheckBoxTreeNode("node_2");
		CheckBoxTreeNode node2_1 = new CheckBoxTreeNode("node_2_1");
		CheckBoxTreeNode node2_2 = new CheckBoxTreeNode("node_2_2");
		node2.add(node2_1);
		node2.add(node2_2);
		rootNode.add(node1);
		rootNode.add(node2);*/
		
		DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("root");
		DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("node_1");
		CheckBoxTreeNode node1_1 = new CheckBoxTreeNode("node_1_1");
		CheckBoxTreeNode node1_2 = new CheckBoxTreeNode("node_1_2");
		CheckBoxTreeNode node1_3 = new CheckBoxTreeNode("node_1_3");
		node1.add(node1_1);
		node1.add(node1_2);
		node1.add(node1_3);
		DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("node_2");
		CheckBoxTreeNode node2_1 = new CheckBoxTreeNode("node_2_1");
		CheckBoxTreeNode node2_2 = new CheckBoxTreeNode("node_2_2");
		node2.add(node2_1);
		node2.add(node2_2);
		rootNode.add(node1);
		rootNode.add(node2);
		
		DefaultTreeModel model = new DefaultTreeModel(rootNode);
		tree.addMouseListener(new CheckBoxTreeNodeSelectionListener());
		tree.setModel(model);
		tree.setCellRenderer(new CheckBoxTreeCellRenderer());
		JScrollPane scroll = new JScrollPane(tree);
		scroll.setBounds(0, 0, 300, 320);
		frame.getContentPane().add(scroll);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
