package group2;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

public class Quarter {
	
	 	public static void main(String[] args) {
	        QuarterBean[] quarters = getRecentQuarters(12);
	        for(int i = 0; i < quarters.length; i++) {
	            System.out.println(quarters[i].getQuarter());
	            System.out.println(quarters[i].toString());
	            
	            System.out.println(quarters[i].getLastMonthOfQuarter());
	            
	            System.out.println(DateFormatUtils.format(quarters[i].getLastDateOfQuarter(), "yyyy-MM"));
	        }
	    }
	     
	    /**
	     * 获得最近的季度
	     * @param count     需要获得季度的数量
	     * @return
	     */
	    public static QuarterBean[] getRecentQuarters(int count) {
	        if(count < 1) {
	            throw new IllegalArgumentException("count must be great than 0");
	        }
	        QuarterBean[] quarters = new QuarterBean[count];
	        
	        quarters[0] = getCurrentQuarter();
	        for(int i = 1; i < quarters.length; i++) {
	            quarters[i] = quarters[i - 1].getPastQuarter();
	        }
	        return quarters;
	    }
	     
	    /**
	     * 获得当前季度
	     * @return
	     */
	    public static QuarterBean getCurrentQuarter() {
	        Calendar calendar = Calendar.getInstance();
	        int year = calendar.get(Calendar.YEAR);
	        int month = calendar.get(Calendar.MONTH) + 1;
	        return new QuarterBean(year, month, getQuarter(month));
	    }
	     
	    /**
	     * 通过月份计算季度
	     * @param month
	     * @return
	     */
	    public static int getQuarter(int month) {
	        if(month < 1 || month > 12) {
	            throw new IllegalArgumentException("month is invalid.");
	        }
	        return (month - 1) / 3 + 1;
	    }
	    
	}
	 


	class QuarterBean {
	     
	    public final static int QUARTERS_EVERY_YEAR = 4;    
	    private final static String[] QUARTERS = {
	            "Q1 {0}(1 - 3)",
	            "Q2 {0}(4 - 6)",
	            "Q3 {0}(7 - 9)",
	            "Q4 {0}(10 - 12)"       
	        };
	     
	    private int year;
	    private int month;
	    private int quarter;   
	    
	    private int lastMonthOfQuarter;
	    
	    private Date lastDateOfQuarter;
	    
	    private QuarterBean() {
	    }
	     
	    public QuarterBean(int year, int month, int quarter) {
	        if(quarter < 1 || quarter > QUARTERS_EVERY_YEAR) {
	            throw new IllegalArgumentException("quarter is invalid.");
	        }
	        this.year = year;
	        this.month = month;
	        this.quarter = quarter;
	    }
	     
	    public void setYear(int year) {
			this.year = year;
		}

		public int getYear() {
	        return year;
	    }
	    
	    public int getMonth() {
			return month;
		}

		public void setMonth(int month) {
			this.month = month;
		}
		
		public void setQuarter(int quarter) {
			this.quarter = quarter;
		}

		public int getQuarter() {
	        return quarter;
	    }   
		
		public int getLastMonthOfQuarter() {
	    	return lastMonthOfQuarter;
	    }
		
		public Date getLastDateOfQuarter() {
			return lastDateOfQuarter;
		}
	     
	    /**
	     * 获得上一季度
	     * @return
	     */
	    public QuarterBean getPastQuarter() {
	        QuarterBean bean = new QuarterBean();
	        int y = (QUARTERS_EVERY_YEAR + 1 - quarter) / QUARTERS_EVERY_YEAR;
	        bean.year = year - y;
	        bean.month = month;
	        bean.quarter = y * QUARTERS_EVERY_YEAR + (quarter - 1);
	        
	        bean.lastMonthOfQuarter = bean.quarter * 3;
	        
	        Calendar c = Calendar.getInstance();
	        c.set(bean.year, bean.month, Calendar.DATE);
	        bean.lastDateOfQuarter = c.getTime();
	        return bean;
	    }
	    
	    public String toString() {
	        return MessageFormat.format(QUARTERS[quarter - 1], String.valueOf(year +"-" +month));
	    }
}
