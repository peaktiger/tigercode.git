/**
 * 
 */
package tigercode;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.FileUtils;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.io.Files;

/**
 * @author Jalen
 *
 */
public class ReadJavaFileTest {
	
	@Test
	public void testReadImplFile () {
		String testFilePath = "D:/neonWorkspace/tigercode/src/main/java/com/ccrc/tiger/util/Test.java";
        File testFile = new File(testFilePath);
        try {
        	 List<String> lines = Files.readLines(testFile, Charsets.UTF_8);
        	 int lastIndex = lines.lastIndexOf(Consts.BRACE_RIGHT);
        	 System.out.println(lastIndex);
        	 lines.addAll(lastIndex, Consts.addImplTmp("user"));
             for (String line : lines) {
                 System.out.println(line);
             }
             
             String contents = Joiner.on(StringUtils.LF).join(lines);
             System.out.println("\n############################################\n");
             System.out.println(contents);
             
             FileUtils.writeFile(contents, testFilePath);
        } catch (Exception e) {
			e.printStackTrace();
		}
       
	}
	
	@Test
	public void testReadInterfaceFile() {
		String testFilePath = "D:/neonWorkspace/tigercode/src/test/java/tigercode/IUser.java";
		File testFile = new File(testFilePath);
		try {
			List<String> lines = Files.readLines(testFile, Charsets.UTF_8);
			int lastIndex = lines.lastIndexOf(Consts.BRACE_RIGHT);
			System.out.println(lastIndex);
			lines.addAll(lastIndex, Consts.addInterfaceTmp("user"));
			for (String line : lines) {
				System.out.println(line);
			}

			String contents = Joiner.on(StringUtils.LF).join(lines);
			System.out.println("\n############################################\n");
			System.out.println(contents);

			FileUtils.writeFile(contents, testFilePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCapitalize() {
		System.out.println(StringUtils.capitalize("abcdefg"));
		System.out.println(StringUtils.uncapitalize("Abcdefg"));
	}

}
