package checkbox.tree;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import com.ccrc.tiger.componet.CheckBoxNode;
import com.ccrc.tiger.componet.CheckBoxNodeEditor;
import com.ccrc.tiger.componet.CheckBoxNodeRenderer;
import com.ccrc.tiger.componet.NamedVector;
import com.google.common.base.Throwables;

public class CheckBoxNodeTreeSample {

	private static JTree tree;

	public static void main(String args[]) {
		try {
			// 设置本属性将改变窗口边框样式定义
			System.setProperty("sun.java2d.noddraw", "true");
			BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
			BeautyEyeLNFHelper.launchBeautyEyeLNF();
			UIManager.put("RootPane.setupButtonVisible", false);
			// 统一设置字体
			// Font font = new Font("宋体", Font.PLAIN, 15);
			// Font font = new Font("alias", Font.PLAIN, 12);
			// InitGlobalFont(font);
		} catch (Exception e) {
			Throwables.throwIfUnchecked(e);
		}
		
		JFrame frame = new JFrame("CheckBox Tree");

		CheckBoxNode accessibilityOptions[] = {
				new CheckBoxNode("Move system caret with focus/selection changes", false),
				new CheckBoxNode("Always expand alt text for images", true) };
		CheckBoxNode browsingOptions[] = { 
				new CheckBoxNode("Notify when downloads complete", true),
				new CheckBoxNode("Disable script debugging", true), 
				new CheckBoxNode("Use AutoComplete", true),
				new CheckBoxNode("Browse in a new process", false) };
		
		Vector<Object> accessVector = new NamedVector("Accessibility", accessibilityOptions);
		Vector<Object> browseVector = new NamedVector("Browsing", browsingOptions);
		Object rootNodes[] = { accessVector, browseVector };
		Vector<Object> rootVector = new NamedVector("Root", rootNodes);
		
		tree = new JTree(rootVector);

		CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
		tree.setCellRenderer(renderer);

		tree.setCellEditor(new CheckBoxNodeEditor(tree));
		tree.setEditable(true);

		JScrollPane scrollPane = new JScrollPane(tree);
		frame.getContentPane().add(scrollPane, BorderLayout.NORTH);

		JPanel buttonPanel = new JPanel();
		JButton button = new JButton("new node");
		buttonPanel.add(button);
		frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
				DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
				DefaultMutableTreeNode newNode = new DefaultMutableTreeNode("New node");
				root.add(newNode);
				model.reload();

			}
		});

		frame.setSize(300, 450);
		frame.setVisible(true);
	}
}
