package com.ccrc.tiger.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ccrc.tiger.util.Objects;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.ccrc.tiger.entity.DbTable;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.IniReader;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class DBHelper {

    private static DBConnection dbconn = new DBConnection();

    public static String ip = "192.168.10.51";
    public static String port = "3306";
    public static String schema = "test";
    public static String encode = "utf-8";
    public static String userName = "root";
    public static String pwd = "root";
    public static String sections = StringUtils.EMPTY;

    public static void initDbConfig() {
        try {
            IniReader reader = IniReader.getIniReader();
            String currdbType = reader.getCurrdbType();
            if(Objects.nonNull(currdbType)){
	            if (currdbType.equals("myType")) {
	            	dbconn.setDriver(Constants.MYSQL_DRIVER);
	            	dbconn.setUrl(Constants.MYSQL_URL);
	            }
	            if (currdbType.equals("orcType")) {
	            	dbconn.setDriver(Constants.ORACLE_DRIVER);
	            	dbconn.setUrl(Constants.ORACLE_URL);
	            }
	            sections = reader.getValue("use_db_type", "db", currdbType);
	            if (StringUtils.isEmpty(sections)) {
	                sections = MySqlSetPanel.sections;
	            }
	            HashMap<String, String> cfg = reader.getActiveCfg(sections);
	            if (MapUtils.isNotEmpty(cfg)) {
	                setConfigValue(cfg);
	            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setConfigValue(HashMap<String, String> cfg) {
        ip = cfg.get("host");
        port = cfg.get("port");
        schema = cfg.get("db_name");
        encode = cfg.get("encode");
        userName = cfg.get("userName");
        pwd = cfg.get("password");
        dbconn.setDbInfo(ip, port, schema, encode, userName, pwd);
    }

    public static void main(String[] args) {
        List<MysqlDbColumn> tableList = getMySqlTableColumns("community", "cmu_group");
        System.out.println(tableList);
    }

    public static List<DbTable> getMysqlTables(String schema) {
        List<DbTable> tableList = null;
        String sql = "SELECT * FROM information_schema.tables WHERE table_schema = ?";
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sql);
            if (Objects.nonNull(stmt)) {
            	stmt.setString(1, schema);
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                    tableList = Lists.newArrayList();
                    DbTable table = null;
                    while (rs.next()) {
                        table = new DbTable();
                        table.setTableSchema(schema)
                        		.setTableName(rs.getString("table_name"))
                                .setTableComment(rs.getString("table_comment"))
                        		.setCreateTime(rs.getTimestamp("create_time"));
                        tableList.add(table);
                    }
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableList;
    }
    
    public static List<DbTable> getOracleTables(String schema) {
        List<DbTable> tableList = null;
        String sql = "select a.TABLE_NAME as table_name, b.COMMENTS as table_comment from user_tables a, user_tab_comments b WHERE a.TABLE_NAME = b.TABLE_NAME order by TABLE_NAME";
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sql);
            if (Objects.nonNull(stmt)) {
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                    tableList = Lists.newArrayList();
                    DbTable table = null;
                    while (rs.next()) {
                        table = new DbTable();
                        table.setTableSchema(schema)
                        		.setTableName(rs.getString("table_name"))
                                .setTableComment(rs.getString("table_comment"));
                        tableList.add(table);
                    }
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableList;
    }

    public static List<MysqlDbColumn> getMySqlTableColumns(String schema, String table) {
        List<MysqlDbColumn> columnList = Lists.newArrayList();
        String sql = "SELECT DISTINCT * FROM information_schema.COLUMNS WHERE table_schema = ? AND table_name = ? ORDER BY ORDINAL_POSITION";
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sql);
            if (Objects.nonNull(stmt)) {
                stmt.setString(1, schema);
                stmt.setString(2, table);
                
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                    MysqlDbColumn column = null;
                    while (rs.next()) {
                        column = new MysqlDbColumn();
                        column.setTableSchema(schema).setTableName(table).setColumnName(rs.getString("column_name"))
                                .setColumnType(rs.getString("column_type"))
                                .setColumnDefault(rs.getObject("column_default"))
                                .setColumnKey(rs.getString("column_key"))
                                .setColumnComment(rs.getString("column_comment")).setDataType(rs.getString("data_type"))
                                .setCharacterMaximumLength(rs.getInt("character_maximum_length"))
                                .setCharacterOctetLength(rs.getInt("character_octet_length"))
                                .setExtra(rs.getString("extra")).setIsNullable(rs.getString("is_nullable"))
                                .setPrivileges(rs.getString("privileges"))
                                .setOrdinalPosition(rs.getInt("ordinal_position"));
                        columnList.add(column);
                    }
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columnList;
    }
    
    public static List<MysqlDbColumn> getOracleTableColumns(String schema, String table) {
        List<MysqlDbColumn> columnList = Lists.newArrayList();
        String sql = "SELECT DISTINCT * FROM information_schema.COLUMNS WHERE table_schema = ? AND table_name = ? ORDER BY ORDINAL_POSITION";
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sql);
            if (Objects.nonNull(stmt)) {
                stmt.setString(1, schema);
                stmt.setString(2, table);
                
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                    MysqlDbColumn column = null;
                    while (rs.next()) {
                        column = new MysqlDbColumn();
                        column.setTableSchema(schema).setTableName(table).setColumnName(rs.getString("column_name"))
                                .setColumnType(rs.getString("column_type"))
                                .setColumnDefault(rs.getObject("column_default"))
                                .setColumnKey(rs.getString("column_key"))
                                .setColumnComment(rs.getString("column_comment")).setDataType(rs.getString("data_type"))
                                .setCharacterMaximumLength(rs.getInt("character_maximum_length"))
                                .setCharacterOctetLength(rs.getInt("character_octet_length"))
                                .setExtra(rs.getString("extra")).setIsNullable(rs.getString("is_nullable"))
                                .setPrivileges(rs.getString("privileges"))
                                .setOrdinalPosition(rs.getInt("ordinal_position"));
                        columnList.add(column);
                    }
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columnList;
    }
    
    public static Map<String, List<DbTableColumn>> getMySqlMutilTableColumns(String schema) {
    	Map<String, List<DbTableColumn>> tableMap = new HashMap<String, List<DbTableColumn>>();
        String sql = "SELECT DISTINCT * FROM information_schema.COLUMNS WHERE table_schema = ? AND table_name in ("
        		+ "SELECT table_name FROM information_schema.tables WHERE table_schema = ?"
        		+ ") ORDER BY table_name, ORDINAL_POSITION";
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sql);
            if (Objects.nonNull(stmt)) {
                stmt.setString(1, schema);
                stmt.setString(2, schema);
                
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                    MysqlDbColumn column = null;
                    List<DbTableColumn> columnList = null;
                    String tempName = StringUtils.EMPTY;
                    String tname = StringUtils.EMPTY;
                    while (rs.next()) {
                        column = new MysqlDbColumn();
                        tname = rs.getString("table_name");
                        if(!tempName.equals(tname)){
                        	if(CollectionUtils.isNotEmpty(columnList)){
                        		tableMap.put(tempName, columnList);
                        	}
                        	tempName = tname;
                        	columnList = Lists.newArrayList();
                        }
                        column.setTableSchema(schema)
                        	.setTableName(tname)
                        	.setTableComment(getCommentByTableName(tname))
                    		.setColumnName(rs.getString("column_name"))
                            .setColumnType(rs.getString("column_type"))
                            .setColumnDefault(rs.getObject("column_default"))
                            .setColumnKey(rs.getString("column_key"))
                            .setColumnComment(rs.getString("column_comment"))
                            .setDataType(rs.getString("data_type"))
                            .setCharacterMaximumLength(rs.getInt("character_maximum_length"))
                            .setCharacterOctetLength(rs.getInt("character_octet_length"))
                            .setExtra(rs.getString("extra"))
                            .setIsNullable(rs.getString("is_nullable"))
                            .setPrivileges(rs.getString("privileges"))
                            .setOrdinalPosition(rs.getInt("ordinal_position"));
                        columnList.add(column);
                    }
                    if(CollectionUtils.isNotEmpty(columnList)){
                		tableMap.put(tempName, columnList);
                	}
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableMap;
    }
    
    public static Map<String, List<DbTableColumn>> getOracleMutilTableColumns(String schema) {
    	Map<String, List<DbTableColumn>> tableMap = new HashMap<String, List<DbTableColumn>>();
    	String subSql = "select table_name from user_tables";
    	
    	StringBuffer sb = new StringBuffer();
    	sb.append("select t1.*, t2.constraint_name, t2.constraint_type, t2.status ");
    	sb.append("from ");
    	sb.append("(select t.*, c.comments ");
    	sb.append("from user_tab_columns t, user_col_comments c ");
    	sb.append("where t.column_Name=c.column_Name "); 
    	sb.append("and t.Table_Name in ("+ subSql +")) t1 ");
    	sb.append("left join ");
    	sb.append("(select b.table_name, b.constraint_name, b.constraint_type, b.status, a.column_Name ");
    	sb.append("from user_cons_columns a, user_constraints b ");
    	sb.append("where a.constraint_name = b.constraint_name ");
    	sb.append("and b.constraint_type ='P' ");
    	sb.append("and a.table_name in ("+ subSql +")) t2 ");
    	sb.append("on t1.column_Name=t2.column_Name ");
    	sb.append("order by t1.table_name, t1.column_Name");
        try {
            PreparedStatement stmt = dbconn.getPreparedStatement(sb.toString());
            if (Objects.nonNull(stmt)) {
            	//stmt.setString(0, Consts.ORACLE_COLUMN_KEY);
            	
                ResultSet rs = stmt.executeQuery();
                if (Objects.nonNull(rs)) {
                	OracleDbColumn column = null;
                    List<DbTableColumn> columnList = null;
                    String tempName = StringUtils.EMPTY;
                    String tname = StringUtils.EMPTY;
                    while (rs.next()) {
                        column = new OracleDbColumn();
                        tname = rs.getString("table_name");
                        if(!tempName.equals(tname)){
                        	if(CollectionUtils.isNotEmpty(columnList)){
                        		tableMap.put(tempName, columnList);
                        	}
                        	tempName = tname;
                        	columnList = Lists.newArrayList();
                        }
                        column.setTableSchema(schema)
                        	.setTableName(tname)
                        	.setTableComment(getOracleCommentByTableName(tname))
                    		.setColumnName(rs.getString("column_name"))
                            .setColumnType(rs.getString("data_type"))
                            //.setColumnDefault(rs.getObject("column_default"))
                            .setColumnKey(rs.getString("constraint_type"))
                            .setColumnComment(rs.getString("comments"))
                            .setDataType(rs.getString("data_type"))
                            .setCharacterMaximumLength(rs.getInt("data_length"));
                            
                            if(rs.getString("nullable").equalsIgnoreCase("N")) {
                            	column.setIsNullable(Consts.COLUMN_NULL_NO);
                            }
                            if(rs.getString("nullable").equalsIgnoreCase("Y")) {
                            	column.setIsNullable(Consts.COLUMN_NULL_YES);
                            }
                            
                            /*.setExtra(rs.getString("extra"))
                            .setPrivileges(rs.getString("privileges"))
                            .setOrdinalPosition(rs.getInt("ordinal_position"));*/
                        columnList.add(column);
                    }
                    if(CollectionUtils.isNotEmpty(columnList)){
                		tableMap.put(tempName, columnList);
                	}
                }
                dbconn.close(rs, stmt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableMap;
    }

    public static Map<String, Object> loadTableData(String sql) throws SQLException {
        Map<String, Object> map = null;
        Vector<Vector<Object>> dataVector = null;
        PreparedStatement stmt = dbconn.getPreparedStatement(sql);
        if (Objects.nonNull(stmt)) {
            ResultSet rs = stmt.executeQuery();
            if (Objects.nonNull(rs)) {
                map = Maps.newHashMap();
                dataVector = new Vector<Vector<Object>>();
                Vector<Object> vector = null;
                Vector<Object> column = null;
                Object value = null;
                int size = 0;
                int row = 1;
                while (rs.next()) {
                    vector = new Vector<Object>();
                    vector.add(row);
                    row++;
                    try {
                        if(Objects.isNull(column)){
                            column = resultSetMetaDataToVector(rs.getMetaData());
                            size =  column.size();
                        }
                        for (int i = 1; i < size; i++) {
                            value = rs.getObject(CommonUtils.objectToString(column.get(i)));
                            if (Objects.isNull(value)) {
                                vector.add("null");
                            } else {
                                vector.add(value);
                            }
                        }
                    } catch (Exception e) {
                    	Throwables.throwIfUnchecked(e);
                    }
                    dataVector.add(vector);
                }
                map.put("dataSet", dataVector);
                map.put("feildSet", column);
            }
            dbconn.close(rs, stmt);
        }
        return map;
    }

    private static Vector<Object> resultSetMetaDataToVector(ResultSetMetaData rsData) {
        Vector<Object> vector = null;
        try {
            int size = rsData.getColumnCount();
            vector = new Vector<Object>();
            vector.add("序号");
            for (int i = 0; i < size; i++) {
                vector.add(rsData.getColumnName(i + 1));
            }
        } catch (SQLException e) {
            vector = new Vector<Object>();
        }
        return vector;
    }
    
    /**
     * 查询表的注释
     * @param tableName
     * @return
     * @throws Exception
     */
    public static String getCommentByTableName(String tableName) {
    	Statement stmt = null;
    	ResultSet rs = null;
    	try {
    		stmt = dbconn.getStatement();
    	    rs = stmt.executeQuery("SHOW CREATE TABLE " + tableName);
    	    if(Objects.nonNull(rs) && rs.next()) {
    	        String create = rs.getString(2);
    	        return parse(create);
    	    }
    	} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbconn.close(rs, stmt);
		}
        return StringUtils.EMPTY;
    }
    
    /**
     * oracle查询表的注释
     * @param tableName
     * @return
     * @throws Exception
     */
    public static String getOracleCommentByTableName(String tableName) {
    	Statement stmt = null;
    	ResultSet rs = null;
    	try {
    		stmt = dbconn.getStatement();
    	    rs = stmt.executeQuery("select comments from user_tab_comments where table_name='"+ tableName +"'");
    	    if(Objects.nonNull(rs) && rs.next()) {
    	        String comments = rs.getString("comments");
    	        return comments;
    	    }
    	} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbconn.close(rs, stmt);
		}
        return StringUtils.EMPTY;
    }
     
    private static String parse(String all) {
        String comment = null;
        int index = all.indexOf("COMMENT='");
        if(index < 0) {
                return StringUtils.EMPTY;
        }
        comment = all.substring(index + 9);
        comment = comment.substring(0, comment.length() - 1);
        return comment;
    }
    
    // 判断 oracle sequence 是否已经存在
    public static boolean isSequenceExists(final String sequenceName) {
    	try {
	        String sql = "select sequence_name from  user_sequences where sequence_name='"+sequenceName.toUpperCase()+"'";
	        ResultSet rs = dbconn.getPreparedStatement(sql).executeQuery();
	        if(rs.next()){
	            return true ;
	        }else {
	            return false;
	        }
    	} catch (Exception e) {
			e.printStackTrace();
		}  
    	return false;
    }

}
