/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.List;
import com.ccrc.tiger.util.Objects;

import javax.persistence.GenerationType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 创建 实体处理器
 */
public class CreateEntity extends Creator {
	
	private boolean hasDate = false;
	private boolean hasTimestamp = false;
	
	public CreateEntity(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}

	/**
	 * 生成实体父类
	 * @return
	 */
	public String createEntitySerde() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getEntitySerdeFilePath());
		String result = "实体父类生成完成：" + javaPath;
		try {
			File javaFile = new File(javaPath);
            if(javaFile.exists()){
                result = "实体父类已存在，无须重新生成!";
            } else {
				String content = Utililies.readResourceFile(dir + "entity_serde.tlp");
				content = Utililies.parseTemplate(content, "EntityPackage", Utililies.getSuperPackage(ca.getEntityPackage()));
				Files.write(content.getBytes(), javaFile);
            }
		} catch (Exception e) {
			result = "实体父类生成失败：" + e.getMessage();
		}
		return result;
	}

	/**
	 * 根据表 创建对应的实体对象
	 * @return
	 */
	public String createEntity() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getEntityFilePath());
		String result = "实体生成完成：" + javaPath;
		try {
			File javaFile = new File(javaPath);
            if(javaFile.exists()){
                result = "实体类已存在，无须重新生成!";
            } else {
				String content = Utililies.readResourceFile(dir + "entity.tlp");
				// package
				content = Utililies.parseTemplate(content, "package", Utililies.getSuperPackage(ca.getEntityPackage()));
				// EntityName
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				// attr_list
				content = Utililies.parseTemplate(content, "attr_list", createAttrList(ca.getDaoFrame()));
				content = Utililies.parseTemplate(content, "importDate", hasDate ? Consts.IMPORT_DATE : StringUtils.EMPTY);
				content = Utililies.parseTemplate(content, "importTimestamp", hasTimestamp ? Consts.IMPORT_TIMESTAMP : StringUtils.EMPTY);
				// attr_getset_list
				content = Utililies.parseTemplate(content, "attr_getset_list", createAttrGetsetList());
				
				content = Utililies.parseTemplate(content, "TableEntity", 
						ca.getDaoFrame().equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE) ? createTableEntity(tableName) : StringUtils.EMPTY);
				
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ? 
																			getTableComment() : StringUtils.EMPTY);
				
				content = Utililies.parseTemplate(content, "importHibernatePackages", 
						ca.getDaoFrame().equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE) ? Consts.IMPORT_HIBERNATE_PACKAGES : Consts.IMPORT_MYBATIS_PACKAGES);
				//content = Utililies.parseTemplate(content, "attr_tostring_list", createAttrToStringList());
				
				// 判断是联合主键的情况下 在实体类添加联合主键类
				if(ca.getDaoFrame().equalsIgnoreCase("Hibernate")){
					if (CollectionUtils.isNotEmpty(getPrimaryKeyColumns()) && getPrimaryKeyColumns().size() > 1) {
						content = Utililies.parseTemplate(content, "IdClassPK", "@IdClass("+ getEntityName() +"PK.class)");
						content = Utililies.parseTemplate(content, "importIdClassPackage", Consts.IMPORT_IDCLASS_PACKAGE);
					}else {
						content = Utililies.parseTemplate(content, "IdClassPK", StringUtils.EMPTY);
						content = Utililies.parseTemplate(content, "importIdClassPackage", StringUtils.EMPTY);
					}
				}else {
					content = Utililies.parseTemplate(content, "IdClassPK", StringUtils.EMPTY);
					content = Utililies.parseTemplate(content, "importIdClassPackage", StringUtils.EMPTY);
				}
	
				Utililies.writeContentToFile(javaPath, content);
            }
		} catch (Exception e) {
			result = "实体生成失败：" + e.getMessage();
		}
		return result;
	}
	
	private String createAttrGetsetList() {
		StringBuffer sb = new StringBuffer();
		if (Objects.nonNull(columnList)) {
			String content = null, attr = null;
			if(DBHelper.sections.equals(MySqlSetPanel.sections)){
    			for (int i = 0, k = columnList.size(); i < k; i++) {
    				MysqlDbColumn column = (MysqlDbColumn) columnList.get(i);
    				if (Objects.nonNull(column)) {
    					attr = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
    					content = Utililies.readResourceFile(dir + "getset.tlp");
    					content = Utililies.parseTemplate(content, "EntityName", getEntityName());
    					content = Utililies.parseTemplate(content, "AttrName", CommonUtils.firstCharToUpperCase(attr));
    					content = Utililies.parseTemplate(content, "attrName", CommonUtils.firstCharToLowerCase(attr));
    					content = Utililies.parseTemplate(content, "comment", CommonUtils.isBlank(column.getColumnComment())
    							? column.getColumnName() : column.getColumnComment());
    					content = Utililies.parseTemplate(content, "JavaType", Utililies.getJavaType(column.getDataType()));
    
    					sb.append(content).append(Consts.ENTER);
    				}
    			}
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)){
    			for (int i = 0, k = columnList.size(); i < k; i++) {
    				OracleDbColumn column = (OracleDbColumn) columnList.get(i);
    				if (Objects.nonNull(column)) {
    					if(StringUtils.isNoneEmpty(column.getColumnKey()) && column.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_C)) {
    						continue;
    					}
    					
    					attr = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
    					content = Utililies.readResourceFile(dir + "getset.tlp");
    					content = Utililies.parseTemplate(content, "EntityName", getEntityName());
    					content = Utililies.parseTemplate(content, "AttrName", CommonUtils.firstCharToUpperCase(attr));
    					content = Utililies.parseTemplate(content, "attrName", CommonUtils.firstCharToLowerCase(attr));
    					content = Utililies.parseTemplate(content, "comment", CommonUtils.isBlank(column.getColumnComment())
    							? column.getColumnName() : column.getColumnComment());
    					content = Utililies.parseTemplate(content, "JavaType", Utililies.getJavaType(column.getDataType()));
    
    					sb.append(content).append(Consts.ENTER);
    				}
    			}
			}
		}
		return sb.toString();
	}
	
	private String createAttrList(String daoFrame) {
		StringBuffer sb = new StringBuffer();
		if (Objects.nonNull(columnList)) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)){
    			for (int i = 0, k = columnList.size(); i < k; i++) {
    				MysqlDbColumn column = (MysqlDbColumn) columnList.get(i);
    				if (Objects.nonNull(column)) {
    					if (column.getDataType().equalsIgnoreCase("date")
    							|| column.getDataType().equalsIgnoreCase("datetime") ) {
    						hasDate = true;
    					}
    					if (column.getDataType().equalsIgnoreCase("timestamp")
    							|| column.getDataType().equalsIgnoreCase("time")) {
    						hasTimestamp = true;
    					}
    					
    					if(daoFrame.equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)){
    						sb.append(Consts.TAB1).append("/** ");
    						sb.append(CommonUtils.isBlank(column.getColumnComment()) ? column.getColumnName() : column.getColumnComment());
    						sb.append(" */").append(Consts.ENTER);
    						
							if (column.getColumnKey().equalsIgnoreCase(Consts.MYSQL_COLUMN_KEY)) {
								sb.append(Consts.TAB1);
								sb.append(Consts.HIBERNATE_ID);
								sb.append(Consts.ENTER);
							}
							if (column.getExtra().equalsIgnoreCase(Consts.COLUMN_GENERATED)) {
								sb.append(Consts.TAB1);
								sb.append(getHibernateGenerationType(GenerationType.IDENTITY));
								sb.append(Consts.ENTER);
							}
							sb.append(Consts.TAB1);
							// 拼接 @Column 注解
							StringBuffer columnStr = new StringBuffer("@Column(name = \""+column.getColumnName()+"\"");
							if(column.getDataType().equalsIgnoreCase("varchar") 
									|| column.getDataType().equalsIgnoreCase("char")){
								columnStr.append(", length = "+ column.getCharacterMaximumLength());
							}
							if(column.getIsNullable().equalsIgnoreCase("NO")){
								columnStr.append(", nullable = false");
							}
							if(column.getIsNullable().equalsIgnoreCase("YES")){
								columnStr.append(", nullable = true");
							}
							columnStr.append(")");
							
							sb.append(columnStr.toString());
							sb.append(Consts.ENTER);
							
							if(column.getDataType().toLowerCase().contains("date")){
								sb.append(Consts.TAB1);
								sb.append(getColumnDateStyle());  
								sb.append(Consts.ENTER);
							}
							
							sb.append(Consts.TAB1);
							sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
									CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
									column.getColumnDefault()));
							sb.append(Consts.ENTER).append(Consts.ENTER);
    					}else{
	    					sb.append(Consts.TAB1).append("/** ")
	    							.append(CommonUtils.isBlank(column.getColumnComment()) ? column.getColumnName()
	    									: column.getColumnComment())
	    							.append(" */").append(Consts.ENTER).append(Consts.TAB1);
	    					if(column.getDataType().toLowerCase().contains("date")){
	    						sb.append(getColumnDateJsonFormat()).append(Consts.TAB1);
	    					}
	    					sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
	    									CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
	    									column.getColumnDefault()))
	    							.append(Consts.ENTER).append(Consts.ENTER);
    					}
    					
    				}
    			}
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)){
				List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
				
    			for (int i = 0, k = columnList.size(); i < k; i++) {
    				OracleDbColumn column = (OracleDbColumn) columnList.get(i);
    				if (Objects.nonNull(column)) {
    					if(StringUtils.isNoneEmpty(column.getColumnKey()) && column.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_C)) {
    						continue;
    					}
    					
    					if (column.getDataType().equalsIgnoreCase("date")
    							|| column.getDataType().equalsIgnoreCase("datetime") ) {
    						hasDate = true;
    					}
    					if (column.getDataType().equalsIgnoreCase("timestamp")
    							|| column.getDataType().equalsIgnoreCase("time")) {
    						hasTimestamp = true;
    					}
    					
    					if(daoFrame.equalsIgnoreCase("Hibernate")){
    						sb.append(Consts.TAB1).append("/** ");
    						sb.append(CommonUtils.isBlank(column.getColumnComment()) ? column.getColumnName() : column.getColumnComment());
    						sb.append(" */").append(Consts.ENTER);
    						
							if (Objects.nonNull(column.getColumnKey()) && column.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_KEY)) {
								sb.append(Consts.TAB1);
								sb.append(Consts.HIBERNATE_ID);
								sb.append(Consts.ENTER);
								if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() < 2) {
									sb.append(Consts.TAB1);
									sb.append(getHibernateGenerationType(GenerationType.SEQUENCE, tableName));
									sb.append(Consts.ENTER);
								}
							}
							/*if (Objects.nonNull(column.getExtra()) && column.getExtra().equalsIgnoreCase(Consts.COLUMN_GENERATED)) {
								sb.append(Consts.TAB1);
								sb.append(getHibernateGenerationType(GenerationType.SEQUENCE));
								sb.append(Consts.ENTER);
							}*/
							sb.append(Consts.TAB1);
							// 拼接 @Column 注解
							StringBuffer columnStr = new StringBuffer("@Column(name = \""+column.getColumnName()+"\"");
							if(column.getDataType().equalsIgnoreCase("varchar") 
									|| column.getDataType().equalsIgnoreCase("char")){
								columnStr.append(", length = "+ column.getCharacterMaximumLength());
							}
							if(column.getIsNullable().equalsIgnoreCase("NO")){
								columnStr.append(", nullable = false");
							}
							if(column.getIsNullable().equalsIgnoreCase("YES")){
								columnStr.append(", nullable = true");
							}
							columnStr.append(")");
							
							sb.append(columnStr.toString());
							sb.append(Consts.ENTER);
							
							if(column.getDataType().toLowerCase().contains("date")){
								sb.append(Consts.TAB1);
								sb.append(getColumnDateStyle());  
								sb.append(Consts.ENTER);
							}
							
							sb.append(Consts.TAB1);
							sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
									CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
									column.getColumnDefault()));
							sb.append(Consts.ENTER).append(Consts.ENTER);
    					}else{
	    					sb.append(Consts.TAB1).append("/** ")
	    							.append(CommonUtils.isBlank(column.getColumnComment()) ? column.getColumnName()
	    									: column.getColumnComment())
	    							.append(" */").append(Consts.ENTER).append(Consts.TAB1);
	    					if(column.getDataType().toLowerCase().contains("date")){
	    						sb.append(getColumnDateJsonFormat()).append(Consts.TAB1);
	    					}
	    					sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
	    									CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
	    									column.getColumnDefault()))
	    							.append(Consts.ENTER).append(Consts.ENTER);
    					}
    					
    				}
    			}
			}
		}
		return sb.toString();
	}
	
	private String getColumnDateJsonFormat() {
		return "@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateStyle.YYYY_MM_DD_HH_MM_SS, timezone = \"GMT+8\")" + Consts.ENTER;
	}
	
	private String getColumnDateStyle() {
		StringBuffer sb = new StringBuffer();
		sb.append(getColumnDateJsonFormat());
		sb.append(Consts.TAB1);
		sb.append("@DateTimeFormat(pattern = DateStyle.YYYY_MM_DD_HH_MM_SS)");
		return sb.toString();
	}

	private String getHibernateGenerationType(GenerationType type) {
		String generationType = "@GeneratedValue(strategy = GenerationType."+ type +")";
		return generationType;
	}
	
	private String getHibernateGenerationType(GenerationType type, String tableName) {
		StringBuffer sb = new StringBuffer();
		sb.append("@SequenceGenerator(name = \""+ tableName +"_SEQUENCE\", sequenceName = \""+ tableName +"_SEQUENCE\", allocationSize = 1)");
		sb.append(Consts.ENTER);
		sb.append(Consts.TAB1);
		sb.append("@GeneratedValue(strategy = GenerationType."+ type +", generator = \""+ tableName +"_SEQUENCE\")");
		return sb.toString();
	}

	
}
