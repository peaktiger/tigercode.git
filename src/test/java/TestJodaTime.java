import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Jalen
 *
 */
public class TestJodaTime {

	@Test
	public void testDiffDays() {
		DateTime today = new DateTime();
		DateTime beforeDay = today.minusDays(40);
		
		int diffDays = Days.daysBetween(beforeDay, today).getDays();
		
		Assert.assertEquals(40, diffDays);
	}

	@Test
	public void testDiffMonths() {
		DateTime today = new DateTime();
		DateTime beforeMonth = today.minusMonths(6);
		
		int beforeMonths = Months.monthsBetween(beforeMonth, today).getMonths();
		
		Assert.assertEquals(6, beforeMonths);
		
	}
}
