package com.ccrc.tiger.creator;

import java.io.File;
import java.io.IOException;
import java.util.List;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ccrc.tiger.componet.CheckBoxNode;
import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.Utililies;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.io.Files;

/**
 * 创造器，用于创建实体，Controller或Action代码，Service代码，Dao代码，jsp代码，js代码
 * @author Jalen
 *
 */
public class Creator {
	private static Logger LOG = Logger.getLogger(Creator.class);

	CreateAttr ca = null;
	String tableName = StringUtils.EMPTY;
	List<DbTableColumn> columnList = null;
	String dir = "template/";
	private String entityName = null;
	private String primaryKey = null;
	private String primaryColumn = null;
	private List<String> primaryColumns = null;
	private String primaryKeyJdbcType = null;
	private String primaryKeyJaveType = null;
	
	protected String methondBys;
	protected List<String> methondParam;
	protected String methondParams;
	protected String returnParams;
	
	public Creator(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		this.ca = ca;
		this.tableName = tname;
		this.columnList = columnList;
		
		getEntityName();
		getCheckedBoxNodes();
	}

	public String getEntityName() {
		if (Objects.isNull(entityName)) {
			//entityName = Utililies.tableToEntity(tableName);
			entityName = CommonUtils.convertToCamelCase(tableName);
			
			ca.replaceAll(tableName);
		}
		return entityName;
	}
	
	public String getTlpPath(String name){
	    String fix = "mysql_";
	    if(DBHelper.sections.equals(MySqlSetPanel.sections)){
	        fix = "mysql_";
	    }
	    return dir + fix + name;
	}

	public void run() {
		CreateEntity createEntity = new CreateEntity(ca, tableName, columnList);
		String result = createEntity.createEntitySerde();
		LOG.info(result);
		
		// 判断实体 属性个数是否大于2， 为联合主键，创建联合主键类
		if (ca.getDaoFrame().equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE) && 
				CollectionUtils.isNotEmpty(getPrimaryKeyColumns()) && getPrimaryKeyColumns().size() > 1) {
			CreateEntityPK createEntityPK = new CreateEntityPK(ca, tableName, columnList);
			result = createEntityPK.createEntityPK();
			LOG.info(result);
		}
		
		result = createEntity.createEntity();
		LOG.info(result);
		
		// 持久层框架
		if(ca.getDaoFrame().equalsIgnoreCase(Consts.DAO_FRAME_MYBATIS)){
			if(ca.getDaoOp()) {
				CreateDaoMybatis createDaoMybatis = new CreateDaoMybatis(ca, tableName, columnList);
				//result = createDaoMybatis.createBaseArgument();
				//LOG.info(result);
				
			    /*result = createDaoMybatis.createIDao();
			    LOG.info(result);
			    result = createDaoMybatis.createDao();
			    LOG.info(result);*/
			    result = createDaoMybatis.createDaoMapper();
			    LOG.info(result);
			    /*result = createDaoMybatis.createIBaseService();
			    LOG.info(result);
			    result = createDaoMybatis.createBaseService();
			    LOG.info(result);*/
			    /*result = createDaoMybatis.createIService();
			    LOG.info(result);
			    result = createDaoMybatis.createService();
			    LOG.info(result);*/
			    /*result = createDaoMybatis.createMapper();
			    LOG.info(result);*/
			    result = createDaoMybatis.createNewMapper();
			    LOG.info(result);
			}
		    
			if(ca.getServiceOp()) {
			    CreateService createService = new CreateService(ca, tableName, columnList);
				result = createService.createMybatisIService();
				LOG.info(result);
				result = createService.createMybatisImplService();
				LOG.info(result);
			}
		} else if(ca.getDaoFrame().equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)) {
			if(ca.getDaoOp()) {
				CreateDaoHibernate createDaoHibernate = new CreateDaoHibernate(ca, tableName, columnList);
				result = createDaoHibernate.createHibernateIDao();
				LOG.info(result);
				result = createDaoHibernate.createHibernateImplDao();
				LOG.info(result);
			}
			
			if(ca.getServiceOp()) {
				CreateService createService = new CreateService(ca, tableName, columnList);
				result = createService.createHibernateIService();
				LOG.info(result);
				result = createService.createHibernateImplService();
				LOG.info(result);
			}
		}
		
		// 控制层框架
		if(ca.getConFrame().equalsIgnoreCase(Consts.MVC_FRAME_SPRING) && ca.getControllerOp()){
			CreateController createController = new CreateController(ca, tableName, columnList);
		    result = createController.createBaseController();
		    LOG.info(result);
		    result = createController.createController();
		    LOG.info(result);
		} else if (ca.getConFrame().equalsIgnoreCase(Consts.MVC_FRAME_STRUTS2)){
			LOG.info("##############Struts2代码还在开发中！");
		}
		
		// 视图层框架
		if(ca.getViewFrame().equalsIgnoreCase(Consts.VIEW_FRAME_JSP) && ca.getJspOp()){
			CreateJsp createJsp = new CreateJsp(ca, tableName, columnList);
			result = createJsp.createJspList();
			LOG.info(result);
			
			result = createJsp.createFirstJsp();
			LOG.info(result);
			
			result = createJsp.createIndexJsp();
			LOG.info(result);
			/*result = createJsp.createJspAdd();
			LOG.info(result);
			result = createJsp.createJspEdit();
			LOG.info(result);*/
		}
		
		// javasript框架
		if(ca.getJsFame().equalsIgnoreCase(Consts.VIEW_FRAME_JS) && ca.getJsOp()){
			CreateJavaScript createJavaScript = new CreateJavaScript(ca, tableName, columnList);
			result = createJavaScript.createJavaSript();
			LOG.info(result);
		}
	}
	
	/**
	 * 
	 * @param javaFile 读取java文件
	 * @param method 创建定义的方法
	 * @return 
	 * @throws IOException
	 */
	protected String createSingleMethod(File javaFile, List<String> method) throws IOException {
		List<String> lines = Files.readLines(javaFile, Charsets.UTF_8);
		int lastIndex = lines.lastIndexOf(Consts.BRACE_RIGHT);
		lines.addAll(lastIndex, method);
		String content = Joiner.on(StringUtils.LF).join(lines);
		return content;
	}
	
	/**
	 * 
	 * @param xmlFile 读取xml文件
	 * @param method 创建定义的方法
	 * @return 
	 * @throws IOException
	 */
	protected String createSingleElement(File javaFile, List<String> method) throws IOException {
		List<String> lines = Files.readLines(javaFile, Charsets.UTF_8);
		int lastIndex = lines.lastIndexOf(Consts.XML_ELEMENT);
		lines.addAll(lastIndex, method);
		String content = Joiner.on(StringUtils.LF).join(lines);
		return content;
	}
	
	/**
	 * 
	 * @param javaFile 读取java文件
	 * @param method 创建定义的方法
	 * @return 
	 * @throws IOException
	 */
	protected String createSingleMethod(File javaFile, List<String> method, String position) throws IOException {
		List<String> lines = Files.readLines(javaFile, Charsets.UTF_8);
		int lastIndex = lines.lastIndexOf(position);
		lines.addAll(lastIndex, method);
		String content = Joiner.on(StringUtils.LF).join(lines);
		return content;
	}
	
	/**
	 * 
	 * @param javaFile 读取java文件
	 * @param method 创建定义的方法
	 * @return 
	 * @throws IOException
	 *//*
	protected static String createSingleMethod1(File javaFile, List<String> method, String position) throws IOException {
		List<String> lines = Files.readLines(javaFile, Charsets.UTF_8);
		int index = lines.lastIndexOf(position);
		lines.addAll(index, method);
		String content = Joiner.on(StringUtils.LF).join(lines);
		return content;
	}*/
	

	/**
	 * 获取表的注释信息
	 * @return
	 */
	protected String getTableComment() {
		if (Objects.nonNull(columnList)) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
				MysqlDbColumn column = (MysqlDbColumn) columnList.get(0);
				if (Objects.nonNull(column)) {
					return column.getTableComment();
				}
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)) {
				return DBHelper.getOracleCommentByTableName(tableName);
			}
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * 获取当前表 主键列名称 和 类型
	 * @return
	 */
	protected String[] getCurColumnKey() {
		String[] curColumn = new String[2];
		if (Objects.nonNull(columnList)) {
			if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					MysqlDbColumn column = (MysqlDbColumn) columnList.get(i);
					if(Objects.nonNull(column) && StringUtils.isNoneBlank(column.getColumnKey())
							&& column.getColumnKey().equals(Consts.MYSQL_COLUMN_KEY)){
						curColumn[0] = column.getColumnName();
						curColumn[1] = column.getDataType();
						return curColumn;
					}
				}
			}
			if (DBHelper.sections.equals(OracleSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					OracleDbColumn column = (OracleDbColumn) columnList.get(i);
					if(Objects.nonNull(column) && StringUtils.isNoneBlank(column.getColumnKey())
							&& column.getColumnKey().equals(Consts.ORACLE_COLUMN_KEY)){
						curColumn[0] = column.getColumnName();
						curColumn[1] = column.getDataType();
						return curColumn;
					}
				}
			}
		}
		return curColumn;
	}

	protected String createTableEntity(String tableName) {
		return new StringBuffer("@Entity")
		.append(StringUtils.LF)
		.append("@Table(name = \""+ tableName +"\")")
		.append(StringUtils.LF)
		.append("@DynamicInsert(true)")
		.append(StringUtils.LF)
		.append("@DynamicUpdate(true)")
		.toString();
	}

	public String createAttrToStringList(){
	    StringBuffer sb = new StringBuffer();
	    if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                MysqlDbColumn mdc = null;
                //sb.append("; ordinalPosition=" + (ordinalPosition == null ? "null" : ordinalPosition.toString()));
                String maps = "sb.append(\"; {0}=\" + ({0} == null ? \"null\" : {0}.toString()));\r\n";
                for (int i = 0, k = columnList.size(); i < k; i++) {
                    mdc = (MysqlDbColumn) columnList.get(i);
                    sb.append(Consts.TAB2).append(CommonUtils.format(maps, Utililies.columnToFeild(mdc.getColumnName())));
                }
            }
	    }
	    return sb.toString();
	}
	
	/**获取主键字段*/
    public DbTableColumn getPrimaryKeyColumn(){
        DbTableColumn dc = null;
        if(Objects.nonNull(columnList)){
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        dc = mdc;
                        break;
                    }
                }
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn mdc = (OracleDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        dc = mdc;
                        break;
                    }
                }
            }
        }
        return dc;
    }
    
    /**获取多个主键字段*/
    public List<DbTableColumn> getPrimaryKeyColumns(){
    	List<DbTableColumn> dcs = null;
        if(Objects.nonNull(columnList)){
        	dcs = Lists.newArrayList();
        	
            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	MysqlDbColumn mdc = (MysqlDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                        dcs.add(mdc);
                        continue;
                    }
                }
            }
            if(DBHelper.sections.equals(OracleSetPanel.sections)){
                for (int i = 0, k = columnList.size(); i < k; i++) {
                	OracleDbColumn mdc = (OracleDbColumn) columnList.get(i);
                    if(mdc.isPrimaryKey()){
                    	dcs.add(mdc);
                    	continue;
                    }
                }
            }
        }
        return dcs;
    }
	
	/**获取主键字段名*/
	public String getPrimaryColumn(){
	    if(StringUtils.isEmpty(primaryColumn)){
	        DbTableColumn dc = getPrimaryKeyColumn();
	        if(Objects.nonNull(dc)){
	            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
	                MysqlDbColumn mdc = (MysqlDbColumn) dc;
	                if(mdc.isPrimaryKey()){
	                    primaryColumn = mdc.getColumnName();
	                }
	            }
	            if(DBHelper.sections.equals(OracleSetPanel.sections)){
	            	OracleDbColumn mdc = (OracleDbColumn) dc;
	            	if(mdc.isPrimaryKey()){
	            		primaryColumn = mdc.getColumnName();
	            	}
	            }
	        }
	        if(Objects.isNull(primaryColumn)){
	            primaryColumn = StringUtils.EMPTY;
	        }
	    }
	    return primaryColumn;
	}
	
	/**获取多个主键字段名*/
	public List<String> getPrimaryColumns(){
	    if(CollectionUtils.isNotEmpty(primaryColumns)){
	    	primaryColumns = Lists.newArrayList();
	    	
	        List<DbTableColumn> dcs = getPrimaryKeyColumns();
	        if(CollectionUtils.isNotEmpty(dcs)){
	        	for (DbTableColumn dc : dcs) {
		            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
		                MysqlDbColumn mdc = (MysqlDbColumn) dc;
		                if(mdc.isPrimaryKey()){
		                    primaryColumns.add(mdc.getColumnName());
		                }
		            }
		            if(DBHelper.sections.equals(OracleSetPanel.sections)){
		            	OracleDbColumn mdc = (OracleDbColumn) dc;
		            	if(mdc.isPrimaryKey()){
		            		primaryColumns.add(mdc.getColumnName());
		            	}
		            }
	        	}
	        }
	    }
	    return primaryColumns;
	}
	
	public String unionPKParams() {
		List<String> paramsData = Lists.newArrayList();
		List<String> argsData = Lists.newArrayList();
		for(DbTableColumn dtc : getPrimaryKeyColumns()) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
				 MysqlDbColumn mdc = (MysqlDbColumn) dtc;
				 paramsData.add(Utililies.getJavaType(mdc.getDataType()) + StringUtils.SPACE + CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()));
				 argsData.add(CommonUtils.convertToFirstLetterLowerCaseCamelCase(mdc.getColumnName()));
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)){
				OracleDbColumn odc = (OracleDbColumn) dtc;
				paramsData.add(Utililies.getJavaType(odc.getDataType()) + StringUtils.SPACE + CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName()));
				argsData.add(CommonUtils.convertToFirstLetterLowerCaseCamelCase(odc.getColumnName()));
			}
		}
		String params = Joiner.on(", ").join(paramsData);
		String args = Joiner.on(", ").join(argsData);
		return params + Constants.CHAR_AND + args;
	}
	
	public String unionPKMapperParams() {
		List<String> paramsData = Lists.newArrayList();
		List<String> argsData = Lists.newArrayList();
		for(DbTableColumn dtc : getPrimaryKeyColumns()) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
				 MysqlDbColumn mdc = (MysqlDbColumn) dtc;
				 paramsData.add(mdc.getDataType() + StringUtils.SPACE + mdc.getColumnName());
				 argsData.add(mdc.getColumnName());
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)){
				OracleDbColumn odc = (OracleDbColumn) dtc;
				paramsData.add(odc.getDataType() + StringUtils.SPACE + odc.getColumnName());
				argsData.add(odc.getColumnName());
			}
		}
		String params = Joiner.on(", ").join(paramsData);
		String args = Joiner.on(", ").join(argsData);
		return params + Constants.CHAR_AND + args;
	}
	
	public String getPrimaryKey(){
        if(Objects.isNull(primaryKey)){
            String column = getPrimaryColumn();
            if(Objects.nonNull(column)){
                primaryKey = Utililies.columnToFeild(column);
            }
        }
        return primaryKey;
    }
	
	public String getPrimaryJdbcType(){
	    if(Objects.isNull(primaryKeyJdbcType)){
	        DbTableColumn dc = getPrimaryKeyColumn();
	        if(Objects.nonNull(dc)){
	            if(DBHelper.sections.equals(MySqlSetPanel.sections)){
	                MysqlDbColumn mdc = (MysqlDbColumn) dc;
	                primaryKeyJdbcType = mdc.getDataType();
	            }
	            if(DBHelper.sections.equals(OracleSetPanel.sections)){
	            	OracleDbColumn odc = (OracleDbColumn) dc;
	                primaryKeyJdbcType = odc.getDataType();
	            }
	        }
	    }
        return Objects.isNull(primaryKeyJdbcType) ? "varchar" : primaryKeyJdbcType;
	}
	
	public String getPrimaryJavaType(){
        if(Objects.isNull(primaryKeyJaveType)){
            DbTableColumn dc = getPrimaryKeyColumn();
            if(Objects.nonNull(dc)){
                if(DBHelper.sections.equals(MySqlSetPanel.sections)){
                    MysqlDbColumn mdc = (MysqlDbColumn) dc;
                    primaryKeyJaveType = Utililies.getJavaType(mdc.getDataType());
                }
                if(DBHelper.sections.equals(OracleSetPanel.sections)){
                	OracleDbColumn odc = (OracleDbColumn) dc;
                    primaryKeyJaveType = Utililies.getJavaType(odc.getDataType());
                }
            }
        }
        return Objects.isNull(primaryKeyJaveType) ? "varchar" : primaryKeyJaveType;
    }
	
	private void getCheckedBoxNodes() {
		StringBuffer methondBy = new StringBuffer();
		methondParam = Lists.newArrayList();
		StringBuffer returnParam = new StringBuffer();
		String columnPky = StringUtils.EMPTY;
		List<CheckBoxNode> checkBoxNodes = ca.getCheckBoxNode(tableName);
		if (CollectionUtils.isNotEmpty(checkBoxNodes)) {
			for (CheckBoxNode node : checkBoxNodes) {
				String text = node.getText();
				if (StringUtils.isNoneEmpty(text)) {
					String[] props = text.split(" - "); 
					
					String columnName = props[0];
					String columnType = props[1];
					if(props.length > 2) {
						columnPky = props[2];
					}
					methondBy.append(StringUtils.capitalize(Utililies.columnToFeild(columnName)));
					if (columnType.indexOf("(") > 0) {
						columnType = columnType.substring(0, columnType.indexOf("("));
					}
					methondParam.add(Utililies.getJavaType(columnType) +" "+ Utililies.columnToFeild(columnName));
				}
			}
		}
		
		if (StringUtils.isNoneEmpty(columnPky)) {
			returnParam.append(getEntityName());
		} else {
			returnParam.append("List<"+getEntityName()+">");
		}
		
		methondParams = "(" +Joiner.on(", ").join(methondParam)+ ")";
		methondBys = methondBy.toString();
		returnParams = returnParam.toString();
	}
}
