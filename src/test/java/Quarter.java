import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.YearMonth;

public class Quarter {
	public static void main(String[] args) {
		System.out.println(new Date().getMonth());
		
		QuarterBean[] quarters = getRecentQuarters(12);
		for (int i = 0; i < quarters.length; i++) {
			
			System.out.println(quarters[i].toString());
			System.out.println("########################################################");
			System.out.println("季度的最后月"+quarters[i].getLastMonthOfQuarter());
			System.out.println("季度的最后年月"+quarters[i].getLastYearMonthOfQuarter().toString("yyyy-MM"));
			System.out.println("########################################################");
			System.out.println("季度的开始月"+quarters[i].getFirstMonthOfQuarter());
			System.out.println("季度的开始年月"+quarters[i].getFirstYearMonthOfQuarter().toString("yyyy-MM"));
			System.out.println("########################日期是否纯在################################");
			YearMonth yearMonth = new YearMonth(2017, 5);
			System.out.println("日期是否纯=="+quarters[i].existYearMonthOfQuarter(yearMonth));
			YearMonth yearMonth4 = new YearMonth(2017, 4);
			System.out.println("日期是否纯4=="+quarters[i].existYearMonthOfQuarter(yearMonth4));
			YearMonth yearMonth6 = new YearMonth(2017, 6);
			System.out.println("日期是否纯6=="+quarters[i].existYearMonthOfQuarter(yearMonth6));
			System.out.println("########################################################");
			
		}
	}

	/**
	 * 获得最近的季度
	 * 
	 * @param count
	 *            需要获得季度的数量
	 * @return
	 */
	public static QuarterBean[] getRecentQuarters(int count) {
		if (count < 1) {
			throw new IllegalArgumentException("count must be great than 0");
		}
		QuarterBean[] quarters = new QuarterBean[count];
		quarters[0] = getCurrentQuarter();
		for (int i = 1; i < quarters.length; i++) {
			quarters[i] = quarters[i - 1].getPastQuarter();
		}
		return quarters;
	}

	/**
	 * 获得当前季度
	 * 
	 * @return
	 */
	public static QuarterBean getCurrentQuarter() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		return new QuarterBean(year, getQuarter(month));
	}

	/**
	 * 通过月份计算季度
	 * 
	 * @param month
	 * @return
	 */
	public static int getQuarter(int month) {
		if (month < 1 || month > 12) {
			throw new IllegalArgumentException("month is invalid.");
		}
		return (month - 1) / 3 + 1;
	}
}

class QuarterBean {
	public final static int MONTH_OF_QUARTER = 3;
	public final static int QUARTERS_EVERY_YEAR = 4;
	private final static String[] QUARTERS = 
		{ "Q1 {0}(1 - 3)", 
		  "Q2 {0}(4 - 6)", 
		  "Q3 {0}(7 - 9)",
		  "Q4 {0}(10 - 12)" 
		 };

	private int year;
	private int quarter;
	
	private int lastMonthOfQuarter;
	private YearMonth lastYearMonthOfQuarter;
	
	private int firstMonthOfQuarter;
	private YearMonth firstYearMonthOfQuarter;
	
	public QuarterBean(int year, int quarter) {
		if (quarter < 1 || quarter > QUARTERS_EVERY_YEAR) {
			throw new IllegalArgumentException("quarter is invalid.");
		}
		this.year = year;
		this.quarter = quarter;
		this.lastMonthOfQuarter = quarter * MONTH_OF_QUARTER;
		this.lastYearMonthOfQuarter = new YearMonth(this.year, this.lastMonthOfQuarter);
		
		this.firstMonthOfQuarter = this.lastMonthOfQuarter - 2;
		this.firstYearMonthOfQuarter = new YearMonth(this.year, this.firstMonthOfQuarter);
	}

	private QuarterBean() {

	}

	public int getYear() {
		return year;
	}

	public int getQuarter() {
		return quarter;
	}

	public int getLastMonthOfQuarter() {
		return lastMonthOfQuarter;
	}

	public YearMonth getLastYearMonthOfQuarter() {
		return lastYearMonthOfQuarter;
	}

	public int getFirstMonthOfQuarter() {
		return firstMonthOfQuarter;
	}

	public YearMonth getFirstYearMonthOfQuarter() {
		return firstYearMonthOfQuarter;
	}
	
	public boolean existYearMonthOfQuarter(YearMonth yearMonth) {
		if ((yearMonth.compareTo(this.getFirstYearMonthOfQuarter()) >= NumberUtils.INTEGER_ZERO) 
				&& yearMonth.compareTo(this.getLastYearMonthOfQuarter()) <= NumberUtils.INTEGER_ZERO) {
			return true;
		}
		return false;
	}

	/**
	 * 获得上一季度
	 * 
	 * @return
	 */
	public QuarterBean getPastQuarter() {
		QuarterBean bean = new QuarterBean();
		int y = (QUARTERS_EVERY_YEAR + 1 - quarter) / QUARTERS_EVERY_YEAR;
		bean.year = year - y;
		bean.quarter = y * QUARTERS_EVERY_YEAR + (quarter - 1);
		bean.lastMonthOfQuarter = bean.quarter * MONTH_OF_QUARTER;
		bean.lastYearMonthOfQuarter = new YearMonth(bean.year, bean.lastMonthOfQuarter);
		
		bean.firstMonthOfQuarter = bean.lastMonthOfQuarter - 2;
		bean.firstYearMonthOfQuarter = new YearMonth(bean.year, bean.firstMonthOfQuarter);
		return bean;
	}
	
	public String toString() {
		return MessageFormat.format(QUARTERS[quarter - 1], String.valueOf(year));
	}
}
