/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.Date;
import java.util.List;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.ccrc.lang.DateStyle;
import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 创建 实体联合主键处理器
 */
public class CreateEntityPK extends Creator {

	public CreateEntityPK(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}

	public String createEntityPK() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), ca.getEntityPKFilePath());
		String result = "实体联合主键类生成完成：" + javaPath;
		try {
			File javaFile = new File(javaPath);
            if(javaFile.exists()){
                result = "实体联合主键类已存在，无须重新生成!";
            } else {
				String content = Utililies.readResourceFile(dir + "entityPK_tlp");
				content = Utililies.parseTemplate(content, "package", Utililies.getSuperPackage(ca.getEntityPackage()));
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				content = Utililies.parseTemplate(content, "pk_attr_list", createPKAttrList(ca.getDaoFrame()));
				content = Utililies.parseTemplate(content, "pk_constructor", createPKConstructor(ca.getDaoFrame()));
				content = Utililies.parseTemplate(content, "pk_attr_getset_list", createPKAttrGetsetList());
				
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ? 
																		getTableComment() : StringUtils.EMPTY);
				Files.write(content.getBytes(), javaFile);
            }
		} catch (Exception e) {
			result = "实体联合主键类生成失败：" + e.getMessage();
		}
		return result;
	}
	
	private String createPKConstructor(String daoFrame) {
		String[] split = unionPKParams().split(Constants.CHAR_AND);
		
		StringBuffer sb = new StringBuffer(Consts.TAB1);
		sb.append(Consts.PUBLIC);
		sb.append(getEntityName()+ "PK (){}");
		sb.append(Consts.ENTER);
		sb.append(Consts.ENTER);
		sb.append(Consts.TAB1);
		sb.append(Consts.PUBLIC);
		sb.append(getEntityName()+ "PK ("+ split[0] +") {");
		sb.append(Consts.ENTER);
		if(DBHelper.sections.equals(MySqlSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				MysqlDbColumn column = (MysqlDbColumn)pkc;
				
				if(daoFrame.equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)){
					String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
					//sb.append(Utililies.getVarJavaType(column.getDataType())column.getColumnName());
					sb.append(Consts.TAB2+"this."+feild +" = "+ feild +";");
					sb.append(Consts.ENTER);
				}else {
					
				}
			}
		}
		
		if(DBHelper.sections.equals(OracleSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				OracleDbColumn column = (OracleDbColumn)pkc;
				
				if(daoFrame.equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)){
					//sb.append(Utililies.getVarJavaType(column.getDataType())column.getColumnName());
					String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
					sb.append(Consts.TAB2+"this."+feild +" = "+ feild +";");
					sb.append(Consts.ENTER);
				}else {
					
				}
			}
		}
		sb.append(Consts.TAB1);
		sb.append("}" + Consts.ENTER).append(Consts.ENTER);
		return sb.toString();
	}

	private String createPKAttrList(String daoFrame) {
		StringBuffer sb = new StringBuffer();
		if(DBHelper.sections.equals(MySqlSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				MysqlDbColumn column = (MysqlDbColumn)pkc;
				
				if(daoFrame.equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)){
					sb.append(Consts.TAB1);
					sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
							CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
							column.getColumnDefault()));
					sb.append(Consts.ENTER).append(Consts.ENTER);
				}else {
					
				}
			}
		}
		
		if(DBHelper.sections.equals(OracleSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				OracleDbColumn column = (OracleDbColumn)pkc;
				
				if(daoFrame.equalsIgnoreCase(Consts.DAO_FRAME_HIBERNATE)){
					sb.append(Consts.TAB1);
					sb.append(Utililies.getAttrDeclare(Utililies.getVarJavaType(column.getDataType()),
							CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()),
							column.getColumnDefault()));
					sb.append(Consts.ENTER).append(Consts.ENTER);
				}else {
					
				}
			}
		}
		return sb.toString();
	}

	private String createPKAttrGetsetList() {
		StringBuffer sb = new StringBuffer();
		String content = null, attr = null;
		
		if(DBHelper.sections.equals(MySqlSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				MysqlDbColumn column = (MysqlDbColumn)pkc;
				
				if (Objects.nonNull(column)) {
					attr = Utililies.tableToEntity(column.getColumnName());
					content = Utililies.readResourceFile(dir + "getset.tlp");
					content = Utililies.parseTemplate(content, "EntityName", getEntityName() + "PK");
					content = Utililies.parseTemplate(content, "AttrName", CommonUtils.firstCharToUpperCase(attr));
					content = Utililies.parseTemplate(content, "attrName", CommonUtils.firstCharToLowerCase(attr));
					content = Utililies.parseTemplate(content, "comment", CommonUtils.isBlank(column.getColumnComment())
							? column.getColumnName() : column.getColumnComment());
					content = Utililies.parseTemplate(content, "JavaType", Utililies.getJavaType(column.getDataType()));

					sb.append(content).append(Consts.ENTER);
				}
			}
		}
		
		if(DBHelper.sections.equals(OracleSetPanel.sections)){
			for (DbTableColumn pkc : getPrimaryKeyColumns()) {
				OracleDbColumn column = (OracleDbColumn)pkc;
				
				if (Objects.nonNull(column)) {
					attr = Utililies.tableToEntity(column.getColumnName());
					content = Utililies.readResourceFile(dir + "getset.tlp");
					content = Utililies.parseTemplate(content, "EntityName", getEntityName() + "PK");
					content = Utililies.parseTemplate(content, "AttrName", CommonUtils.firstCharToUpperCase(attr));
					content = Utililies.parseTemplate(content, "attrName", CommonUtils.firstCharToLowerCase(attr));
					content = Utililies.parseTemplate(content, "comment", CommonUtils.isBlank(column.getColumnComment())
							? column.getColumnName() : column.getColumnComment());
					content = Utililies.parseTemplate(content, "JavaType", Utililies.getJavaType(column.getDataType()));

					sb.append(content).append(Consts.ENTER);
				}
			}
		}
		return sb.toString();
	}
	

}
