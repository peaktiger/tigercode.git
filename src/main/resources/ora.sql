select * from user_tab_columns where Table_Name='TB_DEPARTMENT'
select * from user_tab_columns where Table_Name='TB_EMPLOYEE'
select * from user_tab_columns where Table_Name='TB_SALGRA'

SELECT 1 FROM DUAL
select * from user_col_comments where Table_Name='TB_DEPARTMENT'

select * from user_constraints

select b.*, a.column_Name 
 from user_cons_columns a, user_constraints b 
 where a.constraint_name = b.constraint_name  
and a.table_name in (select table_name from user_tables)

select cu.*, au.* from user_cons_columns cu, user_constraints au 
where cu.constraint_name = au.constraint_name and au.constraint_type = 'P' and au.table_name ='TB_EMPLOYEE'

select b.table_name, b.constraint_name, b.constraint_type, b.status, a.column_Name 
 from user_cons_columns a, user_constraints b 
 where a.constraint_name = b.constraint_name  
and a.table_name in (select table_name from user_tables)

select t.*, c.comments
from user_tab_columns t, user_col_comments c
where t.column_Name=c.column_Name 
and t.Table_Name in (select table_name from user_tables)
order by t.table_name, t.column_Name


select t1.*, t2.constraint_name, t2.constraint_type, t2.status
from
(select t.*, c.comments
from user_tab_columns t, user_col_comments c
where t.column_Name=c.column_Name 
and t.Table_Name in (select table_name from user_tables)) t1
left join
(select b.table_name, b.constraint_name, b.constraint_type, b.status, a.column_Name 
 from user_cons_columns a, user_constraints b 
 where a.constraint_name = b.constraint_name 
 and b.constraint_type='P'
 and a.table_name in (select table_name from user_tables)) t2
on t1.column_Name=t2.column_Name
order by t1.table_name, t1.column_Name



select comments from user_tab_comments where table_name='TB_EMPLOYEE'

create sequence bign nocycle maxvalue 9999999999 start with 1;

CREATE SEQUENCE TB_EMPLOYEE_SEQUENCE 
INCREMENT BY 1
START WITH 1000
NOMAXVALUE
NOCYCLE
CACHE 10;


create sequence TB_EMPLOYEE_SEQUENCE
minvalue 1
maxvalue 999999
start with 1
increment by 1
nocache;

DROP SEQUENCE TB_EMPLOYEE_SEQUENCE;

drop table TB_EMPLOYEE purge;
drop table TB_DEPARTMENT purge;
drop table TB_SALGRA purge;

purge recyclebin;


create table student_course
(
sno char(8) not null,
cno char(10) not null,
score number,
constraint PK_SC primary key (sno, cno)
)


