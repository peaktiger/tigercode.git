package com.ccrc.tiger.form;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JLabel;

import com.ccrc.tiger.util.Consts;

@SuppressWarnings("serial")
public class AboutForm extends JDialog {

    public AboutForm() {
        initCommpant();
    }

    private void initCommpant() {
        this.setTitle("关于"+ Consts.TIGER_CODE_NAME);
        Toolkit toolkit = Toolkit.getDefaultToolkit(); 
        Dimension scmSize = toolkit.getScreenSize(); 
        this.setLocation(scmSize.width/2 - 230, scmSize.height/2 - 130); 
        this.setSize(460, 260);
        this.setBackground(Color.white);
        this.setAlwaysOnTop(true);
        
        this.setLayout(null);
        
        StringBuffer html = new StringBuffer()
        		.append("<html>"+ Consts.TIGER_CODE_NAME +"是一个代码生成工具，该工具旨在简化程序员开发过程中的<br>")
        		.append("繁琐工作，降低错误率，提高开发效率，形成统一的代码风格，提高代<br>")
        		.append("码的阅读性和维护性。 <br>")
        		.append("目前"+ Consts.TIGER_CODE_NAME +"还在开发中，功能陆续完善，敬请期待。<br><br><br>")
        		.append("作者：巅峰之虎    QQ:873010546</html>");
        
        JLabel label = new JLabel(html.toString());
        label.setBounds(20, 20, 420, 160);
        this.add(label);
    }
    
    public void showForm(){
        this.setVisible(true);
    }

}
