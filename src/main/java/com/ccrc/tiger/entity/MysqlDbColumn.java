package com.ccrc.tiger.entity;

import com.ccrc.tiger.util.Objects;

/**
 * mysql数据库表字段信息
 * 
 *
 */
public class MysqlDbColumn extends DbTableColumn {
    private String tableSchema; // 所属库
    private String tableName; // 所属表
    private String tableComment; // 表注释
    private String columnName; // 列名
    private String columnType; // bigint(11)，varchar(30)
    private String dataType; // bigint->Long，int->Integer，varchar/char->String，timestamp->Timestamp，date/time->Date，float->Float，double/decimal->Double，blob->byte[]
    private Object columnDefault; // 列缺省值
    private Integer characterOctetLength; // 列最大字节长度，utf-8：
                                          // characterMaximumLength*
                                          // 3，gbk：characterMaximumLength* 2
    private Integer characterMaximumLength; // 列最大长度
    private Integer ordinalPosition; // 字节顺序码，从1开始
    private String isNullable; // 是否可为空，YES：可空，NO：非空
    private String columnKey; // 列键值,PRI主键
    private String extra; // auto_increment 自增长
    private String columnComment; // 列描述
    private String privileges; // 列权限集合，以逗号隔开

    public String getTableSchema() {
        return tableSchema;
    }

    public MysqlDbColumn setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public MysqlDbColumn setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }
    
    public String getTableComment() {
		return tableComment;
	}

	public MysqlDbColumn setTableComment(String tableComment) {
		this.tableComment = tableComment;
		return this;
	}

	public String getColumnName() {
        return columnName;
    }

    public MysqlDbColumn setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getColumnType() {
        return columnType;
    }

    public MysqlDbColumn setColumnType(String columnType) {
        this.columnType = columnType;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public MysqlDbColumn setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public Object getColumnDefault() {
        return columnDefault;
    }

    public MysqlDbColumn setColumnDefault(Object columnDefault) {
        this.columnDefault = columnDefault;
        return this;
    }

    public Integer getCharacterOctetLength() {
        return characterOctetLength;
    }

    public MysqlDbColumn setCharacterOctetLength(Integer characterOctetLength) {
        this.characterOctetLength = characterOctetLength;
        return this;
    }

    public Integer getCharacterMaximumLength() {
        return characterMaximumLength;
    }

    public MysqlDbColumn setCharacterMaximumLength(Integer characterMaximumLength) {
        this.characterMaximumLength = characterMaximumLength;
        return this;
    }

    public Integer getOrdinalPosition() {
        return ordinalPosition;
    }

    public MysqlDbColumn setOrdinalPosition(Integer ordinalPosition) {
        this.ordinalPosition = ordinalPosition;
        return this;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public MysqlDbColumn setIsNullable(String isNullable) {
        this.isNullable = isNullable;
        return this;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public MysqlDbColumn setColumnKey(String columnKey) {
        this.columnKey = columnKey;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public MysqlDbColumn setExtra(String extra) {
        this.extra = extra;
        return this;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public MysqlDbColumn setColumnComment(String columnComment) {
        this.columnComment = columnComment;
        return this;
    }

    public String getPrivileges() {
        return privileges;
    }

    public MysqlDbColumn setPrivileges(String privileges) {
        this.privileges = privileges;
        return this;
    }

    public boolean isPrimaryKey(){
        return Objects.nonNull(columnKey) && columnKey.equalsIgnoreCase("PRI");
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName());
        sb.append(" # tableSchema=" + (Objects.isNull(tableSchema) ? "null" : tableSchema.toString()));
        sb.append("; tableName=" + (Objects.isNull(tableName) ? "null" : tableName.toString()));
        sb.append("; tableComment=" + (Objects.isNull(tableComment) ? "null" : tableName.toString()));
        sb.append("; columnName=" + (Objects.isNull(columnName) ? "null" : columnName.toString()));
        sb.append("; columnType=" + (Objects.isNull(columnType) ? "null" : columnType.toString()));
        sb.append("; dataType=" + (Objects.isNull(dataType) ? "null" : dataType.toString()));
        sb.append("; characterMaximumLength=" + (Objects.isNull(characterMaximumLength) ? "null" : characterMaximumLength.toString()));
        sb.append("; characterOctetLength=" + (Objects.isNull(characterOctetLength) ? "null" : characterOctetLength.toString()));
        sb.append("; columnDefault=" + (Objects.isNull(columnDefault) ? "null" : columnDefault.toString()));
        sb.append("; ordinalPosition=" + (Objects.isNull(ordinalPosition) ? "null" : ordinalPosition.toString()));
        sb.append("; isNullable=" + (Objects.isNull(isNullable) ? "null" : isNullable.toString()));
        sb.append("; columnKey=" + (Objects.isNull(columnKey) ? "null" : columnKey.toString()));
        sb.append("; extra=" + (Objects.isNull(extra) ? "null" : extra.toString()));
        sb.append("; columnComment=" + (Objects.isNull(columnComment) ? "null" : columnComment.toString()));
        sb.append("; privileges=" + (Objects.isNull(privileges) ? "null" : privileges.toString()));
        return sb.toString();
    }
    
}
