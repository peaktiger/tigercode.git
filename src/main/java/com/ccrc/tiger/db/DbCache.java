package com.ccrc.tiger.db;

import java.util.List;
import java.util.Map;

import com.ccrc.tiger.entity.DbTable;
import com.ccrc.tiger.entity.DbTableColumn;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class DbCache {

	public static List<DbTable> mysqlDbTableList = Lists.newArrayList();

	public static Map<String, List<DbTableColumn>> mysqlDbColumnMap = Maps.newConcurrentMap();

}
