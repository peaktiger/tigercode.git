package com.ccrc.tiger.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Charsets;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.io.Files;

/**
 * <p>
 * 简述：文件操作常用工具方法类 详述：主要提供以下几个方法(可自定义扩展):
 * <li>创建文件夹</li>
 * <li>从文件全名中获取后缀名 ,并转换成小写</li>
 * </p>
 */
public class FileUtils {
	
	protected static final Logger LOG = Logger.getLogger(FileUtils.class);

    /**
     * <p>
     * 创建文件夹,判断目录是否存在，不存在则创建
     * </p>
     * 
     * @param dirPath
     *            文件夹完整路径
     * @return boolean true:创建目录成功 false:创建目录失败
     */
    public static boolean createDir(String dirPath) {
        boolean fileExists = false;
        // 判断文件路径是否为空
        if (CommonUtils.isNotBlank(dirPath)) {
            File file = new File(dirPath);
            // 判断文件不存在
            if (!(fileExists = file.exists())) {
                // 创建目录
                fileExists = file.mkdirs();
            }
        }
        return fileExists;
    }

    /**
     * <p>
     * 创建文件,判断文件是否存在，不存在则创建
     * </p>
     * 
     * @param dirPath
     *            文件完整路径
     * @return boolean true:创建成功 false:创建失败
     * @throws IOException
     */
    public static boolean createFile(String filepath) throws IOException {
        boolean fileExists = false;
        // 判断文件路径是否为空
        if (CommonUtils.isNotBlank(filepath)) {
            File file = new File(filepath);
            // 判断文件不存在
            if (!(fileExists = file.exists())) {
                // 创建文件
                fileExists = file.createNewFile();
            }
        }
        return fileExists;
    }

    /**
     * 从文件路径获取目录路径
     * 
     * @param filepath
     * @return
     */
    public static String getFileDir(String filepath) {
        String dir = null;
        if (CommonUtils.isNotBlank(filepath) && filepath.indexOf("/") != -1) {
            dir = filepath.substring(0, filepath.lastIndexOf("/") + 1);
        }
        return dir;
    }

    /**
     * <p>
     * 从文件全名中获取后缀名 ,并转换成小写
     * </p>
     * 
     * @param fileName
     *            获取文件后缀名
     * @return String 后缀名成
     */
    public static String getFilePrefix(String fileName) {
        String prefix = null;
        // 判断文件名称不为空
        if (CommonUtils.isNotBlank(fileName)) {
            // 判断文件名称是否包含字符.
            if (fileName.lastIndexOf(".") != -1) {
                // 获取文件后缀
                prefix = fileName.substring(fileName.lastIndexOf(".") + Constants.NUM_INT1, fileName.length());
            }
        }
        return CommonUtils.isBlank(prefix) ? StringUtils.EMPTY : prefix.toLowerCase();
    }
    
    /** 
     *  
     * @param fileName 目标文件 
     * @param charset 目标文件的编码格式 
     */  
    public static void readFromEnd(String fileName, String charset) {  
        RandomAccessFile rf = null;  
        try {  
            rf = new RandomAccessFile(fileName, "r");  
            long len = rf.length();  
            long start = rf.getFilePointer();  
            long nextend = start + len - 1;  
            String line;  
            rf.seek(nextend);  
            int c = -1; 
            int braceRightCounter = 0;
            while (nextend > start) {  
                c = rf.read();  
                if (c == '\n' || c == '\r') {  
                    line = rf.readLine();  
                    if (Objects.nonNull(line)) {  
                    	String lineString = new String(line.getBytes("ISO-8859-1"), charset);
                    	if(lineString.contains(Consts.BRACE_RIGHT) && braceRightCounter < 2){ // 定位插如方法的位置
                    		braceRightCounter++; 
                    		//System.out.println(lineString);  
                    		//System.out.println(braceRightCounter);  
                    	}
                    } else {  
                        //System.out.println(line);  
                    }  
                    nextend--;  
                }  
                nextend--;  
                rf.seek(nextend);  
               /* if (nextend == 0) {// 当文件指针退至文件开始处，输出第一行  
                    // System.out.println(rf.readLine());  
                    System.out.println(new String(rf.readLine().getBytes("ISO-8859-1"), charset));  
                } */ 
            }  
        } catch (FileNotFoundException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                if (Objects.nonNull(rf))  
                    rf.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
    }
    
    
    /**
     * 演示向文件中写入字节流
     * 
     * @param fileName 要写入文件的文件名
     * @param contents 要写入的文件内容
     */
	public static void writeFile(final String contents, final String fileName) {
		checkNotNull(fileName, "Provided file name for writing must NOT be null.");
		checkNotNull(contents, "Unable to write null contents.");
		final File newFile = new File(fileName);
		try {
			Files.write(contents.getBytes(), newFile);
		} catch (IOException fileIoEx) {
			LOG.error("ERROR trying to write to file '" + fileName + "' - " + fileIoEx.toString());
		}
	}
    
    
    
    public static void main(String[] args) {
    	String fileName = "D:/neonWorkspace/tigercode/src/main/java/com/ccrc/tiger/util/Test.java";
		readFromEnd(fileName, Charsets.UTF_8.name());
	}
}
