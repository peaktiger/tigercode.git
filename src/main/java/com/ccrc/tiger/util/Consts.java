package com.ccrc.tiger.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class Consts {
	
	public final static String TIGER_CODE_NAME = "TigerCoder";
	public final static float TIGER_CODE_VERSION = 1.0f;
	
	public final static String PRIVATE = "private ";
	public final static String PUBLIC = "public ";
	public final static String OBJECT = "Object";
	public final static String STRING = "String";
	public final static String INTEGER = "Integer";
	public final static String LONG = "Long";
	public final static String FLOAT = "Float";
	public final static String DOUBLE = "Double";
	public final static String DATE = "Date";
	public final static String TIMESTAMP = "Timestamp";
	public final static String LBYTE = "byte[]";
	
	public final static String VOID = "void ";
	public final static String PRIVATE_OBJECT = "private Object ";
	public final static String PRIVATE_VOID = "private "+ VOID;
	public final static String PRIVATE_STRING = "private String ";
	public final static String PRIVATE_INTEGER = "private Integer ";
	public final static String PRIVATE_LONG = "private Long ";
	public final static String PRIVATE_FLOAT = "private Float ";
	public final static String PRIVATE_DOUBLE = "private Double ";
	public final static String PRIVATE_DATE = "private Date ";
	public final static String PRIVATE_TIMESTAMP = "private Timestamp ";
	public final static String PRIVATE_LBYTE = "private byte[] ";
	public final static String PUBLIC_VOID = "public "+ VOID;
	
	public final static String GET = "get";
	public final static String SET = "set";
	public final static String ENTER = "\r\n";
	public final static String TAB1 = "    ";
	public final static String TAB2 = "        ";
	public final static String TAB3 = "            ";
	public final static String TAB4 = "                    ";
	public final static String TAB5 = "                        ";
	
	public final static String BRACE_RIGHT = "}";
	
	public final static String XML_ELEMENT = "</mapper>";
	
	public final static String MYSQL_COLUMN_KEY = "PRI";
	public final static String ORACLE_COLUMN_KEY = "P";
	public final static String ORACLE_COLUMN_C = "C";
	public final static String COLUMN_NULL_NO = "NO";
	public final static String COLUMN_NULL_YES = "YES";
	public final static String COLUMN_GENERATED = "auto_increment";
	
	public final static String HIBERNATE_ID = "@Id";
	public final static String ANNOTATION_OVERRIDE = "@Override";
	public final static String ANNOTATION_TRANSACTIONAL = "@Transactional";
	public final static String ANNOTATION_RESPONSEBODY = "@ResponseBody";
	
	public final static String DAO_FRAME_HIBERNATE = "Hibernate";
	public final static String DAO_FRAME_MYBATIS = "Mybatis";
	
	public final static String MVC_FRAME_SPRING = "SpringMVC";
	public final static String MVC_FRAME_STRUTS2 = "Struts2";
	
	public final static String VIEW_FRAME_JSP = "JSP";
	
	public final static String VIEW_FRAME_JS = "jQueryEasyUI";
	
	public final static String IMPORT_DATE = "import java.util.Date;";
	public final static String IMPORT_LIST = "java.util.List";
	public final static String IMPORT_OBJECTS = "import com.ccrc.tiger.util.Objects";
	public final static String IMPORT_TIMESTAMP = "import java.util.Timestamp;";
	public final static String IMPORT_PAGER = "com.ccrc.lang.Pager";
	public final static String IMPORT_BASE_DAO = "com.ccrc.base.dao.Dao";
	public final static String IMPORT_BASE_DAOIMPL = "com.ccrc.base.dao.impl.DaoImpl";
	
	public final static String IMPORT_SPRING_AUTOWIRED = "import org.springframework.beans.factory.annotation.Autowired";
	public final static String IMPORT_SPRING_SERVICE = "import org.springframework.stereotype.Service";
	public final static String IMPORT_SPRING_TRANSACTIONAL = "import org.springframework.transaction.annotation.Transactional";
	public final static String IMPORT_SPRING_REPOSITORY = "import org.springframework.stereotype.Repository";
	
	public final static String IMPORT_HIBERNATE_PACKAGES = new StringBuffer()
														.append("import javax.persistence.Column;")
														.append(StringUtils.LF)
														.append("import javax.persistence.Entity;")
														.append(StringUtils.LF)
														.append("import javax.persistence.GeneratedValue;")	
														.append(StringUtils.LF)
														.append("import javax.persistence.GenerationType;")
														.append(StringUtils.LF)
														.append("import javax.persistence.Id;")	
														.append(StringUtils.LF)
														.append("import javax.persistence.JoinColumn;")	
														.append(StringUtils.LF)
														.append("import javax.persistence.ManyToOne;")
														.append(StringUtils.LF)
														.append("import javax.persistence.OneToMany;")
														.append(StringUtils.LF)
														.append("import javax.persistence.SequenceGenerator;")
														.append(StringUtils.LF)
														.append("import javax.persistence.Table;")
														.append(StringUtils.LF)
														.append("import org.hibernate.annotations.LazyCollection;")
														.append(StringUtils.LF)
														.append("import org.hibernate.annotations.LazyCollectionOption;")
														.append(StringUtils.LF)
														.append("import org.springframework.format.annotation.DateTimeFormat;")
														.append(StringUtils.LF)
														.append("import com.ccrc.lang.DateStyle;")
														.append(StringUtils.LF)
														.append("import com.fasterxml.jackson.annotation.JsonFormat;")
														.append(StringUtils.LF)
														.append("import org.hibernate.annotations.DynamicInsert;")
														.append(StringUtils.LF)
														.append("import org.hibernate.annotations.DynamicUpdate;")
														.append(StringUtils.LF)
														.toString();
	
	public final static String IMPORT_IDCLASS_PACKAGE = "import javax.persistence.IdClass;";
	
	
	public final static String IMPORT_MYBATIS_PACKAGES = new StringBuffer()
														.append("import com.fasterxml.jackson.annotation.JsonFormat;")
														.append(StringUtils.LF)
														.append("import com.ccrc.lang.DateStyle;")
														.append(StringUtils.LF)
														.toString();
	
	public final static String PAGER_HELPER_PACKAGES = new StringBuffer()
														.append("import com.github.pagehelper.Page;")
														.append(StringUtils.LF)
                                                        .append("import com.github.pagehelper.PageHelper;")
                                                        .append(StringUtils.LF)
 														.toString();
	
	public final static String IMPORT_JQUERY = "libs/jquery-easyui/jquery.min.js";
	public final static String IMPORT_JS_JOOE = "libs/jooe.core.js";
	public static final Integer GRID_WIDTH = 80;
	
	/******************************Mapper interface begin*************************************************/
	public static List<String> getMapperInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" selectByPrimaryKey(@Param(\""+ InId +"\") "+ InType +" "+ InId +");" + ENTER);
		return get;
	}
	
	public static List<String> getMapperInterfaceTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String mapperParams = getMapperParams(params);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" selectByPrimaryKey("+ mapperParams +");" + ENTER);
		return get;
	}

	public static List<String> getMapperInterfaceTmp (String methondBy, String returnParam, List<String> methondParam) {
		List<String> params = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] element = mp.split(" ");
			params.add("@Param(\""+ element[1] +"\") "+ element[0] +" "+ element[1]);
		}
		String methondParams = Joiner.on(", ").join(params);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ returnParam +" selectBy"+ methondBy +"("+methondParams +");"+ ENTER);
		return get;
	}
	
	public static List<String> findMapperInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" findByPrimaryKey(@Param(\""+ InId +"\") "+ InType +" "+ InId +");" + ENTER);
		return get;
	}
	
	public static List<String> findMapperInterfaceTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String mapperParams = getMapperParams(params);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" findByPrimaryKey("+ mapperParams +");" + ENTER);
		return get;
	}
	
	/*public static List<String> countMapperInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" countByPrimaryKey("+ InType +" "+ InId +");" + ENTER);
		return get;
	}*/
	
	public static List<String> findMapperInterfaceTmp (String methondBy, String returnParam, List<String> methondParam) {
		List<String> params = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] element = mp.split(" ");
			params.add("@Param(\""+ element[1] +"\") "+ element[0] +" "+ element[1]);
		}
		String methondParams = Joiner.on(", ").join(params);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ returnParam +" findBy"+ methondBy +"("+methondParams +");"+ ENTER);
		return get;
	}
	
	public static List<String> countMapperInterfaceTmp (String methondBy, List<String> methondParam) {
		List<String> params = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] element = mp.split(" ");
			params.add("@Param(\""+ element[1] +"\") "+ element[0] +" "+ element[1]);
		}
		String methondParams = Joiner.on(", ").join(params);
		
		List<String> get = Lists.newArrayList();
		get.add("\tLong countBy"+ methondBy +"("+methondParams +");"+ ENTER);
		return get;
	}
	
	public static List<String> getPageMapperInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> page = Lists.newArrayList();
		page.add("\tList<"+ entityName +"> selectPage("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return page;
	}
	
	public static List<String> getTotalMapperInterfaceTmp () {
		List<String> total = Lists.newArrayList();
		total.add("\tLong selectCount();" + ENTER);
		return total;
	}
	
	public static List<String> getInsertMapperInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> insert = Lists.newArrayList();
		insert.add("\tint insert("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return insert;
	}
	
	public static List<String> getUpdateMapperInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> update = Lists.newArrayList();
		update.add("\tint updateByPrimaryKey("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return update;
	}
	
	public static List<String> getDeleteMapperInterfaceTmp (Object InType, String InId) {
		List<String> delete = Lists.newArrayList();
		delete.add("\tint deleteByPrimaryKey(@Param(\""+ InId +"\") "+ InType +" "+ InId +");" + ENTER);
		return delete;
	}
	
	public static List<String> getDeleteMapperInterfaceTmpUnion (String params, String args) {
		List<String> delete = Lists.newArrayList();
		String mapperParams = getMapperParams(params);
		
		delete.add("\tint deleteByPrimaryKey("+ mapperParams +");" + ENTER);
		return delete;
	}
	
	/******************************Mapper interface end*************************************************/
	
	/******************************Mapper XML begin*************************************************/
	public static List<String> selectMappeXml(String tableName) {
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"selectByPrimaryKey\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where {PrimaryColumn} = #{{PrimaryFeild},jdbcType={PrimaryJdbcType}}");
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> selectMappeXmlUnion(String tableName, String params, String args) {
		
		String methodWhere = getMethodWhere(params);
		
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"selectByPrimaryKey\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where "+ methodWhere);
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}

	
	public static List<String> selectMappeXml(String tableName, String methondBy, List<String> methondParam) {
		List<String> conditions = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] params = mp.split(" ");
			String tmp = "{1} = #{{0},jdbcType={2}";
			conditions.add(CommonUtils.format(tmp, params[1], CommonUtils.convertToFirstLetterLowerCaseCamelCase(params[1]), Utililies.getMybatisJdbcType(params[0])));
		}
		String condition = Joiner.on("\n"+ TAB3 +"and ").join(conditions);
		
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"selectBy"+ methondBy +"\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where "+ condition +"}");
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> findMappeXml(String tableName) {
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"findByPrimaryKey\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where {PrimaryColumn} = #{{PrimaryFeild},jdbcType={PrimaryJdbcType}}");
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> findMappeXmlUnion(String tableName, String params, String args) {
		
		String methodWhere = getMethodWhere(params);
		
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"findByPrimaryKey\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where "+ methodWhere);
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> findMappeXml(String tableName, String methondBy, List<String> methondParam) {
		List<String> conditions = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] params = mp.split(" ");
			String tmp = "{1} = #{{0},jdbcType={2}";
			conditions.add(CommonUtils.format(tmp, params[1], CommonUtils.convertToFirstLetterLowerCaseCamelCase(params[1]), Utililies.getMybatisJdbcType(params[0])));
		}
		String condition = Joiner.on("\n"+ TAB3 +"and ").join(conditions);
		
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"findBy"+ methondBy +"\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select"); 
		select.add(TAB2 +"<include refid=\"Base_Column_List\" />");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where "+ condition +"}");
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> countMappeXml(String tableName, String methondBy, List<String> methondParam) {
		List<String> conditions = Lists.newArrayList();
		for (String mp : methondParam) {
			String[] params = mp.split(" ");
			String tmp = "{1} = #{{0},jdbcType={2}";
			conditions.add(CommonUtils.format(tmp, params[1], CommonUtils.convertToFirstLetterLowerCaseCamelCase(params[1]), Utililies.getMybatisJdbcType(params[0])));
		}
		String condition = Joiner.on("\n"+ TAB3 +"and ").join(conditions);
		
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"countBy"+ methondBy +"\" resultType=\"java.lang.{PrimaryJavaType}\">");
		select.add(TAB2 +"select count({PrimaryColumn})"); 
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where "+ condition +"}");
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> selectPageMappeXml(String tableName) {
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"selectPage\" parameterType=\"{EntityPackage}\" resultMap=\"BaseResultMap\">");
		select.add(TAB2 +"select ");
		select.add(TAB2 +"<include refid=\"Base_Column_List\"/>");
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB2 +"where 1=1");
		/*select.add(TAB2 +"<if test=\"curPage!=null and pageSize!=null\">");  
		select.add(TAB3 +"limit #{curPage},#{pageSize} "); 
		select.add(TAB2 +"</if>"); */
		select.add(TAB1 +"</select>" + ENTER);
		return select;
	}
	
	public static List<String> selectTotalMappeXml(String tableName){
		List<String> select = Lists.newArrayList();
		select.add(TAB1 +"<select id=\"selectCount\" resultType=\"java.lang.{PrimaryJavaType}\">");
		select.add(TAB2 +"select count({PrimaryColumn})"); 
		select.add(TAB2 +"from "+ tableName);
		select.add(TAB1 +"</select>"  + ENTER);
		return select;
	}
	
	public static List<String> insertMappeXml(String tableName) {
		List<String> insert = Lists.newArrayList();
		insert.add(TAB1 +"<insert id=\"insert\" parameterType=\"{EntityPackage}\">");
		insert.add(TAB2 + "{SelectKey}");
        insert.add(TAB2 +"insert into "+ tableName +" (");
        insert.add("{FeildJoin}");
        insert.add(TAB2 +")");
        insert.add(TAB2 +"values (");
        insert.add("{FeildMapJoin}");
        insert.add(TAB2 +")");
        insert.add(TAB1 +"</insert>"+ ENTER);
		return insert;
	}
	public static List<String> updateMappeXml(String tableName) {
		List<String> update = Lists.newArrayList();
		update.add(TAB1 +"<update id=\"updateByPrimaryKey\" parameterType=\"{EntityPackage}\">");
        update.add(TAB2 +"update "+ tableName);
        update.add(TAB2 +"set ");
        update.add(TAB2 +"{FeildSetList}");
        update.add(TAB2 +"where {PrimaryColumn} = #{{PrimaryFeild},jdbcType={PrimaryJdbcType}}");
        update.add(TAB1 +"</update>"+ ENTER);
		return update;
	}
	public static List<String> deleteMappeXml(String tableName) {
		List<String> delete = Lists.newArrayList();
		delete.add(TAB1 +"<delete id=\"deleteByPrimaryKey\">");
		delete.add(TAB2 +"delete from "+ tableName +" where {PrimaryColumn} = #{{PrimaryFeild},jdbcType={PrimaryJdbcType}}");
		delete.add(TAB1 +"</delete>"+ ENTER);
		return delete;
	}
	
	public static List<String> deleteMappeXmlUnion(String tableName, String params, String args) {
		List<String> delete = Lists.newArrayList();
		String methodWhere = getMethodWhere(params);
		
		delete.add(TAB1 +"<delete id=\"deleteByPrimaryKey\" >");
		delete.add(TAB2 +"delete from "+ tableName +" where " + methodWhere);
		delete.add(TAB1 +"</delete>"+ ENTER);
		return delete;
	}
	/******************************Mapper XML end***************************************************/
	
	/******************************Dao interface begin*************************************************/
	public static List<String> getPageDaoInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> page = Lists.newArrayList();
		page.add("\tList<"+ entityName +"> getPage"+ entityName +"(Pager pager, "+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return page;
	}
	
	public static List<String> getTotalDaoInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> total = Lists.newArrayList();
		total.add("\tLong getTotal"+ entityName +"("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return total;
	}
	
	public static List<String> getGetDaoInterfaceTmp (String entityName, String methondBy, String methondParams, String returnParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> getDaoInter = Lists.newArrayList();
		getDaoInter.add("\t"+ returnParam +" get"+ entityName +"By"+ methondBy +""+methondParams +";"+ ENTER);
		return getDaoInter;
	}
	
	public static List<String> getFindDaoInterfaceTmp (String entityName, String methondBy, String methondParams, String returnParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> findDaoInter = Lists.newArrayList();
		findDaoInter.add("\t"+ returnParam +" find"+ entityName +"By"+ methondBy +""+methondParams +";"+ ENTER);
		return findDaoInter;
	}
	
	public static List<String> getCountDaoInterfaceTmp (String entityName, String methondBy, String methondParams) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		List<String> findDaoInter = Lists.newArrayList();
		findDaoInter.add("\tLong count"+ entityName +"By"+ methondBy +""+methondParams +";"+ ENTER);
		return findDaoInter;
	}
	/******************************Dao interface end***************************************************/
	
	/******************************Dao impl begin*****************************************************/
	public static List<String> getPageDaoImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String unEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"List<"+ entityName +"> getPage"+ entityName +"(Pager pager, "+ entityName +" "+ unEntityName +") {");
		get.add("\t\tCriteria criteria = createCriteria();");
		get.add("\t\t// 添加条件此处 如:");
		get.add("\t\t//if (StringUtils.isNoneEmpty("+ unEntityName +".getUserName())) {");
		get.add("\t\t//	criteria.add(Restrictions.eq(\"userName\", "+ unEntityName +".getUserName()));");
		get.add("\t\t//}");
		get.add("\t\tcriteria.setFirstResult(pager.getCurPage());");
		get.add("\t\tcriteria.setMaxResults(pager.getPageSize());");
		get.add("\t\treturn criteria.list();");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getTotalDaoImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String unEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> total = Lists.newArrayList();
		total.add("\t"+ ANNOTATION_OVERRIDE);
		total.add("\t"+ PUBLIC +"Long getTotal"+ entityName +"("+ entityName +" "+ unEntityName +") {");
		total.add("\t\tCriteria criteria = createCriteria();");
		total.add("\t\t// 添加条件此处 如:");
		total.add("\t\t//if (StringUtils.isNoneEmpty("+ unEntityName +".getUserName())) {");
		total.add("\t\t//	criteria.add(Restrictions.eq(\"userName\", "+ unEntityName +".getUserName()));");
		total.add("\t\t//}");
		total.add("\t\treturn ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).longValue();");
		total.add("\t" + BRACE_RIGHT + ENTER);
		return total;
	}
	
	public static List<String> getGetDaoImplTmp (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> getDaoImpl = Lists.newArrayList();
		getDaoImpl.add("\t"+PUBLIC+ returnParam +" get"+ entityName +"By"+ methondBy +""+methondParams +" {");
		getDaoImpl.add("\t\tCriteria criteria = createCriteria();");
		
		for (String param : methondParam) {
			String[] pm = param.split(" ");
			if(Consts.STRING.equals(pm[0])) {
				getDaoImpl.add("\t\tif (StringUtils.isNotEmpty("+ pm[1] +")) {");
			} else {
				getDaoImpl.add("\t\tif (Objects.nonNull("+ pm[1] +")) {");
			}
			getDaoImpl.add("\t\t\tcriteria.add(Restrictions.eq(\""+ pm[1] +"\", "+ pm[1] +"));");
			getDaoImpl.add("\t\t"+ BRACE_RIGHT);
		}
		getDaoImpl.add("\t\treturn criteria.list();");
		getDaoImpl.add("\t" + BRACE_RIGHT + ENTER);
		return getDaoImpl;
	}
	
	public static List<String> findGetDaoImplTmp (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> findDaoImpl = Lists.newArrayList();
		findDaoImpl.add("\t"+PUBLIC+ returnParam +" find"+ entityName +"By"+ methondBy +""+methondParams +" {");
		findDaoImpl.add("\t\tCriteria criteria = createCriteria();");
		
		for (String param : methondParam) {
			String[] pm = param.split(" ");
			if(Consts.STRING.equals(pm[0])) {
				findDaoImpl.add("\t\tif (StringUtils.isNotEmpty("+ pm[1] +")) {");
			} else {
				findDaoImpl.add("\t\tif (Objects.nonNull("+ pm[1] +")) {");
			}
			findDaoImpl.add("\t\t\tcriteria.add(Restrictions.eq(\""+ pm[1] +"\", "+ pm[1] +"));");
			findDaoImpl.add("\t\t"+ BRACE_RIGHT);
		}
		findDaoImpl.add("\t\treturn criteria.list();");
		findDaoImpl.add("\t" + BRACE_RIGHT + ENTER);
		return findDaoImpl;
	}
	
	public static List<String> countDaoImplTmp (String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		List<String> countDaoImpl = Lists.newArrayList();
		countDaoImpl.add("\t"+PUBLIC+ "Long count"+ entityName +"By"+ methondBy +""+methondParams +" {");
		countDaoImpl.add("\t\tCriteria criteria = createCriteria();");
		
		for (String param : methondParam) {
			String[] pm = param.split(" ");
			if(Consts.STRING.equals(pm[0])) {
				countDaoImpl.add("\t\tif (StringUtils.isNotEmpty("+ pm[1] +")) {");
			} else {
				countDaoImpl.add("\t\tif (Objects.nonNull("+ pm[1] +")) {");
			}
			countDaoImpl.add("\t\t\tcriteria.add(Restrictions.eq(\""+ pm[1] +"\", "+ pm[1] +"));");
			countDaoImpl.add("\t\t"+ BRACE_RIGHT);
		}
		countDaoImpl.add("\t\treturn ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).longValue();");
		countDaoImpl.add("\t" + BRACE_RIGHT + ENTER);
		return countDaoImpl;
	}
	/******************************Dao impl end*******************************************************/
	
	/******************************Service interface begin***********************************************/
	
	public static List<String> getInterfaceTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(args, "Provided args name for writing must NOT be null.");
		
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" get"+ entityName +"By"+ methodEnd +"("+params+");" + ENTER);
		return get;
	}
	
	public static List<String> getInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" get"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+InType+" "+InId+");" + ENTER);
		return get;
	}
	
	public static List<String> findInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> find = Lists.newArrayList();
		find.add("\t"+ entityName +" find"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+InType+" "+InId+");" + ENTER);
		return find;
	}
	
	public static List<String> getInterfaceTmp(String entityName, String methondBy, String methondParams, String returnParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ returnParam +" get"+ entityName +"By"+ methondBy + methondParams +";" + ENTER);
		return get;
	}
	
	public static List<String> findInterfaceTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(args, "Provided args name for writing must NOT be null.");
		
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ entityName +" find"+ entityName +"By"+ methodEnd +"("+ params +");" + ENTER);
		return get;
	}
	
	public static List<String> findInterfaceTmp(String entityName, String methondBy, String methondParams, String returnParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ returnParam +" find"+ entityName +"By"+ methondBy + methondParams +";" + ENTER);
		return get;
	}
	
	public static List<String> getPageInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> page = Lists.newArrayList();
		page.add("\t"+ VOID +"getPage"+ entityName +"(Pager pager, "+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return page;
	}
	
	public static List<String> addInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> add = Lists.newArrayList();
		add.add("\t"+ VOID +"add"+ entityName +"("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return add;
	}
	
	public static List<String> modifyInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> modify = Lists.newArrayList();
		modify.add("\t"+ VOID +"modify"+ entityName +"("+ entityName +" "+ StringUtils.uncapitalize(entityName) +");" + ENTER);
		return modify;
	}
	
	public static List<String> removeInterfaceTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ VOID +"remove"+ entityName +"("+ params +");" + ENTER);
		return delete;
	}
	
	public static List<String> removeInterfaceTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ VOID +"remove"+ entityName +"("+ InType +" "+ InId +");" + ENTER);
		return delete;
	}
	
	public static List<String> countInterfaceTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\tLong count"+ entityName +"();" + ENTER);
		return get;
	}
	
	public static List<String> countInterfaceTmp(String entityName, String methondBy, String methondParams) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		List<String> get = Lists.newArrayList();
		get.add("\tLong count"+ entityName +"By"+ methondBy + methondParams +";" + ENTER);
		return get;
	}
	
	/******************************Service interface end***********************************************/
	
	
	/******************************Service impl begin***********************************************/
	public static List<String> getImplTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(args, "Provided args name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + entityName +" get"+ entityName +"By"+ methodEnd +"("+ params +") {");
		get.add("\t\t"+entityName+"PK pk = new "+entityName+"PK("+args+");");
		get.add("\t\treturn "+ uncapEntityName +"Dao.get(pk);");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getImplTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + entityName +" get"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+ InType +" "+ InId +") {");
		get.add("\t\treturn "+ uncapEntityName +"Dao.get("+ InId +");");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findImplTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> find = Lists.newArrayList();
		find.add("\t"+ ANNOTATION_OVERRIDE);
		find.add("\t"+ PUBLIC + entityName +" find"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+ InType +" "+ InId +") {");
		find.add("\t\treturn "+ uncapEntityName +"Dao.get("+ InId +");");
		find.add("\t" + BRACE_RIGHT + ENTER);
		return find;
	}
	
	public static List<String> getImplTmp2 (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + entityName +" get"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+InType+" "+InId+") {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.selectByPrimaryKey("+ InId +");");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getImplTmp2Union (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + entityName +" get"+ entityName +"By"+ methodEnd +"("+ params +") {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.selectByPrimaryKey("+ args +");");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getImplTmp (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + returnParam +" get"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Dao.get"+ entityName +"By"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getImplTmp2 (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + returnParam +" get"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.selectBy"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findImplTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String methodEnd = getMethodEnd(args);
		
		List<String> find = Lists.newArrayList();
		find.add("\t"+ ANNOTATION_OVERRIDE);
		find.add("\t"+ PUBLIC +""+ entityName +" find"+ entityName +"By"+ methodEnd +"("+params+") {");
		find.add("\t\t"+entityName+"PK pk = new "+entityName+"PK("+args+");");
		find.add("\t\treturn "+ uncapEntityName +"Dao.get(pk);");
		find.add("\t" + BRACE_RIGHT + ENTER);
		return find;
	}
	
	public static List<String> findImplTmp2 (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + returnParam +" find"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.findBy"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> countImplTmp2 (String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"Long count"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.countBy"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findImplTmp2 (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +""+ entityName +" find"+ entityName +"By"+ StringUtils.capitalize(InId) +"("+InType+" "+InId+") {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.findByPrimaryKey("+ InId +");");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findImplTmp2Union (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +""+ entityName +" find"+ entityName +"By"+ methodEnd +"("+ params +") {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.findByPrimaryKey("+ args +");");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> countImplTmp2 (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"Long count"+ entityName +"() {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.selectCount();");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> countImplTmp2Union (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"Long count"+ entityName +"() {");
		get.add("\t\treturn "+ uncapEntityName +"Mapper.selectCount();");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findImplTmp (String entityName, String methondBy, String methondParams, String returnParam, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		checkNotNull(returnParam, "Provided returnParam name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC + returnParam +" find"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Dao.find"+ entityName +"By"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> countImplTmp (String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"Long count"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn "+ uncapEntityName +"Dao.count"+ entityName +"By"+ methondBy + pms +";");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getPageImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC_VOID +"getPage"+ entityName +"(Pager pager, "+ entityName +" "+ uncapEntityName +") {");
		get.add("\t\tList<"+ entityName +"> rets = "+ uncapEntityName +"Dao.getPage"+ entityName +"(pager, "+ uncapEntityName +");");
		get.add("\t\tpager.setRows(rets);");
		get.add("\t\tpager.setTotal("+ uncapEntityName +"Dao.getTotal"+ entityName +"("+ uncapEntityName +"));");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> getPageImplTmp2 (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC_VOID +"getPage"+ entityName +"(Pager pager, "+ entityName +" "+ uncapEntityName +") {");
		get.add("\t\tPageHelper.startPage(pager.getCurPage(), pager.getPageSize());");
		get.add("\t\tList<"+ entityName +"> rets = "+ uncapEntityName +"Mapper.selectPage("+ uncapEntityName +");");
		get.add("\t\tpager.setRows(rets);");
		get.add("\t\tpager.setTotal(((Page<"+ entityName +">)rets).getTotal());");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> addImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> add = Lists.newArrayList();
		add.add("\t"+ ANNOTATION_TRANSACTIONAL);
		add.add("\t"+ ANNOTATION_OVERRIDE);
		add.add("\t"+ PUBLIC_VOID +"add"+ entityName +"("+ entityName +" "+uncapEntityName+") {" );
		add.add("\t\t"+ uncapEntityName +"Dao.save("+ uncapEntityName +");");
		add.add("\t" + BRACE_RIGHT + ENTER);
		return add;
	}
	
	public static List<String> addImplTmp2 (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> add = Lists.newArrayList();
		add.add("\t"+ ANNOTATION_TRANSACTIONAL);
		add.add("\t"+ ANNOTATION_OVERRIDE);
		add.add("\t"+ PUBLIC_VOID +"add"+ entityName +"("+ entityName +" "+uncapEntityName+") {" );
		add.add("\t\t"+ uncapEntityName +"Mapper.insert("+ uncapEntityName +");");
		add.add("\t" + BRACE_RIGHT + ENTER);
		return add;
	}
	
	public static List<String> modifyImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> modify = Lists.newArrayList();
		modify.add("\t"+ ANNOTATION_TRANSACTIONAL);
		modify.add("\t"+ ANNOTATION_OVERRIDE);
		modify.add("\t" + PUBLIC_VOID + "modify"+ entityName +"("+ entityName +" "+uncapEntityName+") {" );
		modify.add("\t\t"+ uncapEntityName +"Dao.update("+ uncapEntityName +");");
		modify.add("\t" + BRACE_RIGHT + ENTER);
		return modify;
	}
	
	public static List<String> modifyImplTmp2 (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> modify = Lists.newArrayList();
		modify.add("\t"+ ANNOTATION_TRANSACTIONAL);
		modify.add("\t"+ ANNOTATION_OVERRIDE);
		modify.add("\t" + PUBLIC_VOID + "modify"+ entityName +"("+ entityName +" "+uncapEntityName+") {" );
		modify.add("\t\t"+ uncapEntityName +"Mapper.updateByPrimaryKey("+ uncapEntityName +");");
		modify.add("\t" + BRACE_RIGHT + ENTER);
		return modify;
	}
	
	public static List<String> removeImplTmpUnion (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ ANNOTATION_TRANSACTIONAL);
		delete.add("\t"+ ANNOTATION_OVERRIDE);
		delete.add("\t" + PUBLIC_VOID + "remove"+entityName+"("+params+") {");
		delete.add("\t\t"+entityName+"PK pk = new "+entityName+"PK("+ args +");");
		delete.add("\t\t"+ uncapEntityName +"Dao.deleteById(pk);");
		delete.add("\t" + BRACE_RIGHT + ENTER);
		return delete;
	}
	
	public static List<String> removeImplTmp (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ ANNOTATION_TRANSACTIONAL);
		delete.add("\t"+ ANNOTATION_OVERRIDE);
		delete.add("\t" + PUBLIC_VOID + "remove"+entityName+"("+ InType +" "+InId+") {");
		delete.add("\t\t"+ uncapEntityName +"Dao.deleteById("+ InId +");");
		delete.add("\t" + BRACE_RIGHT + ENTER);
		return delete;
	}
	
	public static List<String> removeImplTmp2 (String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ ANNOTATION_TRANSACTIONAL);
		delete.add("\t"+ ANNOTATION_OVERRIDE);
		delete.add("\t" + PUBLIC_VOID + "remove"+entityName+"("+InType+" "+InId+") {");
		delete.add("\t\t"+ uncapEntityName +"Mapper.deleteByPrimaryKey("+InId+");");
		delete.add("\t" + BRACE_RIGHT + ENTER);
		return delete;
	}
	
	public static List<String> removeImplTmp2Union (String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> delete = Lists.newArrayList();
		delete.add("\t"+ ANNOTATION_TRANSACTIONAL);
		delete.add("\t"+ ANNOTATION_OVERRIDE);
		delete.add("\t" + PUBLIC_VOID + "remove"+entityName+"("+ params +") {");
		delete.add("\t\t"+ uncapEntityName +"Mapper.deleteByPrimaryKey("+ args +");");
		delete.add("\t" + BRACE_RIGHT + ENTER);
		return delete;
	}
	
	public static List<String> countImplTmp (String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> get = Lists.newArrayList();
		get.add("\t"+ ANNOTATION_OVERRIDE);
		get.add("\t"+ PUBLIC +"Long count"+ entityName +"() {");
		get.add("\t\treturn "+ uncapEntityName +"Dao.count();");
		get.add("\t" + BRACE_RIGHT + ENTER);
		return get;
	}
	
	/******************************Service impl end***********************************************/
	
	/******************************controller begin***********************************************/
	public static List<String> listPage(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> page = Lists.newArrayList();
		page.add("\t@RequestMapping(\"/ajaxPage\")");
		page.add("\t"+ ANNOTATION_RESPONSEBODY);
		page.add("\t"+ PUBLIC +"Pager getPage"+ entityName +"(int rows, int page, "+ entityName +" "+ uncapEntityName +") {");
		page.add("\t\tPager pager = new Pager(rows, page);");
		page.add("\t\t"+ uncapEntityName +"Service.getPage"+ entityName +"(pager, "+ uncapEntityName +");");
		page.add("\t\treturn pager;");
		page.add("\t"+ BRACE_RIGHT + ENTER);
		return page;
	}
	
	
	public static List<String> add(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> add = Lists.newArrayList();
		add.add("\t@RequestMapping(\"/ajaxAdd\")");
		add.add("\t"+ ANNOTATION_RESPONSEBODY);
		add.add("\t"+ PUBLIC +"Data ajaxAdd"+ entityName +"("+ entityName +" "+ uncapEntityName +") {");
		add.add("\t\t"+ uncapEntityName +"Service.add"+ entityName +"("+ uncapEntityName +");");
		add.add("\t\treturn Data.success(\"添加信息成功！\");");
		add.add("\t"+ BRACE_RIGHT + ENTER);
		return add;
	}
	
	public static List<String> modify(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> modify = Lists.newArrayList();
		modify.add("\t@RequestMapping(\"/ajaxModify\")");
		modify.add("\t"+ ANNOTATION_RESPONSEBODY);
		modify.add("\t"+ PUBLIC +"Data ajaxModify"+ entityName +"("+ entityName +" "+ uncapEntityName +") {");
		modify.add("\t\t"+ uncapEntityName +"Service.modify"+ entityName +"("+ uncapEntityName +");");
		modify.add("\t\treturn Data.success(\"修改信息成功！\");");
		modify.add("\t"+ BRACE_RIGHT + ENTER);
		return modify;
	}
	
	public static List<String> removeUnion(String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(params, "Provided params for writing must NOT be null.");
		checkNotNull(args, "Provided args for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> remove = Lists.newArrayList();
		remove.add("\t@RequestMapping(\"/ajaxRemove\")");
		remove.add("\t"+ ANNOTATION_RESPONSEBODY);
		remove.add("\t"+ PUBLIC +"Data ajaxRemove"+ entityName +"("+ params +") {");
		remove.add("\t\t"+uncapEntityName+"Service.remove"+ entityName +"("+ args +");");
		remove.add("\t\treturn Data.success(\"删除信息成功！\");");
		remove.add("\t"+ BRACE_RIGHT + ENTER);
		return remove;
	}
	
	public static List<String> remove(String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> remove = Lists.newArrayList();
		remove.add("\t@RequestMapping(\"/ajaxRemove\")");
		remove.add("\t"+ ANNOTATION_RESPONSEBODY);
		remove.add("\t"+ PUBLIC +"Data ajaxRemove"+ entityName +"("+ InType +" "+ InId +") {");
		remove.add("\t\t"+uncapEntityName+"Service.remove"+ entityName +"("+ InId +");");
		remove.add("\t\treturn Data.success(\"删除信息成功！\");");
		remove.add("\t"+ BRACE_RIGHT + ENTER);
		return remove;
	}
	
	public static List<String> getUnion(String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(params, "Provided params for writing must NOT be null.");
		checkNotNull(args, "Provided args for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		String methodEnd = getMethodEnd(args);
		
		List<String> get = Lists.newArrayList();
		get.add("\t@RequestMapping(\"/ajaxGet\")");
		get.add("\t" + ANNOTATION_RESPONSEBODY);
		get.add("\t"+ PUBLIC +"Data ajaxGet"+ entityName +"By"+ methodEnd +"("+ params +") {");
		get.add("\t\treturn Data.success("+ uncapEntityName +"Service.get"+ entityName +"By"+ methodEnd +"("+ args +"));");
		get.add("\t"+ BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> get(String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(InType, "Provided InType for writing must NOT be null.");
		checkNotNull(InId, "Provided InId for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		String capInId = StringUtils.capitalize(InId);
		
		List<String> get = Lists.newArrayList();
		get.add("\t@RequestMapping(\"/ajaxGet\")");
		get.add("\t" + ANNOTATION_RESPONSEBODY);
		get.add("\t"+ PUBLIC +"Data ajaxGet"+ entityName +"By"+ capInId +"("+InType+" "+ InId +") {");
		get.add("\t\treturn Data.success("+ uncapEntityName +"Service.get"+ entityName +"By"+ capInId +"("+ InId +"));");
		get.add("\t"+ BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> get(String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t@RequestMapping(\"/ajaxGet"+ entityName +"By"+ methondBy +"\")");
		get.add("\t" + ANNOTATION_RESPONSEBODY);
		get.add("\t"+ PUBLIC +"Data ajaxGet"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn Data.success("+ uncapEntityName +"Service.get"+ entityName +"By"+ methondBy + pms +");");
		get.add("\t"+ BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> findUnion(String entityName, String params, String args) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(params, "Provided params for writing must NOT be null.");
		checkNotNull(args, "Provided args for writing must NOT be null.");
		
		String methodEnd = getMethodEnd(args);
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> find = Lists.newArrayList();
		find.add("\t@RequestMapping(\"/ajaxFind\")");
		find.add("\t" + ANNOTATION_RESPONSEBODY);
		find.add("\t"+ PUBLIC +"Data ajaxFind"+ entityName +"By"+ methodEnd +"("+ params +") {");
		find.add("\t\treturn Data.success("+ uncapEntityName +"Service.find"+ entityName +"By"+ methodEnd +"("+ args +"));");
		find.add("\t"+ BRACE_RIGHT + ENTER);
		return find;
	}
	
	public static List<String> find(String entityName, Object InType, String InId) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(InType, "Provided InType for writing must NOT be null.");
		checkNotNull(InId, "Provided InId for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		String capInId = StringUtils.capitalize(InId);
		
		List<String> find = Lists.newArrayList();
		find.add("\t@RequestMapping(\"/ajaxFind\")");
		find.add("\t" + ANNOTATION_RESPONSEBODY);
		find.add("\t"+ PUBLIC +"Data ajaxFind"+ entityName +"By"+ capInId +"("+InType+" "+ InId +") {");
		find.add("\t\treturn Data.success("+ uncapEntityName +"Service.get"+ entityName +"By"+ capInId +"("+ InId +"));");
		find.add("\t"+ BRACE_RIGHT + ENTER);
		return find;
	}
	
	public static List<String> find(String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t@RequestMapping(\"/ajaxFind"+ entityName +"By"+ methondBy +"\")");
		get.add("\t" + ANNOTATION_RESPONSEBODY);
		get.add("\t"+ PUBLIC +"Data ajaxFind"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn Data.success("+ uncapEntityName +"Service.find"+ entityName +"By"+ methondBy + pms +");");
		get.add("\t"+ BRACE_RIGHT + ENTER);
		return get;
	}
	
	public static List<String> count(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> find = Lists.newArrayList();
		find.add("\t@RequestMapping(\"/ajaxCount\")");
		find.add("\t" + ANNOTATION_RESPONSEBODY);
		find.add("\t"+ PUBLIC +"Data ajaxCount"+ entityName +"() {");
		find.add("\t\treturn Data.success("+ uncapEntityName +"Service.count"+ entityName +"());");
		find.add("\t"+ BRACE_RIGHT + ENTER);
		return find;
	}
	
	public static List<String> count(String entityName, String methondBy, String methondParams, List<String> methondParam) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		checkNotNull(methondBy, "Provided methondBy name for writing must NOT be null.");
		checkNotNull(methondParams, "Provided methondParams name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		String pms = getActualParameter(methondParam);
		
		List<String> get = Lists.newArrayList();
		get.add("\t@RequestMapping(\"/ajaxCount"+ entityName +"By"+ methondBy +"\")");
		get.add("\t" + ANNOTATION_RESPONSEBODY);
		get.add("\t"+ PUBLIC +"Data ajaxCount"+ entityName +"By"+ methondBy + methondParams +" {");
		get.add("\t\treturn Data.success("+ uncapEntityName +"Service.count"+ entityName +"By"+ methondBy + pms +");");
		get.add("\t"+ BRACE_RIGHT + ENTER);
		return get;
	}
	/******************************controller end*************************************************/
	
	public static List<String> firstJsp(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName);
		
		List<String> firstJsp = Lists.newArrayList();
		
		firstJsp.add(getHref(uncapEntityName));
		return firstJsp;
	}
	
	public static String getHref (String entityName) {
		return "<a href=\"${ctx}/"+ entityName +"/list\"><c:out value=\"${ctx}/"+ entityName +"/list\"></c:out></a><br/>";
	}
	
	private static String getActualParameter(List<String> methondParam) {
		List<String> params = Lists.newArrayList();
		for (String param : methondParam) {
			String[] pm = param.split(" ");
			params.add(pm[1]);
		}
		String pms = "(" +Joiner.on(", ").join(params) + ")";
		return pms;
	}

	public static List<String> indexJsp(String entityName) {
		checkNotNull(entityName, "Provided entity name for writing must NOT be null.");
		
		String uncapEntityName = StringUtils.uncapitalize(entityName); 
		List<String> indexJsp = Lists.newArrayList();
		String li = "<li iconCls=\"icon-users\"><a href=\"javascript:void(0);\" data-icon=\"icon-users\" data-link=\"${ctx}/"+ uncapEntityName +"/list\" iframe=\"1\">"+ entityName +"</a></li>";
		indexJsp.add(li);
		return indexJsp;
	}

	public static String getMethodEnd(String args){
		String methodEnd = StringUtils.EMPTY;
		List<String> splitToList = Splitter.on(", ").splitToList(args);
		for(String filed : splitToList){
			methodEnd += StringUtils.capitalize(filed);
		}
		return methodEnd;
	}
	
	private static String getMapperParams(String params) {
		List<String> paramsData = Lists.newArrayList();
		List<String> splitToList = Splitter.on(", ").splitToList(params);
		for (String element : splitToList) {
			String[] split = element.split(StringUtils.SPACE);
			paramsData.add("@Param(\""+ split[1] +"\") "+ element);
		}
		String param = Joiner.on(", ").join(paramsData);
		return param;
	}
	
	private static String getMethodWhere(String params) {
		List<String> wheres = Lists.newArrayList();
		List<String> splitToList = Splitter.on(", ").splitToList(params);
		for (String element : splitToList) {
			String[] split = element.split(StringUtils.SPACE);
			String tmp = "{1} = #{{0},jdbcType={2}}";
			wheres.add(CommonUtils.format(tmp, CommonUtils.convertToFirstLetterLowerCaseCamelCase(split[1]), split[1], Utililies.getMybatisJdbcType(split[0])));
		}
		String join = Joiner.on(" and ").join(wheres);
		return join;
	}
}
