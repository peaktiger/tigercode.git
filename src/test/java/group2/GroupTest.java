/**
 * 
 */
package group2;

import java.math.BigDecimal;
import java.util.Formatter.BigDecimalLayoutForm;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;

/**
 * @author Jalen
 *
 */
public class GroupTest {

	//利用partition进行对数据进行分组  
    @Test  
    public void test26(){  
        List<String> list = ImmutableList.of("hello", "HI", "Hey");  
        List<List<String>> partition = Lists.partition(list, 2);  
        System.out.println(partition);  
    }  
  
//Lists中的transform方法（通过函数式接口）能够对数据进行处理  
    @Test  
    public void test25(){  
        List<String> list = ImmutableList.of("hello", "HI", "Hey");  
        List<String> transform = Lists.transform(list, new Function<String, String>() {  
            @Override  
            public String apply(String input) {  
                if(input.contentEquals("HI"))  
                    System.out.println(input);  
                return input;  
            }  
        });  
        System.out.println(transform);  
    }  
  
//通过函数式接口，把处理后的数据放到一个map中  
    // 根据特征进行筛选集合中的数据  
    @Test  
    public void test12() {  
        ImmutableSet<String> digits = ImmutableSet.of("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");  
        Function<String, Integer> lengthFunction = new Function<String, Integer>() {  
            public Integer apply(String string) {  
                return string.length();  
            }  
        };  
        ImmutableListMultimap<Integer, String> digitsByLength = Multimaps.index(digits, lengthFunction);  
        System.out.println(digitsByLength);  
        /* 
         * digitsByLength maps: 3 => {"one", "two", "six"} 4 => {"zero", "four", 
         * "five", "nine"} 5 => {"three", "seven", "eight"} 
         */  
    }  
    
    
    
    
    @Test
    public void testMap(){
    	List<Person> persons = Lists.newArrayList(
                new Person("zhang", 15),
                new Person("wang", 15),
                new Person("lee", 18)
        );
        Map<String, Person> map = Maps.uniqueIndex(persons, new Function<Person, String>() {
            public String apply(Person person) {
                return person.getName();
            }
        });

        map = Maps.filterEntries(map, new Predicate<Map.Entry<String, Person>>() {
            @Override
            public boolean apply(Map.Entry<String, Person> stringPersonEntry) {
                return stringPersonEntry.getKey() != null && stringPersonEntry.getValue() != null;
            }
        });
        
        map = Maps.filterKeys(map, new Predicate<String>() {
            @Override
            public boolean apply(String s) {
                return s != null && s.length() > 2;
            }
        });

        map = Maps.filterValues(map, new Predicate<Person>() {
            @Override
            public boolean apply(Person person) {
                return person != null && person.getAge() > 18;
            }
        });


        Map<String, Person> map1 = map;
        for (Map.Entry<String, Person> entry : map1.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        
        
    }

}
