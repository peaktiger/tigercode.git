package com.ccrc.tiger.main;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import com.ccrc.tiger.util.Objects;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import com.ccrc.tiger.form.AboutForm;
import com.ccrc.tiger.form.MainForm;
import com.ccrc.tiger.panel.BasePanel;
import com.ccrc.tiger.panel.DatabaseSetting;
import com.ccrc.tiger.panel.EntitySetting;
import com.ccrc.tiger.panel.ProcessPanel;
import com.ccrc.tiger.panel.SpringHibernatePanel;
import com.ccrc.tiger.panel.SpringMybatisPanel;
import com.ccrc.tiger.panel.TableDataPanel;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

@SuppressWarnings("serial")
public class MainTiger extends JPanel {
    public static JLayeredPane interFrame = null;
    public static Map<String, BasePanel> panelMap = Maps.newHashMap();
    public static Integer layeredLevel = 1;
    public static int width = 900;
    public static int height = 600;

    public static MainTiger mainTiger = null;
    public static MainForm mainForm = null;

    public DatabaseSetting dbsetPanel;
    public EntitySetting entitySetPanel;
    public TableDataPanel tableDataPanel;
    public SpringHibernatePanel springHibernatePanel;
    public SpringMybatisPanel springMybatisPanel;
    public ProcessPanel processPanel;
    public AboutForm aboutForm;
    
    public MainTiger() {
    }

    protected void initComponents() {
        this.setSize(width, height);
        this.setLayout(null);
        createControlPanel();
    }

    protected void createControlPanel() {
        interFrame = new JLayeredPane();

        JMenuBar menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, width, 30);
        JMenu datasourceCfgMenu = new JMenu(Constants.BUTTON_DBSOURCE_CFG_NAME);
        JMenu setUpMenu = new JMenu(Constants.BUTTON_SETUP_NAME);
        JMenu optManualMenu = new JMenu(Constants.BUTTON_OPT_MANUAL);
        JMenu aboutMenu = new JMenu(Constants.BUTTON_ABOUT_NAME);
        
        JMenuItem databaseItem = new JMenuItem(Constants.BUTTON_DB_CFG_NAME);
        databaseItem.setName("databaseSet");
        datasourceCfgMenu.add(databaseItem);
        
        JMenuItem loadDbItem = new JMenuItem(Constants.BUTTON_LOAD_DB_NAME);
        loadDbItem.setName("loadDbItem");
        datasourceCfgMenu.add(loadDbItem);
        
        JMenuItem setUpEntityItem = new JMenuItem(Constants.BUTTON_ENTITY_SET_NAME);
        setUpEntityItem.setName("setUpEntityItem");
        setUpMenu.add(setUpEntityItem);
        
        JMenuItem springHibernateItem = new JMenuItem(Constants.SPRING_HIBERNATE_DEV);
        springHibernateItem.setName("springHibernateDev");
        optManualMenu.add(springHibernateItem);
        
        JMenuItem springMybatisItem = new JMenuItem(Constants.SPRING_MYBATIS_DEV);
        springMybatisItem.setName("springMybatisDev");
        optManualMenu.add(springMybatisItem);
        
        JMenuItem aboutItem = new JMenuItem(Constants.BUTTON_ABOUT_NAME);
        aboutItem.setName("aboutItem");
        aboutMenu.add(aboutItem);

        databaseItem.addActionListener(new ShowActionListener());
        loadDbItem.addActionListener(new ShowActionListener());
        setUpEntityItem.addActionListener(new ShowActionListener());
        springHibernateItem.addActionListener(new ShowActionListener());
        springMybatisItem.addActionListener(new ShowActionListener());
        aboutItem.addActionListener(new ShowActionListener());

        menuBar.add(datasourceCfgMenu);
        menuBar.add(setUpMenu);
        menuBar.add(optManualMenu);
        menuBar.add(aboutMenu);
        this.add(menuBar);

        interFrame.setLayout(null);
        this.add(interFrame);

        interFrame.setBounds(0, 30, width, height - 30);
        
        mainForm = new MainForm(interFrame.getWidth(), interFrame.getHeight());
        interFrame.add(mainForm, layeredLevel++);
        panelMap.put("mainPanel", mainForm);
        mainForm.setBounds(0, 0, interFrame.getWidth(), interFrame.getHeight());
    }
    
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    // 设置本属性将改变窗口边框样式定义
                    System.setProperty("sun.java2d.noddraw", "true");
                    BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
                    BeautyEyeLNFHelper.launchBeautyEyeLNF();
                    UIManager.put("RootPane.setupButtonVisible", false);
                } catch (Exception e) {
                	Throwables.throwIfUnchecked(e);
                }
                
                JFrame frame = new JFrame();
                frame.setTitle(Consts.TIGER_CODE_NAME +" v" + Consts.TIGER_CODE_VERSION);
                frame.setResizable(false);
                mainTiger = new MainTiger();
                mainTiger.initComponents();
                double lx = Toolkit.getDefaultToolkit().getScreenSize().getWidth();  
                double ly = Toolkit.getDefaultToolkit().getScreenSize().getHeight();  
                frame.setBounds((int)(lx - width) / 2, (int)(ly - height) / 2, width, height);
                frame.add(mainTiger);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                Toolkit toolkit = Toolkit.getDefaultToolkit(); 
                Dimension scmSize = toolkit.getScreenSize(); 
                frame.setLocation(scmSize.width/2 - (width/2), scmSize.height/2 - (height/2)); 
                frame.setVisible(true);
            }
        });
    }

    public void showTableDataPanel(String tableName){
        tableDataPanel = (TableDataPanel) panelMap.get("tableDataPanel");
        if (Objects.isNull(tableDataPanel)) {
            tableDataPanel = new TableDataPanel(width, height - 30);
            panelMap.put("tableDataPanel", tableDataPanel);
            interFrame.add(tableDataPanel, layeredLevel++);
            tableDataPanel.setLocation(0, 0);
        } else {
            interFrame.remove(tableDataPanel);
            interFrame.add(tableDataPanel, layeredLevel++);
        }
        tableDataPanel.viewTableData(tableName);
        tableDataPanel.showPanel();
    }
    
    public void showDatabaseSetPanel(){
        dbsetPanel = (DatabaseSetting) panelMap.get("dbsetPanel");
        if (Objects.isNull(dbsetPanel)) {
            dbsetPanel = new DatabaseSetting(width, height - 30);
            panelMap.put("dbsetPanel", dbsetPanel);
            interFrame.add(dbsetPanel, layeredLevel++);
            dbsetPanel.setLocation(0, 0);
        } else {
            interFrame.remove(dbsetPanel);
            interFrame.add(dbsetPanel, layeredLevel++);
        }
        dbsetPanel.showPanel();
    }
    
    public void showProcessPanel(){
        processPanel = (ProcessPanel) panelMap.get("processPanel");
        if (Objects.isNull(processPanel)) {
            processPanel = new ProcessPanel(width, height - 30);
            panelMap.put("processPanel", processPanel);
            interFrame.add(processPanel, layeredLevel++);
            processPanel.setLocation(0, 0);
        } else {
            interFrame.remove(processPanel);
            interFrame.add(processPanel, layeredLevel++);
        }
        processPanel.showPanel();
    }
    
    /**
     * 实体类名设置
     */
    public void showEntitySetPanel() {
    	entitySetPanel = (EntitySetting) panelMap.get("entitySetPanel");
        if (Objects.isNull(entitySetPanel)) {
        	entitySetPanel = new EntitySetting(width, height - 30);
            panelMap.put("entitySetPanel", entitySetPanel);
            interFrame.add(entitySetPanel, layeredLevel++);
            entitySetPanel.setLocation(0, 0);
        } else {
            interFrame.remove(entitySetPanel);
            interFrame.add(entitySetPanel, layeredLevel++);
        }
        entitySetPanel.showPanel();
    }
    
    /**
     * spring + hibernate 开发
     */
    public void showSpringHibernatePanel() {
    	springHibernatePanel = (SpringHibernatePanel) panelMap.get("springHibernatePanel");
    	if (Objects.isNull(springHibernatePanel)) {
    		springHibernatePanel = new SpringHibernatePanel(width, height - 30);
            panelMap.put("springHibernatePanel", springHibernatePanel);
            interFrame.add(springHibernatePanel, layeredLevel++);
            springHibernatePanel.setLocation(0, 0);
        } else {
            interFrame.remove(springHibernatePanel);
            interFrame.add(springHibernatePanel, layeredLevel++);
        }
    	springHibernatePanel.showPanel();
    }
    
    /**
     * spring + Mybatis 开发
     */
    public void showSpringMybatisPanel() {
    	springMybatisPanel = (SpringMybatisPanel) panelMap.get("springMybatisPanel");
    	if (Objects.isNull(springMybatisPanel)) {
    		springMybatisPanel = new SpringMybatisPanel(width, height - 30);
            panelMap.put("springMybatisPanel", springMybatisPanel);
            interFrame.add(springMybatisPanel, layeredLevel++);
            springMybatisPanel.setLocation(0, 0);
        } else {
            interFrame.remove(springMybatisPanel);
            interFrame.add(springMybatisPanel, layeredLevel++);
        }
    	springMybatisPanel.showPanel();
    }
    
    public void hideProcessPanel(){
        processPanel = (ProcessPanel) panelMap.get("processPanel");
        if (Objects.nonNull(processPanel)) {
            processPanel.hidePanel();
        }
    }
    
    public void loadDbData(){
        if(Objects.nonNull(mainForm)){
            mainForm.initTreeThread(0);
        }
    }
    
    private class ShowActionListener implements ActionListener {
        private ShowActionListener() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            String name = actionEvent.getActionCommand();
            //System.out.println("ShowActionListener name= "+ name);
            
            if (name.equals(Constants.BUTTON_DB_CFG_NAME)) {
                showDatabaseSetPanel();
            } else if (name.equals(Constants.BUTTON_LOAD_DB_NAME)) {
                loadDbData();
            } else if (name.equals(Constants.BUTTON_ENTITY_SET_NAME)) {
            	showEntitySetPanel();
            } else if (name.equals(Constants.SPRING_HIBERNATE_DEV)) {
            	showSpringHibernatePanel();
            	
            	if (Objects.nonNull(springMybatisPanel)) {
            		springMybatisPanel.hidePanel();
            	}
            } else if (name.equals(Constants.SPRING_MYBATIS_DEV)) {
            	showSpringMybatisPanel();
            	
            	if (Objects.nonNull(springHibernatePanel)) {
            		springHibernatePanel.hidePanel();
            	}
            } else if(name.equals(Constants.BUTTON_ABOUT_NAME)){
                if(Objects.isNull(aboutForm)){
                    aboutForm = new AboutForm();
                    aboutForm.showForm();
                } else {
                    aboutForm.showForm();
                }
            }
        }
    }
    
}
