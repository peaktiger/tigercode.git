/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.ccrc.lang.DateStyle;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Constants;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.FileUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 创建 service 层
 */
public class CreateService extends Creator {

	public CreateService(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
		// TODO Auto-generated constructor stub
	}
	
	public String createMybatisIService() {
		return createHibernateIService();
	}

	public String createHibernateIService() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getIserviceFilePath()) + "I"+ getEntityName() +"Service.java");
		String result = "I"+ getEntityName() +"Service接口生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "I"+ getEntityName() +"Service接口已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("hibernate_service.tlp"));
				// package
				content = Utililies.parseTemplate(content, "ServicePackage", Utililies.getSuperPackage(ca.getIservicePackage()));
				content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				/*if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					content = Utililies.parseTemplate(content, "EntityPKPackage", ca.getDaoFrame().equalsIgnoreCase("Hibernate") ? "import " +ca.getEntityPackage() + "PK;" : StringUtils.EMPTY);
				} else {
					content = Utililies.parseTemplate(content, "EntityPKPackage", StringUtils.EMPTY);
				}*/
				//content = Utililies.parseTemplate(content, "Parameters", getCurTableParameters());
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ? 
																		getTableComment() : StringUtils.EMPTY);
				
				//Utililies.writeContentToFile(javaPath, content);
				Files.write(content.getBytes(), javaFile);
			}
			
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.getInterfaceTmp(getEntityName(), methondBys, methondParams, returnParams));
					Files.write(content.getBytes(), javaFile);
				} else {
					
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.getInterfaceTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.getInterfaceTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.findInterfaceTmp(getEntityName(), methondBys, methondParams, returnParams));
					Files.write(content.getBytes(), javaFile);
				} else {
					
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.findInterfaceTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.findInterfaceTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.countInterfaceTmp(getEntityName(), methondBys, methondParams));
					Files.write(content.getBytes(), javaFile);
				} else {
					//String[] curColumn = getCurColumnKey();
					String content = createSingleMethod(javaFile, Consts.countInterfaceTmp(getEntityName()));
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.getPageInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getAddOp()) {
				String content = createSingleMethod(javaFile, Consts.addInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()){
				String content = createSingleMethod(javaFile, Consts.modifyInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				String content = StringUtils.EMPTY;
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKParams().split(Constants.CHAR_AND);
					content = createSingleMethod(javaFile, Consts.removeInterfaceTmpUnion(getEntityName(), split[0], split[1]));
				} else {
					String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.removeInterfaceTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
				}
				Files.write(content.getBytes(), javaFile);
			}
		} catch (Exception e) {
			result = "I"+ getEntityName() +"Service接口生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createHibernateImplService() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getIserviceFilePath()) + getEntityName() +"Service.java");
		String result = getEntityName() +"Service生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() +"Service已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("hibernate_service_impl.tlp"));
				// package
				content = Utililies.parseTemplate(content, "ServicePackage", Utililies.getSuperPackage(ca.getIservicePackage()));
				content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				content = Utililies.parseTemplate(content, "entityName", StringUtils.uncapitalize(getEntityName()));
				content = Utililies.parseTemplate(content, "DaoPackage", Utililies.getSuperPackage(ca.getDaoPackage()) + ".I" + getEntityName() +"Dao");
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					content = Utililies.parseTemplate(content, "EntityPKPackage", ca.getDaoFrame().equalsIgnoreCase("Hibernate") ? "import " +ca.getEntityPackage() + "PK;" : StringUtils.EMPTY);
				} else {
					content = Utililies.parseTemplate(content, "EntityPKPackage", StringUtils.EMPTY);
				}
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ?
																		getTableComment() : StringUtils.EMPTY);
				Files.write(content.getBytes(), javaFile);
			}
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.getImplTmp(getEntityName(), methondBys, methondParams, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.getImplTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.getImplTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.findImplTmp(getEntityName(), methondBys, methondParams, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					String content = StringUtils.EMPTY;
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.findImplTmpUnion(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.findImplTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					String content = createSingleMethod(javaFile, Consts.countImplTmp(getEntityName(), methondBys, methondParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					//String[] curColumn = getCurColumnKey();
					String content = createSingleMethod(javaFile, Consts.countImplTmp(getEntityName()));
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.getPageImplTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getAddOp()) {
				String content = createSingleMethod(javaFile, Consts.addImplTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()){
				String content = createSingleMethod(javaFile, Consts.modifyImplTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				String content = StringUtils.EMPTY;
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKParams().split(Constants.CHAR_AND);
					content = createSingleMethod(javaFile, Consts.removeImplTmpUnion(getEntityName(), split[0], split[1]));
				} else {
					String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.removeImplTmp(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
				}
				Files.write(content.getBytes(), javaFile);
			}
		}catch (Exception e) {
			result = getEntityName() +"Service生成失败：" + e.getMessage();
		}
		return result;
	}

	
	public String createMybatisImplService() {
		List<DbTableColumn> primaryKeyColumns = getPrimaryKeyColumns();
		
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getIserviceFilePath()) + getEntityName() +"Service.java");
		String result = getEntityName() +"Service生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() +"Service已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("mybatis_service_impl.tlp"));
				// package
				content = Utililies.parseTemplate(content, "ServicePackage", Utililies.getSuperPackage(ca.getIservicePackage()));
				content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				content = Utililies.parseTemplate(content, "entityName", StringUtils.uncapitalize(getEntityName()));
				content = Utililies.parseTemplate(content, "DaoPackage", Utililies.getSuperPackage(ca.getDaoPackage()) + "." + getEntityName() +"Mapper");
				content = Utililies.parseTemplate(content, "PagerHelperPackage", Consts.PAGER_HELPER_PACKAGES);
				
				
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ? 
																			getTableComment() : StringUtils.EMPTY);
				Files.write(content.getBytes(), javaFile);
			}
			// 根据选择的crud生成相应的功能
			String content = StringUtils.EMPTY;
			if (ca.getGetOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					content = createSingleMethod(javaFile, Consts.getImplTmp2(getEntityName(), methondBys, methondParams, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.getImplTmp2Union(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.getImplTmp2(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getFindOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					content = createSingleMethod(javaFile, Consts.findImplTmp2(getEntityName(), methondBys, methondParams, returnParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					// 判断是否是联合主键，如果是 通过联合主键查询
					if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
						String[] split = unionPKParams().split(Constants.CHAR_AND);
						content = createSingleMethod(javaFile, Consts.findImplTmp2Union(getEntityName(), split[0], split[1]));
					} else {
						String[] curColumn = getCurColumnKey();
						content = createSingleMethod(javaFile, Consts.findImplTmp2(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
					}
					Files.write(content.getBytes(), javaFile);
				}
			}
			if (ca.getCountOp()) {
				if (StringUtils.isNotEmpty(methondBys)) {
					content = createSingleMethod(javaFile, Consts.countImplTmp2(getEntityName(), methondBys, methondParams, methondParam));
					Files.write(content.getBytes(), javaFile);
				} else {
					//String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.countImplTmp2(getEntityName()));
					
					Files.write(content.getBytes(), javaFile);
				}
				
			}
			if (ca.getPageOp()) {
				content = createSingleMethod(javaFile, Consts.getPageImplTmp2(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getAddOp()) {
				content = createSingleMethod(javaFile, Consts.addImplTmp2(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getModifyOp()){
				content = createSingleMethod(javaFile, Consts.modifyImplTmp2(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getDeleteOp()) {
				// 判断是否是联合主键，如果是 通过联合主键查询
				if (CollectionUtils.isNotEmpty(primaryKeyColumns) && primaryKeyColumns.size() > 1) {
					String[] split = unionPKParams().split(Constants.CHAR_AND);
					content = createSingleMethod(javaFile, Consts.removeImplTmp2Union(getEntityName(), split[0], split[1]));
				} else {
					String[] curColumn = getCurColumnKey();
					content = createSingleMethod(javaFile, Consts.removeImplTmp2(getEntityName(), Utililies.getJavaType(curColumn[1]), CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0])));
				}
				Files.write(content.getBytes(), javaFile);
			}
		}catch (Exception e) {
			result = getEntityName() +"Service生成失败：" + e.getMessage();
		}
		return result;
	}
	
}
