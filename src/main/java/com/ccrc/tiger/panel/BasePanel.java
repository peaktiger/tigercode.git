package com.ccrc.tiger.panel;

import java.awt.Dimension;
import com.ccrc.tiger.util.Objects;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BasePanel extends JPanel {

    protected static BasePanel panel = null;

    private static int width = 800;
    private static int height = 600;

    public BasePanel() {
        setPreferredSize(new Dimension(width, height));
    }

    public BasePanel(int width, int height) {
        BasePanel.width = width;
        BasePanel.height = height;
        setPreferredSize(new Dimension(width, height));
    }

    public static BasePanel getInstance() {
        if (Objects.isNull(panel)) {
            panel = new BasePanel();
        }
        return panel;
    }

    public static BasePanel getInstance(int width, int height) {
        BasePanel.width = width;
        BasePanel.height = height;
        if (Objects.isNull(panel)) {
            panel = new BasePanel();
        }
        return panel;
    }

    public void showPanel() {
        this.setVisible(true);
    }
    
    public void hidePanel() {
    	this.setVisible(false);
    }
}
