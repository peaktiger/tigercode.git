/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.ccrc.lang.DateStyle;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.FileUtils;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 基于Hibernate创建 Dao层
 */
public class CreateDaoHibernate extends Creator {

	public CreateDaoHibernate(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}
	
	public String createHibernateIDao() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getDaoFilePath()) + "I"+ getEntityName() +"Dao.java");
		String result = "I"+ getEntityName() +"Dao接口生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = "I"+ getEntityName() +"Dao接口已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("hibernate_dao.tlp"));
				// package
				content = Utililies.parseTemplate(content, "DaoSuperPackage", Consts.IMPORT_BASE_DAO);
				content = Utililies.parseTemplate(content, "DaoPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "DaoClassName", "I"+ getEntityName() +"Dao");
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				
				
				//content = Utililies.parseTemplate(content, "Parameters", getCurTableParameters());
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ?
																		getTableComment() : StringUtils.EMPTY);
				
				//Utililies.writeContentToFile(javaPath, content);
				Files.write(content.getBytes(), javaFile);
			}
			
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.getGetDaoInterfaceTmp(getEntityName(), methondBys, methondParams, returnParams));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getFindOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.getFindDaoInterfaceTmp(getEntityName(), methondBys, methondParams, returnParams));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getCountOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.getCountDaoInterfaceTmp(getEntityName(), methondBys, methondParams));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.getPageDaoInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
				
				content = createSingleMethod(javaFile, Consts.getTotalDaoInterfaceTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
		} catch (Exception e) {
			result = "I"+ getEntityName() +"Dao接口生成失败：" + e.getMessage();
		}
		return result;
	}

	public String createHibernateImplDao() {
		String javaPath = Utililies.parseFilePath(ca.getSaveDir(), FileUtils.getFileDir(ca.getDaoFilePath()) + getEntityName() +"Dao.java");
		String result = getEntityName() +"Dao生成完成：" + Utililies.parseFilePath(ca.getSaveDir(), javaPath);
		try {
			File javaFile = new File(javaPath);
			if(javaFile.exists()){
				result = getEntityName() +"Dao已存在，无须重新生成！";
			} else {
				String content = Utililies.readResourceFile(getTlpPath("hibernate_dao_impl.tlp"));
				// package
				content = Utililies.parseTemplate(content, "DaoSuperPackage", Consts.IMPORT_BASE_DAOIMPL);
				content = Utililies.parseTemplate(content, "DaoPackage", Utililies.getSuperPackage(ca.getDaoPackage()));
				content = Utililies.parseTemplate(content, "PagerPackage", Consts.IMPORT_PAGER);
				content = Utililies.parseTemplate(content, "EntityPackage", ca.getEntityPackage());
				content = Utililies.parseTemplate(content, "DaoClassName", getEntityName() +"Dao");
				content = Utililies.parseTemplate(content, "EntityName", getEntityName());
				
				content = Utililies.parseTemplate(content, "Author", SystemUtils.USER_NAME);
				content = Utililies.parseTemplate(content, "Time", DateFormatUtils.format(new Date(), DateStyle.YYYY_MM_DD_HH_MM_CN));
				content = Utililies.parseTemplate(content, "EntityDesc", StringUtils.isNoneEmpty(getTableComment()) ?
																			getTableComment() : StringUtils.EMPTY);
				Files.write(content.getBytes(), javaFile);
			}
			// 根据选择的crud生成相应的功能
			if (ca.getGetOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.getGetDaoImplTmp(getEntityName(), methondBys, methondParams, returnParams, methondParam));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getFindOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.findGetDaoImplTmp(getEntityName(), methondBys, methondParams, returnParams, methondParam));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getCountOp() && StringUtils.isNotEmpty(methondBys)) {
				String content = createSingleMethod(javaFile, Consts.countDaoImplTmp(getEntityName(), methondBys, methondParams, methondParam));
				Files.write(content.getBytes(), javaFile);
			}
			if (ca.getPageOp()) {
				String content = createSingleMethod(javaFile, Consts.getPageDaoImplTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
				
				content = createSingleMethod(javaFile, Consts.getTotalDaoImplTmp(getEntityName()));
				Files.write(content.getBytes(), javaFile);
			}
		}catch (Exception e) {
			result = getEntityName() +"Dao生成失败：" + e.getMessage();
		}
		return result;
	}
	
	
}
