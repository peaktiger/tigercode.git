package com.ccrc.tiger.form;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ccrc.tiger.util.Objects;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ccrc.tiger.componet.CheckBoxNode;
import com.ccrc.tiger.componet.CheckBoxNodeEditor;
import com.ccrc.tiger.componet.CheckBoxNodeRenderer;
import com.ccrc.tiger.componet.NamedVector;
import com.ccrc.tiger.creator.CreatorThread;
import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.db.DbCache;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTable;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.main.MainTiger;
import com.ccrc.tiger.panel.BasePanel;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.IniReader;
import com.ccrc.tiger.util.Utililies;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

@SuppressWarnings("serial")
public class MainForm extends BasePanel {
	private final static Logger LOG = Logger.getLogger(MainForm.class);

	private int width = 900;
	private int height = 600;
	private boolean configInited = false;
	private boolean loadding = false;
	private String config_section = "create_config_info";

	private static JTree tree = null;
	private NamedVector dbVector;
	private JScrollPane jScrollPane1;
	public static Map<String, Vector<Object>> columnMap = Maps.newHashMap();
	// 设置被选择的属性
	Enumeration<DefaultMutableTreeNode> children;

	public MainForm() {
		this.setBackground(Color.white);
		this.setLayout(null);
		initTreeThread(1);
		initCompant();
	}

	public MainForm(int width, int height) {
		this.width = width;
		this.height = height;
		this.setBackground(Color.white);
		this.setLayout(null);
		initTreeThread(1);
		initCompant();
	}

	public List<DbTable> getDbTable() {
		DBHelper.initDbConfig();
		if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
			return DBHelper.getMysqlTables(DBHelper.schema);
		}
		if (DBHelper.sections.equals(OracleSetPanel.sections)) {
			return DBHelper.getOracleTables(DBHelper.schema);
		}
		return null;
	}

	public List<?> getTableColumn(String table) {
		DBHelper.initDbConfig();
		if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
			return DBHelper.getMySqlTableColumns(DBHelper.schema, table);
		}
		if (DBHelper.sections.equals(OracleSetPanel.sections)) {
			return DBHelper.getOracleTableColumns(DBHelper.schema, table);
		}
		return null;
	}

	private Map<String, List<DbTableColumn>> getTableColumn() {
		DBHelper.initDbConfig();
		if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
			return DBHelper.getMySqlMutilTableColumns(DBHelper.schema);
		}
		if (DBHelper.sections.equals(OracleSetPanel.sections)) {
			return DBHelper.getOracleMutilTableColumns(DBHelper.schema);
		}
		return null;
	}

	private JTextField entityNameText;
	private JTextField entPackageText;
	private JTextField daoPackageText;
	private JTextField serPackageText;
	//private JTextField mapPackageText;
	//private JTextField hbmPackageText;
	private JTextField cotPackageText;
	private JTextField actPackageText;
	private JTextField jspFileText;
	private JTextField jsFileText;
	
	private JComboBox<Object> dbFrameCombo;
	private JComboBox<Object> cFrameCombo;
	private JComboBox<Object> vFrameCombo;
	private JComboBox<Object> jsFrameCombo;
	private JCheckBox daoBox;
	private JCheckBox serviceBox;
	//private JCheckBox mapBox;
	//private JCheckBox hbmBox;
	private JCheckBox actionBox;
	private JCheckBox controllerBox;
	private JCheckBox jspBox;
	private JCheckBox jsBox;
	private JLabel perview1;
	private JLabel perview2;
	
	//private JLabel packageLabel3;
	
	// crud 操作checkbox 定义
	private JCheckBox getCheckBox;
	private JCheckBox addCheckBox;
	private JCheckBox modifyCheckBox;
	private JCheckBox delCheckBox;
	private JCheckBox pageCheckBox;
	private JCheckBox findCheckBox;
	private JCheckBox countCheckBox;

	private JButton seekTableDataBtn;
	private JButton singleCreateBtn;
	private JButton mutilCreateBtn;
	private JButton chooseDirBtn;

	private JComboBox<Object> cigCombo;
	
	private JButton saveBtn;
	private JButton newSaveBtn;
	private String chooseTable = null;
	
	JFileChooser jfc = new JFileChooser();// 文件选择器  
	private JTextField saveDirText;

	public void initCompant() {
		int left = 300, labelWidth = 55, labelTextHeight = 25, top = 60, textWidth = 150;

		// 按钮层
		seekTableDataBtn = new JButton("查看表数据");
		seekTableDataBtn.setBounds(left, 5, textWidth, labelTextHeight);
		add(seekTableDataBtn);
		seekTableDataBtn.setEnabled(false);

		singleCreateBtn = new JButton("生成选择的表");
		singleCreateBtn.setBounds(left + textWidth + 10, 5, textWidth, labelTextHeight);
		add(singleCreateBtn);
		singleCreateBtn.setEnabled(false);
		
		mutilCreateBtn = new JButton("生成全部表");
		mutilCreateBtn.setBounds(left + (textWidth + 10) * 2, 5, textWidth, labelTextHeight); 
		add(mutilCreateBtn);

		JLabel cigLabel = new JLabel("配置方案");
		cigLabel.setBounds(left, labelTextHeight + 20, labelWidth, labelTextHeight);
		add(cigLabel);
		cigLabel.setVisible(false);
		cigCombo = new JComboBox<Object>();
		cigCombo.setName("cigCombo");
		cigCombo.setBounds(left + labelWidth, labelTextHeight + 20, textWidth, labelTextHeight);
		add(cigCombo);
		cigCombo.setVisible(false);

		saveBtn = new JButton("保存");
		saveBtn.setBounds(left + (labelWidth) * 2 + textWidth - 20 , labelTextHeight + 20, textWidth - 60, labelTextHeight);
		add(saveBtn);
		saveBtn.setVisible(false);
		
		newSaveBtn = new JButton("另存为");
		newSaveBtn.setBounds(left + (labelWidth) * 2 + (textWidth * 2 - 32), labelTextHeight + 20, textWidth - 60, labelTextHeight);
		add(newSaveBtn);
		newSaveBtn.setVisible(false);
		
		/**************CRUD操作*************************/
		JLabel crudFrameLabel = new JLabel("CRUD操作");
		crudFrameLabel.setBounds(left, top + ((labelTextHeight) * 1), labelWidth, labelTextHeight);
		add(crudFrameLabel);
		
		getCheckBox = new JCheckBox("get");
		getCheckBox.setBounds(left + labelWidth, top + labelTextHeight, (textWidth/2 - 20), labelTextHeight);
		add(getCheckBox);
		addCheckBox = new JCheckBox("add");
		addCheckBox.setBounds(left + labelWidth + (textWidth/2 - 10), top + labelTextHeight, (textWidth/2 - 20), labelTextHeight);
		add(addCheckBox);
		modifyCheckBox = new JCheckBox("modify");
		modifyCheckBox.setBounds(left + labelWidth + textWidth - 20, top + labelTextHeight, textWidth/2, labelTextHeight);
		add(modifyCheckBox);
		delCheckBox = new JCheckBox("delete");
		delCheckBox.setBounds(left + labelWidth + textWidth + 65, top + labelTextHeight, textWidth/2, labelTextHeight);
		add(delCheckBox);
		pageCheckBox = new JCheckBox("page");
		pageCheckBox.setBounds(left + labelWidth + textWidth*2, top + labelTextHeight, (textWidth/2 - 10), labelTextHeight);
		add(pageCheckBox);
		findCheckBox = new JCheckBox("find");
		findCheckBox.setBounds(left + labelWidth + (textWidth*2 + 75), top + labelTextHeight, (textWidth/2 - 10), labelTextHeight);
		add(findCheckBox);
		countCheckBox = new JCheckBox("count");
		countCheckBox.setBounds(left + labelWidth + (textWidth*2 + 150), top + labelTextHeight, (textWidth/2 - 10), labelTextHeight);
		add(countCheckBox);
		
		/**************持久层配置*************************/
		JLabel dbFrameLabel = new JLabel("持久层");
		dbFrameLabel.setBounds(left, top + ((labelTextHeight + 10) * 2), labelWidth, labelTextHeight);
		add(dbFrameLabel);
		dbFrameCombo = new JComboBox<Object>(new Object[] {Consts.DAO_FRAME_HIBERNATE, Consts.DAO_FRAME_MYBATIS});
		dbFrameCombo.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 2), textWidth, labelTextHeight);
		add(dbFrameCombo);
		
		/**************实体类配置*************************/
		JLabel entityNameLabel = new JLabel("实体类");
		entityNameLabel.setBounds(left, top + ((labelTextHeight + 10) * 3), labelWidth, labelTextHeight);
		add(entityNameLabel);
		entityNameText = new JTextField(StringUtils.EMPTY);
		entityNameText.setName("entiry");
		entityNameText.setEditable(false);
		entityNameText.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 3), textWidth, labelTextHeight);
		add(entityNameText);

		JLabel packageLabel = new JLabel("包路径");
		packageLabel.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 3), labelWidth, labelTextHeight);
		add(packageLabel);
		entPackageText = new JTextField(StringUtils.EMPTY);
		entPackageText.setName("entityPackage");
		entPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 3), textWidth * 2, labelTextHeight);
		add(entPackageText);

		/**************Dao层配置*************************/
		daoBox = new JCheckBox("生成Dao接口/类");
		daoBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 4), textWidth, labelTextHeight);
		add(daoBox);
		daoBox.setSelected(true);

		JLabel packageLabel2 = new JLabel("包路径");
		packageLabel2.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 4), labelWidth, labelTextHeight);
		add(packageLabel2);
		daoPackageText = new JTextField(StringUtils.EMPTY);
		daoPackageText.setName("daoPackage");
		daoPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 4), textWidth * 2, labelTextHeight);
		add(daoPackageText);

		/**************服务层配置*************************/
		/*mapBox = new JCheckBox("生成Mapper.xml");
		mapBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 5), textWidth, labelTextHeight);
		add(mapBox);
		mapBox.setSelected(true);

		hbmBox = new JCheckBox("生成Hbm.xml");
		hbmBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 5), textWidth, labelTextHeight);
		add(hbmBox);
		hbmBox.setVisible(false);

		packageLabel3 = new JLabel("包路径");
		packageLabel3.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 5), labelWidth, labelTextHeight);
		add(packageLabel3);
		mapPackageText = new JTextField(StringUtils.EMPTY);
		mapPackageText.setName("mapPackage");
		mapPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 5), textWidth * 2, labelTextHeight);
		add(mapPackageText);
		hbmPackageText = new JTextField(StringUtils.EMPTY);
		hbmPackageText.setName("hbmPackage");
		hbmPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 5), textWidth * 2, labelTextHeight);
		add(hbmPackageText);
		hbmPackageText.setVisible(false);*/

		/**************服务层配置*************************/
		serviceBox = new JCheckBox("生成Service接口/类");
		serviceBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 5), textWidth, labelTextHeight);
		add(serviceBox);
		serviceBox.setSelected(true);

		JLabel packageLabel5 = new JLabel("包路径");
		packageLabel5.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 5), labelWidth, labelTextHeight);
		add(packageLabel5);
		serPackageText = new JTextField(StringUtils.EMPTY);
		serPackageText.setName("serPackage");
		serPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 5), textWidth * 2, labelTextHeight);
		add(serPackageText);

		perview1 = new JLabel(StringUtils.EMPTY);
		perview1.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 2), textWidth * 3, labelTextHeight);
		perview1.setForeground(Color.BLUE);
		add(perview1);

		/**************控制层配置*************************/
		JLabel cFrameLabel = new JLabel("控制层");
		cFrameLabel.setBounds(left, top + ((labelTextHeight + 10) * 6), labelWidth, labelTextHeight);
		add(cFrameLabel);
		cFrameCombo = new JComboBox<Object>(new Object[] {Consts.MVC_FRAME_SPRING});
		cFrameCombo.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 6), textWidth, labelTextHeight);
		add(cFrameCombo);

		// action
		controllerBox = new JCheckBox("生成Controller类");
		controllerBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 7), textWidth, labelTextHeight);
		add(controllerBox);
		controllerBox.setSelected(true);

		actionBox = new JCheckBox("生成Action类");
		actionBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 7), textWidth, labelTextHeight);
		add(actionBox);
		actionBox.setVisible(false);

		JLabel packageLabel7 = new JLabel("包路径");
		packageLabel7.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 7), labelWidth, labelTextHeight);
		add(packageLabel7);
		cotPackageText = new JTextField(StringUtils.EMPTY);
		cotPackageText.setName("cotPackage");
		cotPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 7), textWidth * 2, labelTextHeight);
		add(cotPackageText);
		actPackageText = new JTextField(StringUtils.EMPTY);
		actPackageText.setName("actPackage");
		actPackageText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 7), textWidth * 2, labelTextHeight);
		add(actPackageText);
		actPackageText.setVisible(false);
		
		
		/**************视图层配置*************************/
		JLabel vFrameLabel = new JLabel("视图层");
		vFrameLabel.setBounds(left, top + ((labelTextHeight + 10) * 8), labelWidth, labelTextHeight);
		add(vFrameLabel);
		vFrameCombo = new JComboBox<Object>(new Object[] {Consts.VIEW_FRAME_JSP});
		vFrameCombo.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 8), textWidth, labelTextHeight);
		add(vFrameCombo);
		jsFrameCombo = new JComboBox<Object>(new Object[] {Consts.VIEW_FRAME_JS});
		jsFrameCombo.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 8), textWidth, labelTextHeight);
		add(jsFrameCombo);
		
		jspBox = new JCheckBox("生成jsp");
		jspBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 9), textWidth, labelTextHeight);
		add(jspBox);
		jspBox.setSelected(true);
		jspBox.setVisible(true);
		
		JLabel jspLabel = new JLabel("jsp路径");
		jspLabel.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 9), labelWidth, labelTextHeight);
		add(jspLabel);
		jspFileText = new JTextField(StringUtils.EMPTY);
		jspFileText.setName("jspFile");
		jspFileText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 9), textWidth * 2, labelTextHeight);
		add(jspFileText);
		
		jsBox = new JCheckBox("生成js");
		jsBox.setBounds(left + labelWidth, top + ((labelTextHeight + 10) * 10), textWidth, labelTextHeight);
		add(jsBox);
		jsBox.setSelected(true);
		jsBox.setVisible(true);
		
		JLabel jsLabel = new JLabel("js路径");
		jsLabel.setBounds(left + labelWidth + textWidth + 10, top + ((labelTextHeight + 10) * 10), labelWidth, labelTextHeight);
		add(jsLabel);
		jsFileText = new JTextField(StringUtils.EMPTY);
		jsFileText.setName("jsFile");
		jsFileText.setBounds(left + labelWidth * 2 + textWidth + 10, top + ((labelTextHeight + 10) * 10), textWidth * 2, labelTextHeight);
		add(jsFileText);

		perview2 = new JLabel("项目位置");
		perview2.setBounds(left, top + ((labelTextHeight + 11) * 11), labelWidth, labelTextHeight);
		add(perview2);
		
		saveDirText = new JTextField("d:/");
		saveDirText.setName("saveDirText");
		saveDirText.setBounds(left + labelWidth, top + ((labelTextHeight + 11) * 11), textWidth * 3 - 50, labelTextHeight);
		saveDirText.setEditable(false);
		add(saveDirText);
		
		chooseDirBtn = new JButton("选择位置");
		chooseDirBtn.setBounds(left + labelWidth + textWidth * 3 - 40, top + ((labelTextHeight + 11) * 11), textWidth - 50, labelTextHeight);
		add(chooseDirBtn);
		chooseDirBtn.addActionListener(new BtnActionListener());
		
		dbFrameCombo.addItemListener(new ComboItemListener());
		cFrameCombo.addItemListener(new ComboItemListener());
		cigCombo.addItemListener(new ComboItemListener());

		entPackageText.addKeyListener(new InputKeyListener());
		daoPackageText.addKeyListener(new InputKeyListener());
		serPackageText.addKeyListener(new InputKeyListener());
		//mapPackageText.addKeyListener(new InputKeyListener());
		actPackageText.addKeyListener(new InputKeyListener());

		daoBox.addActionListener(new BoxActionListener());
		serviceBox.addActionListener(new BoxActionListener());
		//mapBox.addActionListener(new BoxActionListener());
		//hbmBox.addActionListener(new BoxActionListener());
		controllerBox.addActionListener(new BoxActionListener());
		actionBox.addActionListener(new BoxActionListener());
		jspBox.addActionListener(new BoxActionListener());
		jsBox.addActionListener(new BoxActionListener());

		seekTableDataBtn.addActionListener(new BtnActionListener());
		singleCreateBtn.addActionListener(new BtnActionListener());
		mutilCreateBtn.addActionListener(new BtnActionListener());
		saveBtn.addActionListener(new BtnActionListener());
		newSaveBtn.addActionListener(new BtnActionListener());

		configInited = false;
		initCombo();
	}

	public void initCombo() {
		IniReader reader = IniReader.getIniReader();
		HashMap<String, HashMap<String, String>> configMap = reader.getConfig(config_section);
		if (MapUtils.isNotEmpty(configMap)) {
			cigCombo.removeAllItems();
			String ise = null;
			HashMap<String, String> cfg = null;
			int index = 0, currIndex = 0;
			for (Map.Entry<String, HashMap<String, String>> entity : configMap.entrySet()) {
				cigCombo.addItem(entity.getKey());
				cfg = entity.getValue();
				if (MapUtils.isNotEmpty(cfg)) {
					ise = cfg.get("isSelected");
					if (ise.equals("true")) {
						currIndex = index;
					}
				}
				index++;
			}
			configInited = true;
			if (cigCombo.getItemCount() > currIndex) {
				cigCombo.setSelectedIndex(currIndex);
			}
			if(cigCombo.getItemCount() == 0){
			    cigCombo.addItem("Config 1");
			    cigCombo.setSelectedIndex(0);
			}
		} else {
		    if(cigCombo.getItemCount() == 0){
                cigCombo.addItem("Config 1");
                cigCombo.setSelectedIndex(0);
            }
		}
	}

	public void initValue(HashMap<String, String> viewCig) {
		if (MapUtils.isNotEmpty(viewCig)) {
			try {
				if (viewCig.get("dao_frame").equalsIgnoreCase(Consts.DAO_FRAME_MYBATIS)) {
					dbFrameCombo.setSelectedIndex(0);
				} else {
					dbFrameCombo.setSelectedIndex(1);
				}
				if (viewCig.get("controll_frame").equalsIgnoreCase(Consts.MVC_FRAME_SPRING)) {
					cFrameCombo.setSelectedIndex(0);
				} else {
					cFrameCombo.setSelectedIndex(1);
				}
				if(viewCig.get("view_frame").equalsIgnoreCase(Consts.VIEW_FRAME_JSP)){
					vFrameCombo.setSelectedIndex(0);
				} else {
					vFrameCombo.setSelectedIndex(1);
				}
				if(viewCig.get("js_frame").equalsIgnoreCase(Consts.VIEW_FRAME_JS)){
					jsFrameCombo.setSelectedIndex(0);
				} else {
					jsFrameCombo.setSelectedIndex(1);
				}
				
				daoPackageText.setText(viewCig.get("dao_package"));
				serPackageText.setText(viewCig.get("service_package"));
				entPackageText.setText(viewCig.get("entity_package"));
				//mapPackageText.setText(viewCig.get("map_package"));
				//hbmPackageText.setText(viewCig.get("hbm_package"));
				cotPackageText.setText(viewCig.get("controller_package"));
				actPackageText.setText(viewCig.get("action_package"));
				jspFileText.setText(viewCig.get("jsp_package"));
				jsFileText.setText(viewCig.get("js_package"));
				saveDirText.setText(viewCig.get("save_dir"));
			} catch (Exception e) {
				Toolkit.getDefaultToolkit().beep();
				JOptionPane.showMessageDialog(null, "操作异常：" + e.getMessage(), "提示", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public void saveConfig(int flag) {
		try {
			IniReader reader = IniReader.getIniReader();
			String tag = StringUtils.EMPTY;
			int index = cigCombo.getSelectedIndex();
			if (flag == 2) { // 另存为
				index = cigCombo.getItemCount() + 1;
				tag = "Config " + index;
				cigCombo.addItem(tag);
				configInited = false;
				cigCombo.setSelectedIndex(index - 1);
				configInited = true;
				reader.putValue(config_section, tag, "isSelected", "true");
			} else {
				tag = cigCombo.getSelectedItem().toString();
			}
			reader.putValue(config_section, tag, "if_create_dao", daoBox.isSelected() ? "true" : "false");
			//reader.putValue(config_section, tag, "if_create_map", mapBox.isSelected() ? "true" : "false");
			//reader.putValue(config_section, tag, "if_create_hbm", hbmBox.isSelected() ? "true" : "false");
			reader.putValue(config_section, tag, "if_create_service", serviceBox.isSelected() ? "true" : "false");
			reader.putValue(config_section, tag, "if_create_controller", controllerBox.isSelected() ? "true" : "false");
			reader.putValue(config_section, tag, "if_create_action", actionBox.isSelected() ? "true" : "false");
			reader.putValue(config_section, tag, "if_create_jsp", jspBox.isSelected() ? "true" : "false");
			reader.putValue(config_section, tag, "if_create_js", jsBox.isSelected() ? "true" : "false");

			reader.putValue(config_section, tag, "service_package", serPackageText.getText());
			reader.putValue(config_section, tag, "entity_package", entPackageText.getText());
			reader.putValue(config_section, tag, "dao_package", daoPackageText.getText());
			//reader.putValue(config_section, tag, "map_package", mapPackageText.getText());
			//reader.putValue(config_section, tag, "hbm_package", hbmPackageText.getText());
			reader.putValue(config_section, tag, "controller_package", cotPackageText.getText());
			reader.putValue(config_section, tag, "action_package", actPackageText.getText());
			reader.putValue(config_section, tag, "jsp_package", jspFileText.getText());
			reader.putValue(config_section, tag, "js_package", jsFileText.getText());
			reader.putValue(config_section, tag, "save_dir", saveDirText.getText());

			reader.putValue(config_section, tag, "dao_frame", dbFrameCombo.getSelectedItem().toString());
			reader.putValue(config_section, tag, "controll_frame", cFrameCombo.getSelectedItem().toString());
			reader.putValue(config_section, tag, "view_frame", vFrameCombo.getSelectedItem().toString());
			reader.putValue(config_section, tag, "js_frame", jsFrameCombo.getSelectedItem().toString());
			reader.save();
		} catch (Exception e) {
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(null, "操作异常：" + e.getMessage(), "提示", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void initTreeThread(int flag) {
	    if(loadding){
	        /*int count = dbNode.getChildCount();
	        if(count > 0){
	        }*/
	        return;
	    }
		new Thread() {
			public void run() {
			    loadding = true;
			    MainTiger.mainTiger.showProcessPanel();
				initTree();
				MainTiger.mainTiger.hideProcessPanel();
				loadding = false;
			}
		}.start();
	}

	public void initTree() {
		// 创建没有父节点和子节点、但允许有子节点的树节点，并使用指定的用户对象对它进行初始化。
		if (Objects.nonNull(jScrollPane1)) {
		    jScrollPane1.removeAll();
		    this.remove(jScrollPane1);
		}

		List<DbTable> data = getDbTable();
		int tableSize = -1;
		if (CollectionUtils.isNotEmpty(data)) {
			tableSize = data.size();
		}

		DbTable table = null;
		if (tableSize >= 0) {
			Object tableNodes[] = new Object[tableSize];
			Vector<Object> tableVector = null;
			for (int i = 0; i < tableSize; i++) {
				table = data.get(i);
				tableVector = new NamedVector(table.getTableName());
				tableNodes[i] = tableVector;
			}
			dbVector = new NamedVector(DBHelper.schema, tableNodes);
			
			tree = new JTree(dbVector);
			CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
			tree.setCellRenderer(renderer);
	
			tree.setCellEditor(new CheckBoxNodeEditor(tree));
			tree.setEditable(true);
	
			jScrollPane1 = new JScrollPane();
			jScrollPane1.getViewport().add(tree);
			jScrollPane1.setBounds(5, 5, 290, height - 45);
			this.add(jScrollPane1);
	
			// 添加选择事件
			tree.addTreeSelectionListener(new JTreeSelectionListener());
			// 有表，启动加载表列线程
			new Thread(new LoadColumnThread()).start();
		} else {
			new Thread(new AlertThread()).start();
		}
	}

	class JTreeSelectionListener implements TreeSelectionListener {
		public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
			if (Objects.isNull(node)) {
				return;
			}
			
			int count = node.getChildCount();
			Object object = node.getUserObject();
			if (Objects.nonNull(object)) { // node.isLeaf() //node.isRoot()
			    String table = StringUtils.EMPTY;
				int level = node.getLevel();
				if (level == 0) { // 数据库层
					chooseTable = null;
					seekTableDataBtn.setEnabled(false);
					singleCreateBtn.setEnabled(false);
					entityNameText.setEditable(false);
					entityNameText.setText(StringUtils.EMPTY);
					setCompTooltip();
					if (count == 0) {
						initTreeThread(1);
					}
				} else if (level == 1) { // 表层
					NamedVector nameVector = (NamedVector) object;
					int selnum = tree.getSelectionCount();
					table = nameVector.toString();
					children = node.children();
					chooseTable = table;
					seekTableDataBtn.setEnabled(selnum == 1);
					singleCreateBtn.setEnabled(true);
					//entityNameText.setText(Utililies.tableToEntity(table));
					entityNameText.setText(CommonUtils.convertToCamelCase(table));
					setCompTooltip();
					if (count == 0) {
						initTableFeild(nameVector, table);
					}
				} else if (level == 2) { // 字段层
					int selnum = tree.getSelectionCount();
					seekTableDataBtn.setEnabled(selnum == 1);
					seekTableDataBtn.setEnabled(false);
					singleCreateBtn.setEnabled(false);
					entityNameText.setEditable(false);
					DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();
					NamedVector nameVector = (NamedVector) parent.getUserObject();
					entityNameText.setText(Utililies.tableToEntity(nameVector.toString()));
					chooseTable = nameVector.toString();
				}
			}
		}
	}

	public void renderTableFeild(NamedVector nameVector, String table, List<?> columns) {
		if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
			if (CollectionUtils.isNotEmpty(columns)) {
				MysqlDbColumn colum = null;
				CheckBoxNode node[] = new CheckBoxNode[columns.size()];
				
				String title = StringUtils.EMPTY;
				Vector<Object> vector = new Vector<Object>();
				for (int i = 0, k = columns.size(); i < k; i++) {
					colum = (MysqlDbColumn) columns.get(i);
					
					title = colum.getColumnName() + " - " + colum.getColumnType();
					if(colum.getColumnKey().equalsIgnoreCase(Consts.MYSQL_COLUMN_KEY)){
						title = title + " - " + colum.getColumnKey().toLowerCase();
					}
					node[i] = new CheckBoxNode(title, false);
					vector.add(colum.getColumnName());
					nameVector.add(node[i]);
				}
				if (!columnMap.containsKey(table)) {
					columnMap.put(table, vector);
				}
			}
		}
		if (DBHelper.sections.equals(OracleSetPanel.sections)) { 
			if (CollectionUtils.isNotEmpty(columns)) {
				OracleDbColumn colum = null;
				CheckBoxNode node[] = new CheckBoxNode[columns.size()];
				
				String title = StringUtils.EMPTY;
				Vector<Object> vector = new Vector<Object>();
				for (int i = 0, k = columns.size(); i < k; i++) {
					colum = (OracleDbColumn) columns.get(i);
					
					title = colum.getColumnName() + " - " + colum.getColumnType();
					if(Objects.nonNull(colum.getColumnKey()) && colum.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_KEY)){
						title = title + " - " + Consts.MYSQL_COLUMN_KEY;
					}
					node[i] = new CheckBoxNode(title, false);
					vector.add(colum.getColumnName());
					nameVector.add(node[i]);
				}
				if (!columnMap.containsKey(table)) {
					columnMap.put(table, vector);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")  
	public void initTableFeild(NamedVector nameVector, String table) {
		if (DBHelper.sections.equals(MySqlSetPanel.sections)) {
			List<MysqlDbColumn> columns = (List<MysqlDbColumn>) getTableColumn(table);
			if (CollectionUtils.isNotEmpty(columns)) {
				MysqlDbColumn colum = null;
				CheckBoxNode node[] = new CheckBoxNode[columns.size()];
				
				String title = StringUtils.EMPTY;
				Vector<Object> vector = new Vector<Object>();
				for (int i = 0, k = columns.size(); i < k; i++) {
					colum = columns.get(i);
					title = colum.getColumnName() + " - " + colum.getColumnType();
					if(colum.getColumnKey().equalsIgnoreCase(Consts.MYSQL_COLUMN_KEY)){
						title = title + " - " + colum.getColumnKey().toLowerCase();
					}
					node[i] = new CheckBoxNode(title, false);
					nameVector.add(node[i]);
					vector.add(colum.getColumnName());
				}
				if (!columnMap.containsKey(table)) {
					columnMap.put(table, vector);
				}
			}
		}
	}

	class LoadColumnThread implements Runnable {
		public void run() {
			Map<String, List<DbTableColumn>> tabelMap = getTableColumn();
			if (MapUtils.isNotEmpty(tabelMap)) {
				DbCache.mysqlDbColumnMap = tabelMap;
				List<?> comulnList = null;
				int size = dbVector.size();
				
				NamedVector nameVector;
				for (int i = 0; i < size; i++) {
					nameVector = (NamedVector) dbVector.get(i);
					if (Objects.nonNull(nameVector)) {
						comulnList = tabelMap.get(nameVector.toString());
						if (CollectionUtils.isNotEmpty(comulnList)) {
							renderTableFeild(nameVector, nameVector.toString(), comulnList);
						}
					}
				}
			}
			JScrollBar sBar = jScrollPane1.getVerticalScrollBar(); // 得到JScrollPane中的JScrollBar
			sBar.setValue(sBar.getMaximum()); // 设置JScrollBar的位置到最后
			sBar.setValue(sBar.getMinimum()); // 设置JScrollBar的位置到最前
		}
	}

	public void setCompTooltip() {
		changeTips("entityPackage", entPackageText.getText());
		changeTips("daoPackage", daoPackageText.getText());
		//changeTips("mapPackage", mapPackageText.getText());
		changeTips("serPackage", serPackageText.getText());
		changeTips("actPackage", actPackageText.getText());
	}

	public void changeTips(String name, String value) {
		String entity = entityNameText.getText();
		if (entity == null || entity.length() < 1) {
			entity = "[实体类名称]";
		}
		if (name.equals("entityPackage")) {
			entityNameText.setToolTipText(value + "." + entity + ".java");
		} else if (name.equals("daoPackage")) {
			daoBox.setToolTipText(value + "." + entity + "Mapper.java");
		/*} else if (name.equals("mapPackage")) {
			if (mapBox.isSelected()) {
				mapBox.setToolTipText(Utililies.packageToDir(value) + "/" + entity + "Mapper.xml");
			} else if (hbmBox.isSelected()) {
				hbmBox.setToolTipText(Utililies.packageToDir(value) + "/" + entity + ".hbm.xml");
			}*/
		} else if (name.equals("serPackage")) {
			serviceBox.setToolTipText(value + "." + entity + "Service.java");
		} else if (name.equals("actPackage")) {
			if (actionBox.isSelected()) {
				actionBox.setToolTipText(value + "/" + entity + "Controller.java");
			} else if (controllerBox.isSelected()) {
				controllerBox.setToolTipText(value + "/" + entity + "Controller.java");
			}
		}
	}

	public void changeConfig(String tag) {
		if (configInited) {
			IniReader reader = IniReader.getIniReader();
			reader.replaceActiveCfg(config_section, tag);
			initValue(reader.getConfig(config_section, tag));
		}
	}

	public void startCreate(boolean isall) {
		List<String> tables = null;
		if (isall) {
			tables = getAllTable(dbVector);
		} else {
			tables = getSelectedTable();
		}

		if (CollectionUtils.isNotEmpty(tables)) {
			CreateAttr ca = getCreAttr(tables);
			if (Objects.nonNull(ca)) {
				new CreatorThread(tables, ca).start();
			}
		}
	}

	public CreateAttr getCreAttr(List<String> tables) {
		CreateAttr ca = new CreateAttr();
		String entityName = "{EntityName}", maven = StringUtils.EMPTY;
		String entPackage = entPackageText.getText();
		if(entPackage.startsWith("main.java.")){
		    maven = "main/java/";
		}
		ca.setEntityName(entityName);
		ca.setEntityPackage(Utililies.getRealPackage(entPackageText.getText()) + "." + entityName);
		ca.setEntityFilePath(Utililies.packageToDir(Utililies.getRealPackage(entPackageText.getText()) + "/") + entityName + ".java");
		ca.setEntityPKFilePath(Utililies.packageToDir(Utililies.getRealPackage(entPackageText.getText()) + "/") + entityName + "PK.java");
		ca.setEntitySerdeFilePath(Utililies.packageToDir(Utililies.getRealPackage(entPackageText.getText()) + "/") + "EntitySerde.java");
		if (daoBox.isSelected()) {
			ca.setDaoName(entityName + "Mapper");
			ca.setDaoPackage(Utililies.getRealPackage(daoPackageText.getText()) + "." + ca.getDaoName());
			ca.setDaoFilePath(Utililies.packageToDir(Utililies.getRealPackage(daoPackageText.getText()) + "/") + ca.getDaoName() + ".java");
			ca.setMapFilePath(Utililies.packageToDir(Utililies.getRealPackage(daoPackageText.getText()) + "/") + ca.getDaoName() + ".xml");
		}
		if (serviceBox.isSelected()) {
			ca.setServiceName(entityName + "ServiceImpl");
			ca.setIserviceName("I" + entityName + "Service");
			ca.setServicePackage(Utililies.getRealPackage(serPackageText.getText()) + ".impl." + ca.getServiceName());
			ca.setIservicePackage(Utililies.getRealPackage(serPackageText.getText()) + "." + ca.getIserviceName());
			ca.setServiceFilePath(Utililies.packageToDir(Utililies.getRealPackage(serPackageText.getText()) + "/impl/") + ca.getServiceName() + ".java");
			ca.setIserviceFilePath(Utililies.packageToDir(Utililies.getRealPackage(serPackageText.getText()) + "/") + ca.getIserviceName() + ".java");
		}
		/*if (mapBox.isSelected()) {
			ca.setMapName(entityName + "Mapper");
			ca.setMapPackage(Utililies.getRealPackage(mapPackageText.getText()));
			ca.setMapFilePath(Utililies.packageToDir(Utililies.getRealPackage(mapPackageText.getText()) + "/") + ca.getMapName() + ".xml");
		}
		if (hbmBox.isSelected()) {
			ca.setHbmName(entityName + ".hbm");
			ca.setHbmPackage(Utililies.getRealPackage(hbmPackageText.getText()));
			ca.setHbmFilePath(Utililies.packageToDir(Utililies.getRealPackage(hbmPackageText.getText()) + "/") + ca.getHbmName() + ".xml");
		}*/
		if (controllerBox.isSelected()) {
			ca.setControllerName(entityName + "Controller");
			ca.setControllerRealPackage(Utililies.getRealPackage(cotPackageText.getText()));
			ca.setControllerPackage(Utililies.getRealPackage(cotPackageText.getText()) + "." + ca.getControllerName());
			ca.setControllerFilePath(Utililies.packageToDir(Utililies.getRealPackage(cotPackageText.getText()) + "/") + ca.getControllerName() + ".java");
		}
		if (actionBox.isSelected()) {
			ca.setActionName(entityName + "Action");
			ca.setActionPackage(Utililies.getRealPackage(actPackageText.getText()) + "." + ca.getActionName());
			ca.setActionFilePath(Utililies.packageToDir(Utililies.getRealPackage(actPackageText.getText()) + "/") + ca.getActionName() + ".java");
		}
		if (jspBox.isSelected()) {
			ca.setJspFileName(entityName);
			ca.setJspFilePath(Utililies.packageToDir(jspFileText.getText() + "/") + entityName +"/");
		}
		if(jsBox.isSelected()) {
			ca.setJsFileName(entityName);
			ca.setJsFilePath(Utililies.packageToDir(jsFileText.getText() + "/") + ca.getJsFileName() + ".js");
		}
		
		ca.setDaoFrame(dbFrameCombo.getSelectedItem().toString());
		ca.setConFrame(cFrameCombo.getSelectedItem().toString());
		ca.setViewFrame(vFrameCombo.getSelectedItem().toString());
		ca.setJsFame(jsFrameCombo.getSelectedItem().toString());
		
		ca.setDaoOp(daoBox.isSelected());
		ca.setServiceOp(serviceBox.isSelected());
		ca.setControllerOp(controllerBox.isSelected());
		ca.setJspOp(jspBox.isSelected());
		ca.setJsOp(jsBox.isSelected());
		
		ca.setGetOp(getCheckBox.isSelected());
		ca.setAddOp(addCheckBox.isSelected());
		ca.setModifyOp(modifyCheckBox.isSelected());
		ca.setDeleteOp(delCheckBox.isSelected());
		ca.setPageOp(pageCheckBox.isSelected());
		ca.setFindOp(findCheckBox.isSelected());
		ca.setCountOp(countCheckBox.isSelected());
		
		ca.setSaveDir(CommonUtils.isBlank(saveDirText.getText()) ? "d:/code/" : saveDirText.getText() + maven);
		ca.setSaveDir2(CommonUtils.isBlank(saveDirText.getText()) ? "d:/code/" : saveDirText.getText());
		
		// 设置表有选中的属性对象
		Multimap<String, CheckBoxNode> selectedNodes = ArrayListMultimap.create();
		while(children.hasMoreElements()) {
		    DefaultMutableTreeNode next = children.nextElement();  
		    CheckBoxNode checkBoxNode = (CheckBoxNode) next.getUserObject();
		    //System.out.println(checkBoxNode.toString());  
		    if (Objects.nonNull(checkBoxNode)) {
		    	if (checkBoxNode.isSelected()) { 
		    		selectedNodes.put(chooseTable, checkBoxNode);
		    	}
		    }
		}  
		ca.setSelectedNodes(selectedNodes);
		return ca;
	}
	
	// 获取所有表
	public List<String> getAllTable(Vector<Object> dbVector) {
		List<String> tableList = Lists.newArrayList();
		Enumeration<Object> children = dbVector.elements();
		while (children.hasMoreElements()) {
			NamedVector NameVector = (NamedVector) children.nextElement(); 
			if (Objects.nonNull(NameVector)) {
				tableList.add(NameVector.toString());
			}
		}
		return tableList;
	}

	// 获取所有选中的表
	public List<String> getSelectedTable() {
		List<String> tableList = Lists.newArrayList();
		TreePath[] treePaths = tree.getSelectionPaths();
		if (ArrayUtils.isNotEmpty(treePaths)) {
			DefaultMutableTreeNode node = null;
			for (int i = 0; i < treePaths.length; i++) {
				node = (DefaultMutableTreeNode) treePaths[i].getLastPathComponent();
				if (Objects.nonNull(node)) {
					if (node.getLevel() == 1) { // 表
						NamedVector nameVector = (NamedVector) node.getUserObject();
						if (Objects.nonNull(nameVector)) {
							tableList.add(nameVector.toString());
						}
					}
				}
			}
		}
		return tableList;
	}

	class AlertThread implements Runnable {
		public void run() {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "未检测到有效配置，数据库表加载失败", "提示", JOptionPane.ERROR_MESSAGE);
		}
	}

	private class ComboItemListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				String fname = e.getItem().toString();
				/*if (fname.equals("Hibernate")) {
					mapBox.setSelected(false);
					mapBox.setVisible(false);
					mapPackageText.setVisible(false);
					hbmBox.setSelected(true);
					hbmBox.setVisible(false);
					hbmPackageText.setVisible(false);
					packageLabel3.setVisible(false);
				} else if (fname.equals("Mybatis")) {
					hbmBox.setSelected(false);
					hbmBox.setVisible(false);
					hbmPackageText.setVisible(false);
					mapBox.setSelected(true);
					mapBox.setVisible(true);
					mapPackageText.setVisible(true);
					packageLabel3.setVisible(true);
				} else*/ if (fname.equals(Consts.MVC_FRAME_STRUTS2)) {
					actionBox.setVisible(true);
					actionBox.setSelected(true);
					actPackageText.setVisible(true);
					
					controllerBox.setSelected(false);
					controllerBox.setVisible(false);
					cotPackageText.setVisible(false);
				} else if (fname.equals(Consts.MVC_FRAME_SPRING)) {
					controllerBox.setVisible(true);
					controllerBox.setSelected(true);
					cotPackageText.setVisible(true);
					
					actionBox.setVisible(false);
					actionBox.setEnabled(false);
					actPackageText.setVisible(false);
				} else { // 配置方案更改
					changeConfig(fname);
				}
			}
		}
	}

	class InputKeyListener implements KeyListener {
		public void keyTyped(KeyEvent e) { // 按下

		}

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) { // 放开
			JTextField textFeild = (JTextField) e.getComponent();
			String name = textFeild.getName();
			changeTips(name, textFeild.getText());
		}
	}

	class BoxActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JCheckBox box = (JCheckBox) e.getSource();
			String name = box.getText();
			LOG.info("JCheckBox####################"+ name);
			if (name.equals("生成Dao接口/类")) {
				daoPackageText.setEnabled(box.isSelected());
				daoPackageText.setText(StringUtils.EMPTY);
			/*} else if (name.equals("生成Mapper.xml")) {
				mapPackageText.setEnabled(box.isSelected());
				mapPackageText.setText(StringUtils.EMPTY);*/
			} else if (name.equals("生成Service接口/类")) {
				serPackageText.setEnabled(box.isSelected());
				serPackageText.setText(StringUtils.EMPTY);
			/*} else if (name.equals("生成Hbm.xml")) {
				mapPackageText.setEnabled(box.isSelected());
				mapPackageText.setText(StringUtils.EMPTY);*/
			} else if (name.equals("生成Controller类")) {
				cotPackageText.setEnabled(box.isSelected());
				cotPackageText.setText(StringUtils.EMPTY);
			} else if (name.equals("生成Action类")) {
				actPackageText.setEnabled(box.isSelected());
				actPackageText.setText(StringUtils.EMPTY);
			} else if (name.equals("生成jsp")) {
				jspFileText.setEnabled(box.isSelected());
				jspFileText.setText(StringUtils.EMPTY);
			} else if (name.equals("生成js")) {
				jsFileText.setEnabled(box.isSelected());
				jsFileText.setText(StringUtils.EMPTY);
			}
			
		}
	}

	class BtnActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String comm = e.getActionCommand();
			LOG.info("##########BtnActionListener->comm=======" + comm);
			if (comm.equals("查看表数据")) {
				MainTiger.mainTiger.showTableDataPanel(chooseTable);
			} else if (comm.equals("生成全部表")) {
				startCreate(true);
			} else if (comm.equals("生成选择的表")) {
				LOG.info("in....");
				startCreate(false);
			} else if (comm.equals("保存")) {
				saveConfig(1);
			} else if (comm.equals("另存为")) {
				saveConfig(2);
			} else if (comm.equals("选择位置")) {
				String dir = saveDirText.getText();
				if(CommonUtils.isBlank(dir)){
					jfc.setCurrentDirectory(new File("d://")); // 文件选择器的初始目录定为d盘 
				} else {
					jfc.setCurrentDirectory(new File(dir));
				}
				jfc.setFileSelectionMode(1); // 设定只能选择到文件夹  
		        int state = jfc.showOpenDialog(null); // 此句是打开文件选择器界面的触发语句  
		        if (state == 1) {  
		            return;  
		        } else {  
		            File f = jfc.getSelectedFile(); // f为选择到的目录  
		            String path = f.getAbsolutePath();
		            saveDirText.setText(path.replaceAll("\\\\", "/") + "/");  
		        }
			}
		}
	}

}
