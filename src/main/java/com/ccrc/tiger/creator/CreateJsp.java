/**
 * 
 */
package com.ccrc.tiger.creator;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import com.ccrc.tiger.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.ccrc.tiger.db.DBHelper;
import com.ccrc.tiger.entity.CreateAttr;
import com.ccrc.tiger.entity.DbTableColumn;
import com.ccrc.tiger.entity.MysqlDbColumn;
import com.ccrc.tiger.entity.OracleDbColumn;
import com.ccrc.tiger.panel.MySqlSetPanel;
import com.ccrc.tiger.panel.OracleSetPanel;
import com.ccrc.tiger.util.CommonUtils;
import com.ccrc.tiger.util.Consts;
import com.ccrc.tiger.util.Utililies;
import com.google.common.io.Files;

/**
 * @author Jalen
 * 创建jsp 页面
 */
public class CreateJsp extends Creator {

	public CreateJsp(CreateAttr ca, String tname, List<DbTableColumn> columnList) {
		super(ca, tname, columnList);
	}

	public String createJspList() {
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jspPath = Utililies.parseFilePath(tmpPath, ca.getJspFilePath() + ca.getJspFileName()+ "_list.jsp");
        String result = ca.getJspFileName() + "_list.jsp文件生成完成：" + jspPath;
        try {
        	File jspFile = new File(jspPath);
			if(jspFile.exists()){
				result = ca.getJspFileName() + "_list.jsp文件已存在，无须重新生成！";
			} else {
	            String content = Utililies.readResourceFile(dir + "jsp_list.tlp");
	            
	            // title
	            content = Utililies.parseTemplate(content, "title", getEntityName());
	            content = Utililies.parseTemplate(content, "jsPath", StringUtils.isNoneBlank(ca.getJsFilePath()) ? ca.getJsFilePath().replaceAll("main/webapp/", StringUtils.EMPTY) : StringUtils.EMPTY);
	            content = Utililies.parseTemplate(content, "jsJquery", Consts.IMPORT_JQUERY);
	            content = Utililies.parseTemplate(content, "jsJooe", Consts.IMPORT_JS_JOOE);
	            // js 对象名
	            content = Utililies.parseTemplate(content, "jsObj", StringUtils.uncapitalize(getEntityName()));
	            content = Utililies.parseTemplate(content, "jsClazz", getEntityName());
	            // 获取 页面列表列
	            content = Utililies.parseTemplate(content, "TableColumn", getTableColumn());
	            // 获取 编辑列 
	            content = Utililies.parseTemplate(content, "EditColumn", getEditColumn());
	            
	            String[] curColumn = getCurColumnKey();
	            content = Utililies.parseTemplate(content, "ID", CommonUtils.convertToFirstLetterLowerCaseCamelCase(curColumn[0]));
	            Utililies.writeContentToFile(jspPath, content);
			}
        } catch (Exception e) {
        	e.printStackTrace();
            result = ca.getJspFileName() + "_list.jsp文件生成失败：" + e.getMessage();
        }
        return result;
	}
	
	public String createFirstJsp() {
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jspPath = Utililies.parseFilePath(tmpPath, "main/webapp/first.jsp");
        String result = "first.jsp文件生成完成：" + jspPath;
        try {
        	File jspFile = new File(jspPath);
        	String content = StringUtils.EMPTY;
        	String uncapEntityName = StringUtils.uncapitalize(getEntityName());
			if(jspFile.exists()){
				result = "first.jsp文件已存在，无须重新生成！";
			} else {
	            content = Utililies.readResourceFile(dir + "first.tlp");
	           // content = Utililies.parseTemplate2(content, "URL", Consts.getHref(uncapEntityName));
	            Files.write(content.getBytes(), jspFile);
			}
			
			if(!Utililies.isFileText(jspFile, "/"+ uncapEntityName +"/")){
				content = createSingleMethod(jspFile, Consts.firstJsp(getEntityName()), "</body>");
				Files.write(content.getBytes(), jspFile);
			}
        } catch (Exception e) {
            result = "first.jsp文件生成失败：" + e.getMessage();
        }
        return result;
	}
	
	public String createIndexJsp() {
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jspPath = Utililies.parseFilePath(tmpPath, "main/webapp/index.jsp");
        String result = "index.jsp文件生成完成：" + jspPath;
        try {
        	File jspFile = new File(jspPath);
        	String content = StringUtils.EMPTY;
        	String uncapEntityName = StringUtils.uncapitalize(getEntityName());
			if(jspFile.exists()){
				result = "index.jsp文件已存在，无须重新生成！";
			} else {
	            content = Utililies.readResourceFile(dir + "index.tlp");
	            Files.write(content.getBytes(), jspFile);
			}
			
			if(!Utililies.isFileText(jspFile, "/"+ uncapEntityName +"/")){
				content = createSingleMethod(jspFile, Consts.indexJsp(getEntityName()), "<br>");
				Files.write(content.getBytes(), jspFile);
			}
        } catch (Exception e) {
        	e.printStackTrace();
            result = "index.jsp文件生成失败：" + e.getMessage();
        }
        return result;
	}
	
	/*public static void main(String[] args) {
		try {
		String filePath = "D:/neonWorkspace/test/src/main/webapp/index.jsp";
		File jspFile = new File(filePath);
		String content = createSingleMethod1(jspFile, Consts.indexJsp("UserTab"), "<br>");
		Files.write(content.getBytes(), jspFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public String createJspAdd() {
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jspPath = Utililies.parseFilePath(tmpPath, ca.getJspFilePath() + ca.getJspFileName()+ "_add.jsp");
        String result = ca.getJspFileName() + "_add.jsp文件生成完成：" + jspPath;
        try {
        	File jspFile = new File(jspPath);
			if(jspFile.exists()){
				result = ca.getJspFileName() + "_add.jsp文件已存在，无须重新生成！";
			} else {
	            String content = Utililies.readResourceFile(dir + "jsp_add.tlp");
	            
	            // title
	            content = Utililies.parseTemplate(content, "title", getEntityName());
	            content = Utililies.parseTemplate(content, "jsJquery", Consts.IMPORT_JQUERY);
	            content = Utililies.parseTemplate(content, "jsJooe", Consts.IMPORT_JS_JOOE);
	            
	            Utililies.writeContentToFile(jspPath, content);
			}
        } catch (Exception e) {
            result = ca.getJspFileName() + "_add.jsp文件生成失败：" + e.getMessage();
        }
        return result;
	}
	
	public String createJspEdit() {
		String tmpPath = ca.getSaveDir().replace("main/java/", StringUtils.EMPTY);
		String jspPath = Utililies.parseFilePath(tmpPath, ca.getJspFilePath() + ca.getJspFileName()+ "_edit.jsp");
        String result = ca.getJspFileName() + "_edit.jsp文件生成完成：" + jspPath;
        try {
        	File jspFile = new File(jspPath);
			if(jspFile.exists()){
				result = ca.getJspFileName() + "_edit.jsp文件已存在，无须重新生成！";
			} else {
	            String content = Utililies.readResourceFile(dir + "jsp_edit.tlp");
	            
	            // title
	            content = Utililies.parseTemplate(content, "title", getEntityName());
	            content = Utililies.parseTemplate(content, "jsJquery", Consts.IMPORT_JQUERY);
	            content = Utililies.parseTemplate(content, "jsJooe", Consts.IMPORT_JS_JOOE);
	            
	            Utililies.writeContentToFile(jspPath, content);
			}
        } catch (Exception e) {
            result = ca.getJspFileName() + "_edit.jsp文件生成失败：" + e.getMessage();
        }
        return result;
	}

	/**
	 * 编辑列
	 * @return
	 */
	private String getEditColumn() {
		StringBuffer sb = new StringBuffer();
		if (Objects.nonNull(columnList)) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					MysqlDbColumn column = (MysqlDbColumn) columnList.get(i);
					
					if(Objects.nonNull(column) && column.getColumnKey().equals(Consts.MYSQL_COLUMN_KEY)){
						sb.append(Consts.TAB3 + "<input type=\"hidden\" name=\""+ CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()) +"\">" + Consts.ENTER);
					}else {
						
						String valid = StringUtils.EMPTY;
						if(column.getIsNullable().equalsIgnoreCase(Consts.COLUMN_NULL_NO)) {
							valid = "class=\"easyui-validatebox\" required=\"true\"";
						}
						if(column.getDataType().toLowerCase().contains("date") || column.getDataType().toLowerCase().equalsIgnoreCase("time")) {
							valid = "class=\"easyui-datetimebox\" required=\"true\"";
						}
						String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
						sb.append(Consts.TAB3 +"<div class=\"fitem\">");
						sb.append("<label>"+ feild +":</label>");
						sb.append("<input name=\""+ feild +"\" "+ valid +">");
						sb.append("</div>" + Consts.ENTER);
					}
				}
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					OracleDbColumn column = (OracleDbColumn) columnList.get(i);
					if(StringUtils.isNoneEmpty(column.getColumnKey()) && column.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_C)) {
						continue;
					}
					
					if(Objects.nonNull(column) && StringUtils.isNoneEmpty(column.getColumnKey()) 
							&& column.getColumnKey().equals(Consts.ORACLE_COLUMN_KEY)){
						sb.append(Consts.TAB3 + "<input type=\"hidden\" name=\""+ CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName()) +"\">" + Consts.ENTER);
					}else {
						String valid = StringUtils.EMPTY;
						if(column.getIsNullable().equalsIgnoreCase(Consts.COLUMN_NULL_NO)) {
							valid = "class=\"easyui-validatebox\" required=\"true\"";
						}
						if(column.getDataType().toLowerCase().contains("date") || column.getDataType().toLowerCase().equalsIgnoreCase("time")) {
							valid = "class=\"easyui-datetimebox\" required=\"true\"";
						}
						String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
						sb.append(Consts.TAB3 +"<div class=\"fitem\">");
						sb.append("<label>"+ feild +":</label>");
						sb.append("<input name=\""+ feild +"\" "+ valid +">");
						sb.append("</div>" + Consts.ENTER);
					}
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 页面列表列
	 * @return
	 */
	private String getTableColumn() {
		StringBuffer sb = new StringBuffer();
		if (Objects.nonNull(columnList)) {
			if(DBHelper.sections.equals(MySqlSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					MysqlDbColumn column = (MysqlDbColumn) columnList.get(i);
					
					Integer length = column.getCharacterMaximumLength();
					if(length.equals(BigDecimal.ZERO.intValue())){
						length = Consts.GRID_WIDTH;
					}
					String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
					sb.append(Consts.TAB4 + "<th data-options=\"field:'"+ feild +"',width:"+ length +"\">"+ feild +"</th>" + Consts.ENTER);
				}
			}
			if(DBHelper.sections.equals(OracleSetPanel.sections)) {
				for(int i=0; i<columnList.size(); i++){
					OracleDbColumn column = (OracleDbColumn) columnList.get(i);
					if(StringUtils.isNoneEmpty(column.getColumnKey()) && column.getColumnKey().equalsIgnoreCase(Consts.ORACLE_COLUMN_C)) {
						continue;
					}
					
					Integer length = column.getCharacterMaximumLength();
					if(length.equals(BigDecimal.ZERO.intValue())){
						length = Consts.GRID_WIDTH;
					}
					String feild = CommonUtils.convertToFirstLetterLowerCaseCamelCase(column.getColumnName());
					sb.append(Consts.TAB4 + "<th data-options=\"field:'"+ feild +"',width:"+ length +"\">"+ feild +"</th>" + Consts.ENTER);
				}
			}
		}
		return sb.toString();
	}
	
}
